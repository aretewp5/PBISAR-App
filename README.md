# PBIS-AR App

The use of **Augmented Reality** in behavioral education is the main topic explored inside the pilot #3 of the EU funded project **ARETE** that aims to build a collaborative AR ecosystem with multi-user interaction to enhance educational processes. In particular, the pilot #3, named Augmented Reality for promoting Positive Behaviour Intervention and Support (PBIS), has the purpose of integrating an innovative AR solution (**PBIS-AR App**) within the PBIS framework that promotes student behavioral and cognitive growth. PBIS is a multi-tiered data-driven support, assessment, and intervention system that has a potentially broad application range. PBIS systems and procedures use empirical decision making to stimulate appropriate behaviors and successful outcomes for students. Schools using PBIS are different from traditional schools. In a PBIS school, behavior is conceived as a form of communication.


In the ARETE project AR experiences, related to various PBIS scenarios, will be developed and piloted. These experiences will be used to investigate the effective value of integrating AR technology within PBIS interventions and supports to encourage positive behaviors school-wide and classroom-wide. To support the assessment of this scenario, the ExperienceAPI (xAPI) standard for monitoring and recording behavior will be adopted to integrate the AR ecosystem with a cloud-based learning record repository and PBIS analytics tools.


PBIS-AR App has received funding from the European Union’s Horizon 2020 research and innovation programme through the ARETE project with the grant agreement No 856533.
