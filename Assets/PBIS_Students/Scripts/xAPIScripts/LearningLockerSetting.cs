﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Library for LearningLocker
using System;
//using System.Net;
using TinCan;
using TinCan.LRSResponses;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

public class LearningLockerSetting : MonoBehaviour
{
    public static LearningLockerSetting instance;
    public RemoteLRS lrs;

    private const char Separator = '=';
    private readonly Dictionary<string, string> _verb = new Dictionary<string, string>();
    private readonly Dictionary<string, string> _activity = new Dictionary<string, string>();


    void Awake()
    {
        // Debug.Log("Learnig Locker Awake");
        ReadActivities();
        ReadVerbs();
        instance = this;
        //reference to Learning Locker Server: url key and secret
        lrs = new RemoteLRS(
        "https://analytics.pa.itd.cnr.it/data/xAPI",
        "f463979fe212b7849ffc4bcabd289eaba965c74b",
        "b01a612a44c730a90bdac8994a6b48bfda608875"
        );
        // TODO In order to solve any issue related to the certificate chain insert this code
        // Per risolvere eventuali errori dvuti alla catena del certificato bisogna inserire la linea di codice sottostante, includendo la libreria using System.Net;
        //ServicePointManager.ServerCertificateValidationCallback = delegate {return true;};
    }

    public void SendStatement()
    {

        //Build out Actor details
        Agent actor = new Agent();

        actor.mbox = "mailto:marco.arrigo@itd.cnr.it";
        //actor.account = "https://arete.pa.itd.cnr.it/";
        //actor.name = Player.instance.Username;
        actor.name = "User123";


        //Build out Verb details
        Verb verb = new Verb();
        verb.id = new Uri("https://brindlewaye.com/xAPITerms/verbs/loggedin/");
        verb.display = new LanguageMap();
        verb.display.Add("en-US", "Log In");

        //Build out Activity details
        Activity activity = new Activity();
        activity.id = "http://activitystrea.ms/schema/1.0/application";

        //Build out Activity Definition details
        ActivityDefinition activityDefinition = new ActivityDefinition();
        activityDefinition.description = new LanguageMap();
        activityDefinition.name = new LanguageMap();
        activityDefinition.name.Add("en-US", ("AR-PBIS Application"));
        activity.definition = activityDefinition;

        // SendStatement(actor, verb, activity);

    }

    public void SendStatement(String verbName, String activityName)
    {
        Debug.Log("LearningLockerSetting- SendStatement ...");
        Agent actor = new Agent();
        AgentAccount agentActor = new AgentAccount();
        //agentActor.name = Player.instance.Username;
        //agentActor.name = "Agent123";
        agentActor.name = Player.instance.username;
        // agentActor.name = NickNameManager.instance.nameUser;
        Debug.Log("LearningLockerSetting- SendStatement - Name: " + agentActor.name);
        agentActor.homePage = new Uri("https://arete.pa.itd.cnr.it/");
        actor.account = agentActor;

        //Build out Verb details
        Verb verb = new Verb();
        string verbAddress = Regex.Unescape(_verb[verbName]);
        Debug.Log("LearningLockerSetting- SendStatement - verbAddress:" + verbAddress);
        //Decommentare in seguito
        //string verbLocalizedName = FindObjectOfType<LangResolver>().GetLocalizedText(verbName);
        //Debug.Log("verbAddress:" +verbLocalizedName);
        verb.id = new Uri(verbAddress);
        verb.display = new LanguageMap();
        verb.display.Add("en-US", verbName);
        //verb.display.Add("en-US", verbLocalizedName);

        //Build out Activity details
        Activity activity = new Activity();
        activity.id = Regex.Unescape(_activity[activityName]);
        ActivityDefinition activityDefinition = new ActivityDefinition();
        activityDefinition.description = new LanguageMap();
        activityDefinition.name = new LanguageMap();
        //string activityLocalizedName = FindObjectOfType<LangResolver>().GetLocalizedText(activityName);
        //activityDefinition.name.Add("en-US", (activityLocalizedName));
        activityDefinition.name.Add("en-US", (activityName));
        activity.definition = activityDefinition;

        SendStatement(actor, verb, activity);
    }

    public void SendStatement(Agent actor, Verb verb, Activity activity)
    {
        //Build out full Statement details
        Statement statement = new Statement();
        statement.actor = actor;
        statement.verb = verb;
        statement.target = activity;


        // Settaggio del timestamp!!!
        /*DateTime dt = new DateTime(2017, 6, 26, 20, 45, 0, 70, DateTimeKind.Utc);
        string iDate = dt.ToString("yyyy-MM-ddTHH:mm:ssZ");
        DateTime oDate = Convert.ToDateTime(iDate);
        statement.timestamp = oDate;*/

        //Send the statement
        //StatementLRSResponse lrsResponse = lrs.SaveStatement(statement);
        StatementLRSResponse lrsResponse = LearningLockerSetting.instance.lrs.SaveStatement(statement);
        if (lrsResponse.success) //Success
        {
            Debug.Log("Save statement: " + lrsResponse.content.id);
        }
        else //Failure
        {
            Debug.Log("Statement Failed: " + lrsResponse.errMsg);
        }
    }


    private void ReadVerbs()
    {
        Debug.Log("LearningLockerSetting- ReadVerbs -  Learnig Locker loading Verbs ...");
        var verbFile = Resources.Load<TextAsset>("Verb");
        if (verbFile != null)
        {
            foreach (var line in verbFile.text.Split('\n'))
            {
                var prop = line.Split(Separator);
                //Debug.Log("Numero parametri per Linea: " + prop.Length);
                // To accep also comment lines
                if (prop.Length == 2)
                {
                    _verb[prop[0]] = prop[1];
                }
                else
                {
                    Debug.Log("LearningLockerSetting- ReadVerbs - Non key=Value - Probably a comment");
                }


            }
            Debug.Log("LearningLockerSetting- ReadVerbs - Learnig Locker Verbs loaded.");
        }else{
            Debug.Log("LearningLockerSetting- ReadVerbs - Learnig Locker Verbs file NULL ------- ERROR ------");
        }
    }

    private void ReadActivities()
    {
        Debug.Log("LearningLockerSetting- ReadActivities -Learnig Locker loading Activities ...");
        var activityFile = Resources.Load<TextAsset>("Activity");
        if (activityFile != null)
        {
            foreach (var line in activityFile.text.Split('\n'))
            {
                var prop = line.Split(Separator);
                //Debug.Log("Numero parametri per Linea: " + prop.Length);
                // To accep also comment lines
                if (prop.Length == 2)
                {
                    _activity[prop[0]] = prop[1];
                }
                else
                {
                    Debug.Log("LearningLockerSetting- ReadActivities - No key=Value - Probably a comment");
                }
            }
            Debug.Log("LearningLockerSetting- ReadActivities - Learnig Locker Activities loaded.");
        }else{
            Debug.Log("LearningLockerSetting- ReadActivities - Learnig Locker Activities file NULL ------- ERROR ------");
        }
    }
}
