﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeWritterEffect : MonoBehaviour
{
    public GameObject startButton;
    public float delay = 0.1f;
    public string fullText;
    private string currentText = "";
    private string fullTextLang = "";
    
    void Start()
    {
        this.GetComponent<Text>().text = currentText;
        fullTextLang = FindObjectOfType<LangResolver>().GetLocalizedText(fullText);
        StartCoroutine(ShowText());
        
    }
    
    IEnumerator ShowText(){
        for (int i=0; i<fullTextLang.Length; i++){
            currentText = fullTextLang.Substring(0,i);
            this.GetComponent<Text>().text = currentText;
            yield return new WaitForSeconds(delay);
        }
        // Quando terma l'effetto macchina da scriver avvia l'animazione sul pulsante start
        startButton.GetComponent<Animation>().Play("Start_Button");
        
    }

}
