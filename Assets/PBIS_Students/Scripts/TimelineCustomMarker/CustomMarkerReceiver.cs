using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

/*
 * CustomMarkerReceiver - to manage signals send by a timeline animation through a custom Marker "TmelineAnimationFinished"
 */
public class CustomMarkerReceiver : MonoBehaviour, INotificationReceiver
{
    public void OnNotify(Playable origin, INotification notification, object context)
    {
        // Debug.Log("AnimationFinishedReceiver: " + notification);
        if (notification is TmelineAnimationFinished animationFinished && animationFinished != null)
        {
            Debug.Log("AnimationFinished: " + animationFinished.AnimationName);
            PBISAnimationEvents.SendAnimationFinished(animationFinished.AnimationName);
        }
    }
}
