using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

/*
 * TmelineAnimationFinished - Custom Marker to manage signals form Timeline Animation. It needs a receiver to activate an event reaction
 * 
 * Marco Arrigo
 */
public class TmelineAnimationFinished : Marker, INotification, INotificationOptionProvider
{
    [SerializeField] private string animationName = "";

    [SerializeField] private bool retroactive = false;
    [SerializeField] private bool emitOnce = false;
    public PropertyName id => new PropertyName();

    public string AnimationName => animationName;

    public NotificationFlags flags =>
        (retroactive ? NotificationFlags.Retroactive : default) |
        (emitOnce ? NotificationFlags.TriggerOnce : default);

}
