using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class messagePanelManager : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI _message;
    [SerializeField] private Image _leftImage;
    [SerializeField] private Image _rightImage;
    [SerializeField] private Transform _messagePanel;


    // Start is called before the first frame update
    void Start()
    {
        //SetMoveDeviceMessage();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ClearMessage()
    {
        Debug.Log("messagePanelManager - ClearMessage");
        //_message.color = new Color32(118, 239, 243, 255);
        _message.text = "";
        _leftImage.enabled = false;
        _rightImage.enabled = false;
        _message.text = "";
        _messagePanel.gameObject.SetActive(false);
    }


    public void SetMoveDeviceMessage()
    {
        Debug.Log("messagePanelManager - SetMoveDeviceMessage");
        _message.text = "MOVE YOUR DEVICE";
        _leftImage.enabled = true;
        _rightImage.enabled = true;
        _messagePanel.gameObject.SetActive(true);
    }

    public void SetPlaceAlienMessage()
    {
        Debug.Log("messagePanelManager - SetPlaceAlienMessage");
        //_message.color = new Color32(118, 239, 243, 255); 
        _message.text = "Would you like to meet a strange friend? \n\n If so, please touch the place where you like to see it.";
        _leftImage.enabled = false;
        _rightImage.enabled = false;
        _messagePanel.gameObject.SetActive(true);
    }

    public void SetDiscoveryMessage()
    {
        Debug.Log("messagePanelManager - SetDiscoveryMessage");
        //_message.color = new Color32(118, 239, 243, 255);
        _message.text = "Hi, you're in discovery mode. Look for some ARETE clues that hide additional behaviours, collect them and you might get a prize!";
        _leftImage.enabled = false;
        _rightImage.enabled = false;
        _messagePanel.gameObject.SetActive(true);
        StartCoroutine(ClearCoroutine());
    }

    public void ClearDiscoveryMessage()
    {
        Debug.Log("messagePanelManager - ClearDiscoveryMessage");
        //_message.color = new Color32(118, 239, 243, 255);
        _message.text = "";
        _leftImage.enabled = false;
        _rightImage.enabled = false;
        _message.text = "";
        _messagePanel.gameObject.SetActive(false);
    }

    IEnumerator ClearCoroutine()
    {
        // Debug.Log("messagePanelManager - ClearCoroutine - Start");
        // waits for 5 seconds.
        yield return new WaitForSeconds(5);

        //After we have waited 5 seconds clear the message pannel and hide it.
        _message.text = "";
        _messagePanel.gameObject.SetActive(false);
    }



}