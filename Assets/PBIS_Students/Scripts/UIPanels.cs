using UnityEngine;
using UnityEngine.Events;

// [RequireComponent(typeof(Animator))]
public class UIPanels : MonoBehaviour
{
    private Animator anim;
    private void Awake()
    {
        Debug.Log("****   **** UIPanel Awake");
        anim = GetComponent<Animator>();
        
        foreach(AnimationClip ac in anim.runtimeAnimatorController.animationClips){
             Debug.Log("****   **** UIPanels - Awake Animazione: " + ac.name);
        }
    }

    public void showHaiPersoPanel()
    {
        anim.Play("SHORT-IN");
    }


    public void showLoginPanel()
    {
        Debug.Log("****   **** UIPanels - showLoginPanel");
        //anim.Play("Login_IN");
        anim.Play("LoginPanelIN");
    }

    public void closeLoginPanel()
    {
        // anim.Play("Login-OUT");
    }

}
