using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class LeaderBoard
{
    public string nameFirst;
    public int pointFirst;
    public string nameSecond;
    public int pointSecond;
    public string nameThird;
    public int pointThird;

}
