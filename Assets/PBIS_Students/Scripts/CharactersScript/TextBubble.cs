using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextBubble : MonoBehaviour
{
    private TextMeshPro textMessage;
    private SpriteRenderer speechBubble;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Awake() {
        // backgroundSpriteRenderer = transform.Find("Background").GetComponent<SpriteRenderer>();
        // iconSpriteRenderer = transform.Find("Icon").GetComponent<SpriteRenderer>();
        speechBubble = transform.Find("SpeechBubble").GetComponent<SpriteRenderer>();
        textMessage = transform.Find("TextMessage").GetComponent<TextMeshPro>();
        Debug.Log("****   **** TextBubble- TextMessage:"+textMessage.text);
        
    }


    public void Setup(string text) {
        Debug.Log("****   **** TextBubble- set TextMessage:"+text);
        textMessage.SetText(text);
        textMessage.ForceMeshUpdate();
        /*
        Vector2 textSize = textMeshPro.GetRenderedValues(false);

        Vector2 padding = new Vector2(7f, 3f);
        backgroundSpriteRenderer.size = textSize + padding;

        Vector3 offset = new Vector3(-3f, 0f);
        backgroundSpriteRenderer.transform.localPosition = 
            new Vector3(backgroundSpriteRenderer.size.x / 2f, 0f) + offset;

        iconSpriteRenderer.sprite = GetIconSprite(iconType);

        TextWriter.AddWriter_Static(textMeshPro, text, .03f, true, true, () => { });
        */
    }
}
