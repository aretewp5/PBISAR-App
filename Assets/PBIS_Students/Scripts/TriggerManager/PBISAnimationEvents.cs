using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// PBISAnimationEvents contains all nimation Events connected to the Routine animations. The event are triggered from the relative animator
/// </summary>
public class PBISAnimationEvents : MonoBehaviour
{
    /// <summary>
    /// Invoked whenever an animatin is finished.
    /// </summary
    //public static event System.Action<Transform> OnAnimationFinished;
    public static event System.Action<string> OnAnimationFinished;


    /*
    public static void SendAnimationFinished(Transform animationRoutine)
    {
        Debug.Log("****   **** PBISAnimationEvents - sendAnimationFinished Animazione: " + animationRoutine.name);
        OnAnimationFinished?.Invoke(animationRoutine);
    }
    */
    
    public static void SendAnimationFinished(string animationRoutine)
    {
        Debug.Log("PBISAnimationEvents - sendAnimationFinished Animazione: " + animationRoutine);
        OnAnimationFinished?.Invoke(animationRoutine);
    }
}
