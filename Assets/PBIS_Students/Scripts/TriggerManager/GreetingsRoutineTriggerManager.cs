using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GreetingsRoutineTriggerManager : MonoBehaviour
{
    public String animationName;
    public void StopEvent()
    {

        Transform animationRoutine = gameObject.transform.parent;
        Debug.Log("RoutineTriggerManager - StopEvent Name: " + gameObject.name + "  Animation: " + animationRoutine.name);


        // Used in IntroDialogManage
        // if (OnAnimationFinished != null)
        //{
        //    OnAnimationFinished(animationRoutine);
        //}
        //AnimationEvents.OnAnimationFinished?.Invoke(animationRoutine);


        //PBISAnimationEvents.SendAnimationFinished(animationRoutine);
        PBISAnimationEvents.SendAnimationFinished(animationName);
    }
}
