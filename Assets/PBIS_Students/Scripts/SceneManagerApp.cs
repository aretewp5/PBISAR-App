using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;
using UnityEngine.Networking;
using System.Text;

public class SceneManagerApp : MonoBehaviour
{
    private static SceneManagerApp _instance;
    
    public static SceneManagerApp Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(this);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
        //SceneManager.LoadSceneAsync("IntroPBIS", LoadSceneMode.Additive);

        // Check if it is the first time the app is used
        if (!PlayerPrefs.HasKey("appUsage")) {
            PlayerPrefs.SetInt("appUsage", 0);
            PlayerPrefs.Save();
        }
        else
        {
            Debug.Log("SceneManagerApp - Awake - appUsage: " + PlayerPrefs.GetInt("appUsage"));
        }
        



        string pbisAppUser = PlayerPrefs.GetString("pbisAppUserName");
        string pbisAppPsw = PlayerPrefs.GetString("pbisAppUserPassword");


        Debug.Log("SceneManagerApp - Awake - pbisAppUser: " + pbisAppUser + "   pbisAppPsw: " + pbisAppPsw);
        if ((pbisAppUser != null) && (pbisAppUser.Length > 0) && (pbisAppPsw != null) && (pbisAppPsw.Length > 0))
        {
            StartCoroutine(SiletLoginPbisAppUser(pbisAppUser, pbisAppPsw));
        }
        else
        {
            SceneManager.LoadScene("SetupScene", LoadSceneMode.Additive);
        }
    }

    IEnumerator SiletLoginPbisAppUser(string pbisAppUser, string pbisAppPsw)
    {
        string ulrString = "https://arete.pa.itd.cnr.it/arete/rest/public/login";
        string logindataJsonString = "{ \"username\":\"" + pbisAppUser + "\" , \"password\": \"" + pbisAppPsw + "\" }";
        Debug.Log("SceneManagerApp - SiletLoginPbisAppUser - urlString: " + ulrString);
        var request = new UnityWebRequest(ulrString, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(logindataJsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();

        if (request.error != null)
        {
            if ((request.responseCode >= 400) && (request.responseCode < 500))
            {
                if ((request.responseCode == 401) || (request.responseCode == 403))
                {
                    Debug.Log("SceneManagerApp - SiletLoginPbisAppUser - Error! ");
                    SceneManagerApp.ResetEnviroment();
                    SceneManager.LoadScene("SetupScene", LoadSceneMode.Additive);
                }
                else
                {
                    Debug.Log("SceneManagerApp - SiletLoginPbisAppUser - Error! ");
                    SceneManagerApp.ResetEnviroment();
                    SceneManager.LoadScene("SetupScene", LoadSceneMode.Additive);
                    //infoMessage.text = FindObjectOfType<LangResolver>().GetLocalizedText("RESOURCE_FORBIDDEN");
                }
            }
            else
            {//if (request.responseCode >= 500))
                Debug.Log("SceneManagerApp - SiletLoginPbisAppUser - Error! ");
                SceneManagerApp.ResetEnviroment();
                SceneManager.LoadScene("SetupScene", LoadSceneMode.Additive);
                //infoMessage.text = FindObjectOfType<LangResolver>().GetLocalizedText("INTERNAL_SERVER_ERROR");
            }
        }
        else
        {
            Debug.Log("SceneManagerApp - SiletLoginPbisAppUser - OK");
            string xAuthToken = request.GetResponseHeader("x-auth");
            Debug.Log("SceneManagerApp - SiletLoginPbisAppUser - xAuthToken: " + xAuthToken);
            UserModel user = JsonConvert.DeserializeObject<UserModel>(request.downloadHandler.text);
            //Debug.Log("SceneManager - SiletLoginPbisAppUser - User Nome: " + user.nome);
            Player.instance.pbisAppUserXAuthToken = xAuthToken;
            //Debug.Log("SceneManager - SiletLoginPbisAppUser - User  OrganizationName: " + user.organizationName);
            Player.instance.organization = user.organizationName;

            // TODO Revove - ONLY for debug
            //PlayerPrefs.SetString("pbisAppUserName","");

            //Debug.Log("SceneManager - LoadSceneAsync: IntroPBIS");
            SceneManager.LoadSceneAsync("IntroPBIS", LoadSceneMode.Additive);
        }

    }

    public static void ResetEnviroment()
    {
        Debug.Log("SceneManagerApp - ResetEnviroment - OK");
        Player.Reset();

        PlayerPrefs.SetString("pbisAppUserName", "");
        PlayerPrefs.SetString("pbisAppUserPassword", "");
        PlayerPrefs.Save();

    }

}
