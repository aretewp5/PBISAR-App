﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Linq;

namespace MirageXR
{
    public class CharacterController : MirageXRPrefab
    {
        [SerializeField] private TMP_Dropdown animationMenu;
        [SerializeField] private GameObject imageContainer;
        [SerializeField] private Transform rightHandBone;
        [SerializeField] private GameObject imageAssigner;
        [Tooltip("You need to play with this to set the correct start rotation of the image container")]
        [SerializeField] private Vector3 handRotationOffset;

        private Animator _anim;
        private NavMeshAgent _agent;

    
        private void Start()
        {


        }

        private  void OnEnable()
        {
            _anim = GetComponentInChildren<Animator>();
            _agent = GetComponent<NavMeshAgent>();
        }

        private void OnDisable()
        {
      
        }


        private void Update()
        {

            PlayClip("Idle");
        }

        

        private void PlayClip(string clipName)
        {
            if (!_anim) return;

            foreach (var param in _anim.parameters) _anim.SetBool(param.name, param.name == clipName);
        }
    }

}
