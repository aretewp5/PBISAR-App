﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationUIManagerBtn : MonoBehaviour
{

    private Animator anim;
    public Button btnOpen;
    public Button btnClose;

    private void Start()
    {
       

    }
    void Awake()
    {
        
        GameObject uiCanvas = GameObject.Find("TeachCanvas");
        if(uiCanvas)
            anim = uiCanvas.GetComponent<Animator>();
        

    }

    public void CloseRoutine()
    {
        anim.Play("ROUTINE_OUT");
        
        btnOpen.gameObject.SetActive(true);
        btnClose.gameObject.SetActive(false);

    }

    public void OpenRoutine()
    {
        anim.Play("ROUTINE_IN");
        
        btnOpen.gameObject.SetActive(false);
        btnClose.gameObject.SetActive(true);
    }
}
