using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PracticeSceneManager : MonoBehaviour
{
    public void Start()
    {
        SceneManager.sceneUnloaded += OnSceneUnloaded;
        Debug.Log("****   **** PracticeSceneManager - Start: SceneLoaded");
    }

    private void OnSceneUnloaded(Scene current)
    {
        Resources.UnloadUnusedAssets();
        Debug.Log("****   **** PracticeSceneManager - OnSceneUnloaded: " + current);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
