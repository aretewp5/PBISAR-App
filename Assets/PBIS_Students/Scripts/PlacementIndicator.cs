using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems; // for the raycast

public class PlacementIndicator : MonoBehaviour
{
    private ARRaycastManager rayManager;
    private GameObject visual;
    private Transform messagePanelTansform;
    // private GameObject messagePanel;
    private messagePanelManager messageManager;

    private void Start()
    {
        // Get the componets
        rayManager = FindObjectOfType<ARRaycastManager>();
        visual = transform.GetChild(0).gameObject;

        // Hide the placement visual
        visual.SetActive(false);
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        if (uiCanvas != null)
        {
            messagePanelTansform = uiCanvas.transform.FindDeepChild("MessagePanel");
            GameObject messagePanel = messagePanelTansform.gameObject;
            messageManager = (messagePanelManager)messagePanel.GetComponent(typeof(messagePanelManager));
        }
    }

    private void Update()
    {
        // Shoot a raycast from the center of the screen
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        rayManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, TrackableType.Planes);

        // If we hit an AR plane, update the position and rotation
        if ((hits.Count > 0)&&(!Player.instance.skipped))
        {
            Debug.Log("PlacementIndicator - Update - Detecting Planes");
            if (messageManager)
            {
                Debug.Log("PlacementIndicator - Update - messageManager SetPlaceAlienMessage");
                // Debug.Log("****   **** TextBubble trovata");
                //messagePanelTansform.gameObject.SetActive(true);
                messageManager.SetPlaceAlienMessage();
                //Debug.Log("****   **** TextBubble trovata");
                //messagePanelTansform.gameObject.SetActive(true);
                //alienDialougeText = messagePanelTansform.GetComponentInChildren<TextMeshPro>();
            }

            transform.position = hits[0].pose.position;
            transform.rotation = hits[0].pose.rotation;

            if (!visual.activeInHierarchy)
            {
                visual.SetActive(true);
            }
        }
        else if (!Player.instance.skipped)
        {
            visual.SetActive(false);
        }
    }
}
