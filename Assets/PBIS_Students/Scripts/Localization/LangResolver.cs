﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class LangResolver : MonoBehaviour
{
    private const char Separator = '=';
    private readonly Dictionary<string, string> _lang = new Dictionary<string, string>();

    private SystemLanguage _language;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        ReadProperties();
        Debug.Log(_lang.Count);
        Debug.Log(_lang.Keys.First());
        Debug.Log(_lang.Values.First());
    }

    private void ReadProperties()
    {
        Debug.Log("LangResolver - Loading Language ...");
        _language = Application.systemLanguage;
        Debug.Log("LangResolver - Language: "+_language.ToString());
        //var file = Resources.Load<TextAsset>(_language.ToString());
        var file = Resources.Load<TextAsset>("Lang/"+_language.ToString());
        if (file == null)
        {
            //file = Resources.Load<TextAsset>(SystemLanguage.English.ToString());
            file = Resources.Load<TextAsset>("Lang/"+SystemLanguage.English.ToString());
            _language = SystemLanguage.English;
        }

        foreach (var line in file.text.Split('\n'))
        {
            var prop = line.Split(Separator);
            //Debug.Log("Numero parametri per Linea: " + prop.Length);
            // To accep also comment lines
            if (prop.Length == 2) {
                _lang[prop[0]] = prop[1];
            }
            else
            {
                Debug.Log("Non è un key=Value -Probabilmente è un commento");
            }
            
            
        }
    }

    public void ResolveTexts()
    {
        var allTexts = Resources.FindObjectsOfTypeAll<LangText>();
        foreach (var langText in allTexts)
        {
            try
            {
                var text = langText.GetComponent<Text>();
                text.text = Regex.Unescape(_lang[langText.Identifier]);
            }
            catch (Exception e) { }
        }
    }

    public string GetLocalizedText(string identifier)
    {
        string localizedText = "";
        try
        {
            localizedText = Regex.Unescape(_lang[identifier]);
        }
        catch (Exception e)
        {
            localizedText = "";
        }
        return localizedText;
    }
}