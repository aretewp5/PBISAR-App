using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TokenManager : MonoBehaviour
{
    public TextMeshProUGUI numPointsText;

    // Start is called before the first frame update
    //void Start()
    //{
        
    //}

    // Update is called once per frame
    void Update()
    {
        this.numPointsText.text = Player.instance.points != null ? Player.instance.points.ToString() : "0";
    }
}
