using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

//***   ***
// To manage more Game object with ARFoundation Image traker 
// Object to spawn and target image must have same name.

[RequireComponent(typeof(ARTrackedImageManager))]
public class ImageTraking : MonoBehaviour
{
    [SerializeField]
    private GameObject[] placeablePrefabs;

    private Dictionary<string, GameObject> spawnedPrefabs = new Dictionary<string, GameObject>();
    private ARTrackedImageManager trackedImageManager;
    private GameObject spawnedObject;

    public GameObject spawnedRoutine;

    void Start()
    {
        /*
        foreach (GameObject prefab in placeablePrefabs)
        {
            Debug.Log("****   **** ImageTraking - Awake prefab.name: " + prefab.name);
            GameObject newPrefab = Instantiate(prefab, Vector3.zero, Quaternion.identity);
            newPrefab.name = prefab.name;
            newPrefab.SetActive(false);
            spawnedPrefabs.Add(prefab.name, newPrefab);
        }
        */
    }

    private void Awake()
    {
        // Debug.Log("****   **** ImageTraking - Awake ");
        trackedImageManager = FindObjectOfType<ARTrackedImageManager>();
    }

    private void OnEnable()
    {
        trackedImageManager.trackedImagesChanged += ImageChanged;
        PBISAnimationEvents.OnAnimationFinished += RoutineAnimationFinisced;
    }

    private void OnDisable()
    {
        Debug.Log("ImageTraking - OnDisable");
        trackedImageManager.trackedImagesChanged -= ImageChanged;

        if (spawnedRoutine!=null)
        {
            Debug.Log("ImageTraking - OnDisable spawned ");
            spawnedRoutine.SetActive(false);
            DestroyImmediate(spawnedRoutine);
        }
        PBISAnimationEvents.OnAnimationFinished -= RoutineAnimationFinisced;
    }

    private void ImageChanged (ARTrackedImagesChangedEventArgs eventArgs)
    {
        //Debug.Log("ImageTraking - ImageChanged ");
        foreach (ARTrackedImage trackedImage in eventArgs.added)
        {
            Debug.Log("ImageTraking - ImageChanged - ADD: "+ trackedImage.name);
            //UpdateImage(trackedImage);

            GameObject sectionPanel = GameObject.Find("SectionPanel");
            SectionPanelManager sectionPanelScript = sectionPanel.GetComponent<SectionPanelManager>();
            if (sectionPanelScript.sectionMode.Equals("discovery"))
            {
                MyUpdateImage(trackedImage);
            }
        }

        foreach (ARTrackedImage trackedImage in eventArgs.updated)
        {
            //Debug.Log("****   **** ImageTraking - ImageChanged - UPDATE");
            //UpdateImage(trackedImage);
            //MyUpdateImage(trackedImage
            GameObject sectionPanel = GameObject.Find("SectionPanel");
            SectionPanelManager sectionPanelScript = sectionPanel.GetComponent<SectionPanelManager>();
            if (sectionPanelScript.sectionMode.Equals("discovery"))
            {
                MyUpdateImage(trackedImage);
            }
        }

        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {
            Debug.Log("ImageTraking - ImageChanged - REMOVE");
            spawnedPrefabs[trackedImage.name].SetActive(false);
        }
    }

    /*
    private void UpdateImage(ARTrackedImage trackedImage)
    {
        string name = trackedImage.referenceImage.name;
        Vector2 position = trackedImage.transform.position;

        Debug.Log("****   **** ImageTraking - UpdateImage name: "+ name);

        GameObject prefab = spawnedPrefabs[name];
        prefab.transform.position = position;
        prefab.SetActive(true);

        foreach(GameObject go in spawnedPrefabs.Values)
        {
            if (go.name != name)
            {
                go.SetActive(false); 
            }
        }
    }
    */

    //***   ***
    // Instantiate only the prefab needed
    // TODO: set a class spawnedObject in order to remove previous spawned object if not null
    private void MyUpdateImage(ARTrackedImage trackedImage)
    {
        string name = trackedImage.referenceImage.name;
        Vector3 position = trackedImage.transform.position;
        //Vector2 position = trackedImage.transform.position;
        Quaternion rotation = trackedImage.transform.rotation;
        //Debug.Log("ImageTraking - MyUpdateImage name: " + name);

        TrackingState trackingState = trackedImage.trackingState;
        if (trackingState == TrackingState.Tracking)
        {
            if (spawnedRoutine!=null)
            {
                if (spawnedRoutine.name == name)
                {
                    spawnedRoutine.transform.position = position;
                    spawnedRoutine.transform.rotation = rotation;
                    spawnedRoutine.SetActive(true);
                }
                else
                {
                    // TODO: Delete spawnedRoutine and instanciate newone
                    spawnedRoutine.SetActive(false);
                    DestroyImmediate(spawnedRoutine);
                    foreach (GameObject prefab in placeablePrefabs)
                    {
                        if (prefab.name == name)
                        {
                            Debug.Log("ImageTraking - MyUpdateImage name: " + name);
                            Debug.Log("Position   x:" + position.x + " y:" + position.y + " z:" + position.z);
                            Debug.Log("Position 1 x:" + position.x + " y:" + position.y + " z:" + position.z);
                            Debug.Log("Prefab     x:" + prefab.transform.position.x + " y:" + prefab.transform.position.y + " z:" + prefab.transform.position.z);

                            SoundManagerScript.playSound();
                            //spawnedRoutine = Instantiate(prefab, position, Quaternion.identity);
                            spawnedRoutine = Instantiate(prefab, position, rotation);
                            spawnedRoutine.name = prefab.name;
                            spawnedRoutine.SetActive(true);
                        }
                    }
                }
            }
            else
            {
                foreach (GameObject prefab in placeablePrefabs)
                {
                    if (prefab.name == name)
                    {
                        SoundManagerScript.playSound();
                        //spawnedRoutine = Instantiate(prefab, position, Quaternion.identity);
                        spawnedRoutine = Instantiate(prefab, position, rotation);
                        spawnedRoutine.name = prefab.name;
                        spawnedRoutine.SetActive(true);
                    }
                }
            }
        }
        else
        {
            //Debug.Log("ImageTraking - MyUpdateImage name: " + name + " >>>>> STATE: "+trackingState);
            /*
            if ((spawnedRoutine!=null) && (spawnedRoutine.name == name))
            {
                Debug.Log("ImageTraking - MyUpdateImage DESTROY name: " + name);
                spawnedRoutine.SetActive(false);
                DestroyImmediate(spawnedRoutine);
            }
            */
        }
    }

    private void RoutineAnimationFinisced(string animationRoutine)
    {
        Debug.Log("ImageTraking - RoutineAnimationFinisced: " + animationRoutine);
        if (spawnedRoutine)
        {
            Debug.Log("ImageTraking - RoutineAnimationFinisced  - spawnedRoutine");
            if (spawnedRoutine.name == animationRoutine)
            {
                Debug.Log("ImageTraking - RoutineAnimationFinisced  - destroy");
                spawnedRoutine.SetActive(false);
                DestroyImmediate(spawnedRoutine);
            }
        }
    }

}
