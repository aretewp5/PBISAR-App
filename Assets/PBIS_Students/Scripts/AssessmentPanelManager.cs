using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AssessmentPanelManager : MonoBehaviour
{
    [SerializeField] private GameObject AnswersPanel;
    [SerializeField] private GameObject assessmentMessage;
    [SerializeField] private GameObject answer1;
    [SerializeField] private GameObject answer2;
    [SerializeField] private GameObject answer3;
    [SerializeField] private GameObject answer4;


    private int MaxAnswerNumer = 4;
    private const char Separator = '=';
    private readonly Dictionary<string, string> _dialogs = new Dictionary<string, string>();
    private readonly Dictionary<string, string> _aswers = new Dictionary<string, string>();


    private Animator anim;

    void Start()
    {
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        anim = uiCanvas.GetComponent<Animator>();



    }


    public void SetAssessment(string routineName)
    {
        Debug.Log("****   **** AssessmentPanelManager - routineName: " + routineName);

        

        // Set the Assessment message
        string labelMessage = "Discovery" + routineName;
        if ((_dialogs.ContainsKey(labelMessage)) && (_dialogs[labelMessage] != null))
        {
            TextMeshProUGUI message = assessmentMessage.GetComponentInChildren<TextMeshProUGUI>();
            message.text = _dialogs[labelMessage];
        }


        // Set relative Assessment answers
        Transform buttonTansform;
        TextMeshProUGUI buttontext;
        string answer, labelAnswer, buttonName;
        for (int i = 1; i <= MaxAnswerNumer; i++)
        {
            buttonName = "btnAssessAnswer" + i;
            buttonTansform = AnswersPanel.transform.FindDeepChild(buttonName);
            labelAnswer = "answerDiscovery" + routineName + "-" + i;
            //Debug.Log("****   **** Punto 1");
            //answer = _dialogs[labelAnswer];
            //if ((_dialogs.ContainsKey(labelAnswer)) && (_dialogs[labelAnswer]!=null))
            if ((_aswers.ContainsKey(labelAnswer)) && (_aswers[labelAnswer] != null))
            {
                //Debug.Log("****   **** Punto 2");
                //answer = _dialogs[labelAnswer];
                answer = _aswers[labelAnswer];
                buttonTansform.gameObject.SetActive(true);
                buttontext = buttonTansform.gameObject.GetComponentInChildren<TextMeshProUGUI>();
                //Debug.Log("****   **** Answer " + labelAnswer + ": " + answer);
                buttontext.text = answer;
            }
            else
            {
                //Debug.Log("****   **** Answer " + labelAnswer + " NULLA ");
                buttonTansform.gameObject.SetActive(false);
                break;
            }
        }
    }



    private void LoadDialogText()
    {
        //Debug.Log("****   **** Load dialogs ...");

        var file = Resources.Load<TextAsset>("Text/assessmentData");
        if (file == null)
        {
            // 
        }

        foreach (var line in file.text.Split('\n'))
        {
            var prop = line.Split(Separator);
            // Debug.Log("Numero parametri per Linea: " + prop.Length);
            // To accep also comment lines
            if (prop.Length == 2)
            {
                string key = prop[0];
                if (key.StartsWith("Discovery"))
                {
                    //Debug.Log("Dialog: " + key);
                    _dialogs[prop[0]] = prop[1];
                }
                else
                {
                    //Debug.Log("Answer: " + key);
                    _aswers[prop[0]] = prop[1];
                }
            }
            else
            {
                //Debug.Log("****   **** NOT key=Value - Probably iti is a comment");
            }


        }
    }



    public void AssessmentAnswerButton1Selected()
    {
        //Debug.Log("****   **** EvaluationPanelManager - PositiveButtonSelected");
        IntroPBISActionManager.GainToken();
        anim.Play("ASSESSMENT_OUT");

    }

    public void AssessmentAnswerButton2Selected()
    {
        //Debug.Log("****   **** EvaluationPanelManager - NegativeButtonSelected");
        IntroPBISActionManager.LostToken();
        anim.Play("ASSESSMENT_OUT");

    }

    public void AssessmentAnswerButton3Selected()
    {
        //Debug.Log("****   **** EvaluationPanelManager - NegativeButtonSelected");
        IntroPBISActionManager.LostToken();
        anim.Play("ASSESSMENT_OUT");

    }

    public void AssessmentAnswerButton4Selected()
    {
        //Debug.Log("****   **** EvaluationPanelManager - NegativeButtonSelected");
        IntroPBISActionManager.LostToken();
        anim.Play("ASSESSMENT_OUT");

    }

}
