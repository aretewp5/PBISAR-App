using OrkestraLibImp;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class OrkestraGUI : MonoBehaviour
{
    public TextMeshProUGUI Text_ConnectedUsersLabel;
    public TextMeshProUGUI Text_ConnectedUsers;
    public TextMeshProUGUI Text_Room;
    private OrkestraImpl orkestra;

    // Start is called before the first frame update
    void Start()
    {
        orkestra = FindObjectOfType<OrkestraImpl>();
    }

    // Update is called once per frame
    void Update()
    {
        orkestra?.UpdateSceneText(this.Text_Room, this.Text_ConnectedUsers, this.Text_ConnectedUsersLabel);
    }
}
