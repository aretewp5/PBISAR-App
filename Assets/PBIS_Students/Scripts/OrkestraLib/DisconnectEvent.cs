using OrkestraLibImp;
using TMPro;
using UnityEngine;

public class DisconnectEvent : MonoBehaviour
{
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI disconnectText;
    public int timerInSeconds = 10;
    public int maxAllowedUsers = 2;
    public string onServerDisconnectMessage = "The other student left the room";
    public string onMaxUsersDisconnectMessage = "Too many users in this room";
    public string timerMessage = "You will be disconnected in {0:00}:{1:00} seconds";
    public GameObject gui;
    private float timeRemaining;
    private bool timerIsRunning = false;
    private OrkestraImpl orkestra;

    private void Start()
    {
        timeRemaining = timerInSeconds;
        orkestra = FindObjectOfType<OrkestraImpl>();
        if (!orkestra)
        {
            orkestra = gameObject.AddComponent<OrkestraImpl>();
        }

        orkestra.OnDisconnectEvent = () =>
        {
            gui.SetActive(true);
            timerIsRunning = true;
            disconnectText.text = onServerDisconnectMessage;
        };
    }

    void Update()
    {
        if (orkestra.Users.Keys.Count > maxAllowedUsers && !timerIsRunning)
        {
            gui.SetActive(true);
            disconnectText.text = onMaxUsersDisconnectMessage;
            timerIsRunning = true;
        }
        else if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;
                orkestra?.Exit();
            }
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        timeText.text = string.Format(timerMessage, minutes, seconds);
    }
}
