namespace OrkestraLibImp
{
    using System;
    using System.Linq;
    using OrkestraLib;
    using OrkestraLib.Exceptions;
    using OrkestraLib.Message;
    using OrkestraLib.Plugins;
    using TMPro;
    using UnityEngine;

    public class OrkestraImpl : Orkestra
    {
        public Action<ActMessages> AnimRemoteCharacterFunc { get; internal set; }

        public Action RestartFunc { get; internal set; }

        public Action OnConnectionError { get; internal set; }

        public Action OnDisconnectEvent = null;

        public bool ConnectOnStart = true;

        private bool connected = false;
        private string connectionStatus = "Connecting ...";

        public void Start()
        {
            Debug.Log("----- OrkestraLib Start -----");
            Debug.Log("AgentId/Room: " + this.agentId + " " + this.room);
            ConnectOrkestra();
            DontDestroyOnLoad(this);
        }

        /// <summary>
        /// Connect to server.
        /// </summary>
        public void StartConnection()
        {
            // Use Orkestra SDK with HSocketIOClient
            OrkestraWithHSIO.Install(this, (graceful, message) =>
            {
                // Main thread required by gameObject.
                this.Events.Add(() =>
                {
                    if (!graceful)
                    {
                        connectionStatus = message;
                        OnConnectionError?.Invoke();
                    }
                    else
                    {
                        if (OnDisconnectEvent != null)
                        {
                            OnDisconnectEvent?.Invoke();
                        }
                        else
                        {
                            Exit();
                        }
                    }
                });
            });

            try
            {
                // Start Orkestra
                Connect(() =>
                {
                    connected = true;
                    Debug.Log("Connection ready for new messages");
                });
            }
            catch (ServiceException e)
            {
                Debug.LogError(e.Message);
            }
        }

        public void Exit()
        {
            var results = this.gameObject.GetComponentsInChildren<MonoBehaviour>().OfType<IGameController>();
            if (results.Count() > 0)
            {
                results.First().Exit();
            }
        }

        public void UpdateSceneText(TextMeshProUGUI textRoom, TextMeshProUGUI textUsers, TextMeshProUGUI textUsersLabel)
        {
            if (textRoom != null && textUsers != null && textUsersLabel != null)
            {
                // Orkestra session room id
                textRoom.text = this.room;

                // Orkestra session agent ids
                if (this.Users.Keys.Count > 0)
                {
                    textUsers.text = "";
                    foreach (string a in this.Users.Keys)
                    {
                        textUsers.text += a.ToString() + ", ";
                    }

                    // Remove trailing comma
                    textUsers.text = textUsers.text.Substring(0, textUsers.text.Length - 2);
                }

                if (!connected)
                {
                    textUsersLabel.text = connectionStatus;
                }
                else
                {
                    textUsersLabel.text = "Connected Users:";
                }
            }
        }

        /// <summary>
        /// Establish the Orkestra Connection.
        /// </summary>
        private void ConnectOrkestra()
        {
            // Room name
            this.room = PlayerPrefs.GetString("OrkestraSesion");

            // Agent id
            this.agentId = PlayerPrefs.GetString("agentID");

            // true to erease the events of the room
            ResetRoomAtDisconnect = true;

            // Register the particular messages of the application 
            RegisterEvents(new Type[]
            {
                typeof(HandShakeMessage),
                typeof(ActMessage),
                typeof(TryAgainMessage),
                typeof(ReadyToStart),
                typeof(YourTurn),
                typeof(ObjCoords),
                typeof(AnswersFinished),
            });

            if (ConnectOnStart)
            {
                // Subscribe to Application Events so we receive the events from the Application Channel
                ApplicationEvents += AppEventSubscriber;
                StartConnection();
            }
        }

        /// <summary>
        /// Subscriber to the orkestra application channel.
        /// </summary>
        /// <param name="sender">Sender of the message.</param>
        /// <param name="evt">Event receive.</param>
        private void AppEventSubscriber(object sender, ApplicationEvent evt)
        {
            // Check the type of the message receive
            if (evt.IsEvent(typeof(ActMessage)))
            {
                ActMessage actMessage = new ActMessage(evt.value);

                if (!actMessage.sender.Equals(agentId))
                {
                    Debug.Log("Answer receive: " + actMessage.response);
                    Events.Add(() =>
                    {
                        AnimRemoteCharacterFunc?.Invoke(actMessage.response);
                    });
                }
            }
            else if (evt.IsEvent(typeof(TryAgainMessage)))
            {
                TryAgainMessage tryAgainMessage = new TryAgainMessage(evt.value);
                if (!tryAgainMessage.sender.Equals(agentId))
                {
                    RestartFunc?.Invoke();
                }
            }
        }
    }
}
