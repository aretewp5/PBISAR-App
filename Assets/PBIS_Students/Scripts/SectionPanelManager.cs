using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SectionPanelManager : MonoBehaviour
{
    public static bool debug  = false;

    //[SerializeField] private TMP_InputField _inputFieldUserName;
    [SerializeField] private Button _teacherModeButton;
    [SerializeField] private Button _discoveryModeButton;
    [SerializeField] private Button _practiceModeButton;

    [SerializeField] private Sprite _discoveryActiveSprite;
    [SerializeField] private Sprite _teachActiveSprite;
    [SerializeField] private Sprite _practiceActiveSprite;

    [SerializeField] private Sprite _discoveryGraySprite;
    [SerializeField] private Sprite _teachGraySprite;
    [SerializeField] private Sprite _practiceGraySprite;

    Color activeColor = new Color(118 / 255f, 239 / 255f, 243 / 255f);
    Color unActiveColor = new Color(0f, 0f, 0f);

    static string PracticeSceneName = "Practice";
    //static string DiscoveryScenelName = "DiscoveryPanel";
    static string TeachSceneName = "TeachWithBubble";
    //static string TeachSceneName = "WheelRoutine";
    
    public string sectionMode;

   
    void Start()
    {
        sectionMode = "discovery";
        setDiscoveryMode();
    }


    // ****************** BUTTONS dispatchers ****************
    public void TeachModeSelected()
    {
        if (debug ) Debug.Log("SectionPanelManager - TeachModeSelected");
        PrintSceneList();
        if (!sectionMode.Equals("teach"))
        {
            setTeacherMode();
            //SceneManager.LoadSceneAsync("Teach", LoadSceneMode.Additive);

            //PlayerPrefs.SetString("lessonSelected", "1-");
            //PlayerPrefs.Save();

            SceneManager.LoadSceneAsync(TeachSceneName, LoadSceneMode.Additive);
        }
    }

    public void DiscoveryModeSelected()
    {
        if (debug ) Debug.Log("SectionPanelManager - DiscoveryModeSelected");
        PrintSceneList();
        setDiscoveryMode();
    }


    public void PracticeModeSelected()
    {
        if (debug ) Debug.Log("SectionPanelManager - PracticeModeSelected");
        PrintSceneList();
        setPracticeMode();
        SceneManager.LoadSceneAsync("Practice", LoadSceneMode.Additive);

    }
    // *****************************************************





    // **** TEACH Mode ****
    private void setTeacherMode()
    {
        if (sectionMode.Equals("teach"))
        {
            if (debug ) Debug.Log("SectionPanelManager - Teach");
        }
        else if (sectionMode.Equals("discovery"))
        {
            if (debug ) Debug.Log("SectionPanelManager - NON Teach - Discovery");
            unloadDiscovery();
            sectionMode = "teach";
        }
        else if (sectionMode.Equals("practice"))
        {
            if (debug ) Debug.Log("SectionPanelManager - NON Teach - Practice");
            unloadPractice();
            sectionMode = "teach";
        }
        _teacherModeButton.transform.FindDeepChild("iconImage").gameObject.GetComponentInChildren<Image>().sprite = _teachActiveSprite;
        _discoveryModeButton.transform.FindDeepChild("iconImage").gameObject.GetComponentInChildren<Image>().sprite = _discoveryGraySprite;
        _practiceModeButton.transform.FindDeepChild("iconImage").gameObject.GetComponentInChildren<Image>().sprite = _practiceGraySprite;

        _teacherModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = activeColor;
        _discoveryModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = unActiveColor;
        _practiceModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = unActiveColor;



    }


    // **** DISCOVERY Mode ****
    private void setDiscoveryMode()
    {
        //   -  20/10/2021
        GameObject uiCanvas = GameObject.Find("UI Canvas");

        Transform discoveryPanelTansform = uiCanvas.transform.FindDeepChild("DiscoveryPanel");
        if (discoveryPanelTansform)
        {
            discoveryPanelTansform.gameObject.SetActive(true);
        }

        Transform messagePanelTansform = uiCanvas.transform.FindDeepChild("MessagePanel");
        if (messagePanelTansform)
        {
            messagePanelTansform.gameObject.SetActive(true);
            GameObject messagePanel = messagePanelTansform.gameObject;
            messagePanelManager messageManager = (messagePanelManager)messagePanel.GetComponent(typeof(messagePanelManager));
            if (messageManager)
            {
                messageManager.SetDiscoveryMessage();
            }
        }
            
        if (sectionMode.Equals("discovery"))
        {
            if (debug ) Debug.Log("SectionPanelManager - Discovery");
        }
        else if (sectionMode.Equals("teach"))
        {
            if (debug ) Debug.Log("SectionPanelManager - NON Practice - Teach");
            unloadTeach();
            sectionMode = "discovery";
        }
        else if (sectionMode.Equals("practice"))
        {
            if (debug ) Debug.Log("SectionPanelManager - NON Discovery - Practice");
            unloadPractice();
            sectionMode = "discovery";
        }
        _teacherModeButton.transform.FindDeepChild("iconImage").gameObject.GetComponentInChildren<Image>().sprite = _teachGraySprite;
        _discoveryModeButton.transform.FindDeepChild("iconImage").gameObject.GetComponentInChildren<Image>().sprite = _discoveryActiveSprite;
        _practiceModeButton.transform.FindDeepChild("iconImage").gameObject.GetComponentInChildren<Image>().sprite = _practiceGraySprite;

        _teacherModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = unActiveColor;
        _discoveryModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = activeColor;
        _practiceModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = unActiveColor;
    }




    // **** PACTICE Mode ****
    private void setPracticeMode()
    {
        if (sectionMode.Equals("practice"))
        {
            if (debug ) Debug.Log("SectionPanelManager - Practice");
        }
        else if (sectionMode.Equals("discovery"))
        {
            if (debug ) Debug.Log("SectionPanelManager - NON Practice - Discovery");
            // if (checkSceneLoaded("practice"))
            //     SceneManager.UnloadSceneAsync("practice");
            unloadDiscovery();
            sectionMode = "practice";
            
        }
        else if (sectionMode.Equals("teach"))
        {
            if (debug ) Debug.Log("SectionPanelManager - NON Practice - Teach");
            unloadTeach();
            sectionMode = "practice";
        }

        _teacherModeButton.transform.FindDeepChild("iconImage").gameObject.GetComponentInChildren<Image>().sprite = _teachGraySprite;
        _discoveryModeButton.transform.FindDeepChild("iconImage").gameObject.GetComponentInChildren<Image>().sprite = _discoveryGraySprite;
        _practiceModeButton.transform.FindDeepChild("iconImage").gameObject.GetComponentInChildren<Image>().sprite = _practiceActiveSprite;

        //_practiceModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = new Color(118 / 255f, 239 / 255f, 243 / 255f);
        _teacherModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = unActiveColor;
        _discoveryModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = unActiveColor;
        _practiceModeButton.transform.FindDeepChild("textLabel").gameObject.GetComponentInChildren<Text>().color = activeColor;
    }






    // **************** UNLOAD methods ****************

    private void unloadTeach()
    {
        if (checkSceneLoaded(TeachSceneName))
        {
            if (debug ) Debug.Log("SectionPanelManager - unloadTeach - UnloadScene Teach");
            SceneManager.UnloadSceneAsync(TeachSceneName);
        }

        if (checkSceneLoaded("TeachWithBubble"))
        {
            if (debug ) Debug.Log("SectionPanelManager - unloadTeach - UnloadScene Player");
            SceneManager.UnloadSceneAsync("TeachWithBubble");
        }

        if (checkSceneLoaded("Player"))
        {
            if (debug ) Debug.Log("SectionPanelManager - unloadTeach - UnloadScene Player");
            SceneManager.UnloadSceneAsync("Player");
        }

        if (checkSceneLoaded("RoutineSelection"))
        {
            if (debug ) Debug.Log("SectionPanelManager - unloadTeach - UnloadScene RoutineSelection");
            SceneManager.UnloadSceneAsync("RoutineSelection");
        }
    }



    private void unloadDiscovery()
    {
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        Transform discoveryPanelTansform = uiCanvas.transform.FindDeepChild("DiscoveryPanel");
        if (discoveryPanelTansform)
        {
            discoveryPanelTansform.gameObject.SetActive(false);
        }
        Transform messagePanelTansform = uiCanvas.transform.FindDeepChild("MessagePanel");
        if (messagePanelTansform)
        {
            messagePanelTansform.gameObject.SetActive(false);
            GameObject messagePanel = messagePanelTansform.gameObject;
            messagePanelManager messageManager = (messagePanelManager)messagePanel.GetComponent(typeof(messagePanelManager));
            if (messageManager)
            {
                messageManager.ClearDiscoveryMessage();
            }
        }
        //TODO: to be fixed in order to hide or delete the animation when change section
        if (debug ) Debug.Log("SectionPanelManager - unloadDiscovery - destroy RoutineAnimation");

        foreach(var gameObj in FindObjectsOfType(typeof(GameObject)) as GameObject[])
        {
            if (gameObj.name == "GreetingsRoutine")
            {
                //Do something...
                if (debug ) Debug.Log("SectionPanelManager - unloadDiscovery - GreetingsRoutine found");
            }
        }

        GameObject routine = GameObject.Find("GreetingsRoutine");
        if (routine)
        {
            if (debug ) Debug.Log("SectionPanelManager - unloadDiscovery - GreetingsRoutine");
            routine.SetActive(false);
            DestroyImmediate(routine);
        }
    }



    private void unloadPractice()
    {
        if (checkSceneLoaded(PracticeSceneName))
        {
            if (debug ) Debug.Log("****   **** SectionPanelManager - unloadPractice - UnloadScene practice");
            SceneManager.UnloadSceneAsync(PracticeSceneName);
        }
    }



    private bool checkSceneLoaded (string sceneName)
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            // Debug.Log("****   **** SectionPanelManager - checkSceneLoaded - sceneName: "+ scene.name);
            if (scene.name == sceneName && scene.isLoaded)
            {
                return true;
            }
        }
        return false;
    }


    private void PrintSceneList()
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (debug ) Debug.Log("****   **** SectionPanelManager - checkSceneLoaded - sceneName: "+ scene.name);
            //if (scene.name == sceneName && scene.isLoaded)
        }
    }


}

