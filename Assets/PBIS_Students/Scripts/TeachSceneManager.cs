using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MirageXR;
using Network = MirageXR.Network;
using UnityEngine.SceneManagement;

public class TeachSceneManager : MonoBehaviour
{
    public static bool debug = true; 

    private Animator anim;
    string username = "student_pbis";
    string password = "AreteMoodle2021#";

    

    public void Start()
    {
        //SceneManager.sceneUnloaded += OnSceneUnloaded;
        if (debug) Debug.Log("****   **** TeachSceneManager - Started");
    }


    void OnEnable()
    {
        if (debug) Debug.Log("****   **** TeachSceneManager OnEnable");
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    void OnDisable()
    {
        if (debug) Debug.Log("****   **** TeachSceneManager OnDisable");
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }


    private void OnSceneUnloaded(Scene current)
    {
        if (debug) Debug.Log("****   **** TeachSceneManager - OnSceneUnloaded: " + current);
        //Resources.UnloadUnusedAssets();
    }

    void Awake()
    {
        if (debug ) Debug.Log($"****   **** TeachSceneManager - Awake 1");
        //Login();
        if (debug ) Debug.Log($"****   **** TeachSceneManager - Awake 2");
        GameObject uiCanvas = GameObject.Find("TeachCanvas");
        if (debug ) Debug.Log($"****   **** TeachSceneManager - Awake 3");
        if (uiCanvas)
        {
            if (debug ) Debug.Log($"****   **** TeachSceneManager - Awake 4");
            //Debug.Log("****   **** TeachSceneManager - L'HO TROVATO!");
            anim = uiCanvas.GetComponent<Animator>();
            //anim.Play("ROUTINE_IN");
        }

        if (debug ) Debug.Log($"****   **** TeachSceneManager - Awake 5");
    }

    private async void Login()
    {
        if (debug ) Debug.Log($"****   **** TeachSceneManager - Login 1 ");
        if (debug ) Debug.Log($"****   **** TeachSceneManager - Login - username:"+username+ " password="+password+"   domain:"+ DBManager.domain);
        var (result, response) = await Network.LoginRequestAsync(username, password, DBManager.domain);
        if (debug ) Debug.Log($"****   **** TeachSceneManager - Login 2");
        
        //ModalWindowView.Instance.HideWaiter();
        if (result && response.StartsWith("succeed"))
        {
            if (debug ) Debug.Log($"****   **** TeachSceneManager - Login 3");
            OnLoginSucceed(username, password, response.Split(',')[1]);
            if (debug ) Debug.Log("****   **** TeachSceneManager - Moodle userId: " + await RootObject.Instance.moodleManager.GetUserId());

            // **************   *****************
            // Da implementare per prendere il nome dell'utente collegato
            // Debug.Log("Moodle Name: "+await MoodleManager.Instance.GetUserFullName());
 

        }
        if (debug ) Debug.Log($"****   **** TeachSceneManager - Login 4");
    }

    private void OnLoginSucceed(string username, string password, string token)
    {
        if (debug ) Debug.Log($"****   **** TeachSceneManager - OnLoginSucceed OnLoginSucceed");
        DBManager.token = token;
        DBManager.username = username;
        if (debug ) Debug.Log($"****   **** TeachSceneManager - {username} logged in successfully.");

        
        GameObject alienSpawned = GameObject.Find("AlienBubbled(Clone)");
        if (debug ) Debug.Log($"****   **** TeachSceneManager - OnLoginSucceed Punto1.");
        if (alienSpawned)
            alienSpawned.SetActive(false);
        if (debug ) Debug.Log($"****   **** TeachSceneManager - OnLoginSucceed Punto2.");
        RoutineListView.Instance.UpdateListView();
        if (debug ) Debug.Log($"****   **** TeachSceneManager - OnLoginSucceed Punto3.");
        IntroDialougeManager.Instance.PostLoginSucceed();
        if (debug ) Debug.Log($"****   **** TeachSceneManager - OnLoginSucceed Punto4.");
        //ActivityListView.Instance.UpdateListView();
        /*
        if (_toggleRemember.isOn)
        {
            LocalFiles.SaveUsernameAndPassword(username, password);
        }
        else
        {
            LocalFiles.RemoveUsernameAndPassword();
        }*/

        
    }


}
