using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DiscoveryPanelManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _tokens;
    [SerializeField] private TextMeshProUGUI _points;
    [SerializeField] private Transform _LeaderBoardContentPanel;
    [SerializeField] private Transform _infoPanel;
    [SerializeField] private Transform _messagePanel;

    // Section for ASSESSMENT
    [SerializeField] private GameObject AnswersPanel;
    [SerializeField] private GameObject assessmentMessage;
    [SerializeField] private GameObject answer1;
    [SerializeField] private GameObject answer2;
    [SerializeField] private GameObject answer3;
    [SerializeField] private GameObject answer4;
    private const int MaxAnswerNumer = 4;
    private const char Separator = '=';
    private readonly Dictionary<string, PBISQuestion> _dialogs = new Dictionary<string, PBISQuestion>();
    private PBISQuestion activeQuestion = null;



    bool isWaiting = false;
    int numTokens;
    private Animator anim;

    void Start()
    {
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        anim = uiCanvas.GetComponent<Animator>();
        
        foreach (AnimationClip ac in anim.runtimeAnimatorController.animationClips)
        {
            Debug.Log("DiscoveryPanelManager - Animation: " + ac.name);
        }
        
        // LoadDialogText();
        LoadDialogJasonFile();
        _infoPanel.gameObject.SetActive(false);
    }

    void Update()
    {

    }

    void Awake()
    {
        Debug.Log("DiscoveryPanelManager - Awake");
        // numTokens = PlayerPrefs.GetInt("tokesgained", 0);
        // updateTokens();
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        anim = uiCanvas.GetComponent<Animator>();

        updatePointsonView();
    }

    void OnEnable()
    {
        Debug.Log("DiscoveryPanelManager - OnEnable");
        // IntroPBISActionManager.onTokenGained += GainedTokenEvent;
        // IntroPBISActionManager.onTokenLost += LostTokenEvent;

        PBISAnimationEvents.OnAnimationFinished += RoutineAnimationFinisced;
    }


    void OnDisable()
    {
        Debug.Log("DiscoveryPanelManager - OnDisable");
        //IntroPBISActionManager.onTokenGained -= GainedTokenEvent;
        //IntroPBISActionManager.onTokenLost -= LostTokenEvent;

        PBISAnimationEvents.OnAnimationFinished -= RoutineAnimationFinisced;
        destroySpawnedRoutine();
    }


    public void infoButtonSelected()
    {
        Debug.Log("DiscoveryPanelManager - infoButtonSelected");
        _messagePanel.gameObject.SetActive(false);
        _infoPanel.gameObject.SetActive(true);
    }


    public void closeInfoButtonSelected()
    {
        Debug.Log("DiscoveryPanelManager - closeInfoButtonSelected");
        _infoPanel.gameObject.SetActive(false);
    }

    // Delete it
    public void LeaderBoardButtonSelected()
    {
        /*
         * GameObject uiCanvas = GameObject.Find("UI Canvas");
        Animator anim = uiCanvas.GetComponent<Animator>();
        */
        // TODO DELETE this code it is just for debug


        anim.Play("EVALUATION_IN");
        /*
        if (_LeaderBoard)
        {
            if (_LeaderBoard.gameObject.activeSelf)
            {
                _LeaderBoard.gameObject.SetActive(false);
            }
            else
            {
                _LeaderBoard.gameObject.SetActive(true);
            }
        }
        */
    }


    public void tokenButtonSelected()
    {

        Debug.Log("DiscoveryPanelManager - tokenButtonSelected ");


        // LeaderBoard 
        anim.Play("LEADERBOARDNEW_IN");
        //_LeaderBoardBackgroudPanel.gameObject.SetActive(true);
        LeaderBoardManager leaderBoardManager = _LeaderBoardContentPanel.gameObject.GetComponent<LeaderBoardManager>();
        leaderBoardManager.UpdatePodium();
        
        
        Debug.Log("DiscoveryPanelManager - tokenButtonSelected LEADERBOARDNEW_IN");

        // Only for debug
        /*
        SetAssessment("DiscoveryLesson3NE1");
        SetInteractionAssessmentPanel(true,0);
        anim.Play("ASSESSMENT_Panel_IN");
        */
    }




    //private void RoutineAnimationFinisced(Transform animationRoutine)
    private void RoutineAnimationFinisced(string animationRoutine)
    {
        // yield return new WaitForSeconds(1.0f);

        //Debug.Log("DiscoveryPanelManager - RoutineAnimationFinisced Animation: " + animationRoutine.name);
        Debug.Log("DiscoveryPanelManager - RoutineAnimationFinisced Animation: " + animationRoutine);

        /*
         * GameObject uiCanvas = GameObject.Find("UI Canvas");
        Animator anim = uiCanvas.GetComponent<Animator>();
        */

        //SetAssessment(animationRoutine.name);
        SetAssessment(animationRoutine);

        // anim.Play("EVALUATION_IN");
        // _AssessmentPanel.gameObject.SetActive(true);
        // anim.Play("ASSESSMENT_IN");

        SetInteractionAssessmentPanel(true,0);

        anim.Play("ASSESSMENT_Panel_IN");
    }




    // Section for ASSESSMENT
    // private readonly Dictionary<string, string> _aswers = new Dictionary<string, string>();



    public void SetAssessment(string routineName)
    {
        Debug.Log("DiscoveryPanelManager - SetAssessment - routineName: " + routineName);
        // Set the Assessment message
        if ((_dialogs.ContainsKey(routineName)) && (_dialogs[routineName] != null))
        {
            Debug.Log("DiscoveryPanelManager - SetAssessment - routineName trovata ");
            TextMeshProUGUI message = assessmentMessage.GetComponentInChildren<TextMeshProUGUI>();
            PBISQuestion question = _dialogs[routineName];
            message.text = question.text;
            activeQuestion = question;
            PBISAswer[] answers = question.answers;
            if ((answers != null) && (answers.Length > 0))
            {
                answer1.transform.gameObject.SetActive(true);
                TextMeshProUGUI buttonAnswer1 = answer1.transform.gameObject.GetComponentInChildren<TextMeshProUGUI>();
                buttonAnswer1.text = answers[0].text;
                SetButtonColor("", answer1); // set the default color
                if (answers.Length > 1)
                {
                    answer2.transform.gameObject.SetActive(true);
                    TextMeshProUGUI buttonAnswer2 = answer2.transform.gameObject.GetComponentInChildren<TextMeshProUGUI>();
                    buttonAnswer2.text = answers[1].text;
                    SetButtonColor("", answer2); // set the default color
                    if (answers.Length > 2)
                    {
                        answer3.transform.gameObject.SetActive(true);
                        TextMeshProUGUI buttonAnswer3 = answer3.transform.gameObject.GetComponentInChildren<TextMeshProUGUI>();
                        buttonAnswer3.text = answers[2].text;
                        SetButtonColor("", answer3); // set the default color
                        if (answers.Length > 3)
                        {
                            answer4.transform.gameObject.SetActive(true);
                            TextMeshProUGUI buttonAnswer4 = answer4.transform.gameObject.GetComponentInChildren<TextMeshProUGUI>();
                            buttonAnswer4.text = answers[3].text;
                            SetButtonColor("", answer4); // set the default color
                        }
                        else
                        {
                            answer4.transform.gameObject.SetActive(false);
                        }
                    }
                    else
                    {
                        answer3.transform.gameObject.SetActive(false);
                    }
                }
                else
                {
                    answer2.transform.gameObject.SetActive(false);
                }
            }
            else
            {
                answer1.transform.gameObject.SetActive(false);
            }
        }
    }





    private void LoadDialogJasonFile()
    {
        Debug.Log("DiscoveryPanelManager - LoadDialogJasonFile");

        SystemLanguage _language = Application.systemLanguage;
        Debug.Log("DiscoveryPanelManager - LoadDialogJasonFile - Language: " + _language.ToString());

        var jsonFile = Resources.Load<TextAsset>("Text/PBISQuestions" + _language.ToString());

        if (jsonFile == null)
        {
            jsonFile = Resources.Load<TextAsset>("Text/PBISQuestions" + SystemLanguage.English.ToString());
        }

 
        //var jsonFile = Resources.Load<TextAsset>("Text/PBISQuestions");
        Debug.Log("LoadDialogJasonFile - json: " + jsonFile.text);

        PBISQuestions questionsInJson = JsonUtility.FromJson<PBISQuestions>(jsonFile.text);

        foreach (PBISQuestion question in questionsInJson.questions)
        {
            _dialogs[question.name] = question;
            Debug.Log("Found Routine: " + question.name + " - question: " + question.text + "  Number of answers: " + question.answers.Length);
        }
    }

    [System.Serializable]
    public class PBISQuestion
    {
        public string name;
        public string text; // The PBIS question

        // public string[] answers;
        public PBISAswer[] answers;
        // public string rightAnswer;   // Index of the right answer
        public string penality;

        public bool correctAnswerDone = false;
        public bool firstAttemp = true;
    }

    public class PBISQuestions
    {
        public PBISQuestion[] questions;
    }


    [System.Serializable]
    public class PBISAswer
    {
        public string text;
        public string points;
        public string value;
    }



    private void SetInteractionAssessmentPanel(bool isActive, int value)
    {
        Button myButton;
        if (isActive)
        {
            myButton = answer1.GetComponent<Button>();
            myButton.interactable = true;
            myButton = answer2.GetComponent<Button>();
            myButton.interactable = true;
            myButton = answer3.GetComponent<Button>();
            myButton.interactable = true;
            myButton = answer4.GetComponent<Button>();
            myButton.interactable = true;
            isWaiting = false;
        }
        else
        {
            switch (value)
            {
                case 1:
                    myButton = answer1.GetComponent<Button>();
                    myButton.interactable = true;
                    myButton = answer2.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer3.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer4.GetComponent<Button>();
                    myButton.interactable = false;
                    break;
                case 2:
                    myButton = answer1.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer2.GetComponent<Button>();
                    myButton.interactable = true;
                    myButton = answer3.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer4.GetComponent<Button>();
                    myButton.interactable = false;
                    break;

                case 3:
                    myButton = answer1.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer2.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer3.GetComponent<Button>();
                    myButton.interactable = true;
                    myButton = answer4.GetComponent<Button>();
                    myButton.interactable = false;
                    break;

                case 4:
                    myButton = answer1.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer2.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer3.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer4.GetComponent<Button>();
                    myButton.interactable = true;
                    break;

                default :
                    myButton = answer1.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer2.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer3.GetComponent<Button>();
                    myButton.interactable = false;
                    myButton = answer4.GetComponent<Button>();
                    myButton.interactable = false;
                    break;

            }
           
        }
    }


    
    public void AssessmentAnswerButton1Selected()
    {
        Debug.Log("DiscoveryPanelManager - AssessmentAnswerButton1Selected ");
        SetInteractionAssessmentPanel(false,1);
        if (!isWaiting)
        {
            Debug.Log("DiscoveryPanelManager - AssessmentAnswerButton1Selected - NOT Waiting");
            isWaiting = true;
            updatePoints(CheckAnswer(1, answer1));
        }

        //Debug.Log("DiscoveryPanelManager - AssessmentAnswerButton1Selected");
        //IntroPBISActionManager.GainToken();
        //numTokens += CheckAnswer(1);
        // updatePoints(CheckAnswer(1, answer1));
        //anim.Play("ASSESSMENT_OUT");
        destroySpawnedRoutine();

    }

    public void AssessmentAnswerButton2Selected()
    {
        SetInteractionAssessmentPanel(false,2);
        if (!isWaiting)
        {
            isWaiting = true;
            updatePoints(CheckAnswer(2, answer2));
        }
        //Debug.Log("DiscoveryPanelManager - AssessmentAnswerButton2Selected");
        //IntroPBISActionManager.LostToken();
        //numTokens += CheckAnswer(2);
        //updatePoints(CheckAnswer(2, answer2));
        //anim.Play("ASSESSMENT_OUT");
        destroySpawnedRoutine();
    }

    public void AssessmentAnswerButton3Selected()
    {
        SetInteractionAssessmentPanel(false,3);
        if (!isWaiting)
        {
            isWaiting = true;
            updatePoints(CheckAnswer(3, answer3));
        }
        //Debug.Log("DiscoveryPanelManager - AssessmentAnswerButton3Selected");
        //IntroPBISActionManager.LostToken();
        //numTokens += CheckAnswer(3);
        //updatePoints(CheckAnswer(3, answer3));
        //anim.Play("ASSESSMENT_OUT");
        destroySpawnedRoutine();
    }

    public void AssessmentAnswerButton4Selected()
    {
        SetInteractionAssessmentPanel(false,4);
        if (!isWaiting)
        {
            isWaiting = true;
            updatePoints(CheckAnswer(4, answer4));
        }
        //Debug.Log("DiscoveryPanelManager - AssessmentAnswerButton4Selected");
        //IntroPBISActionManager.LostToken();
        //numTokens += CheckAnswer(4);
        //updatePoints(CheckAnswer(4, answer4));
        // anim.Play("ASSESSMENT_OUT");
        destroySpawnedRoutine();
    }


    private void destroySpawnedRoutine()
    {
        Debug.Log("DiscoveryPanelManager - destroySpawnedRoutine - spawned ... ");
        GameObject arSessionOrigin = GameObject.Find("AR Session Origin");
        ImageTraking imageTrakingScript = arSessionOrigin.GetComponent<ImageTraking>();
        if (imageTrakingScript.spawnedRoutine !=null)
        {
            Debug.Log("DiscoveryPanelManager - destroySpawnedRoutine - spawned NOT null ");
            imageTrakingScript.spawnedRoutine.SetActive(false);
            DestroyImmediate(imageTrakingScript.spawnedRoutine);
            Debug.Log("DiscoveryPanelManager - destroySpawnedRoutine - spawned Destroyed");
        }
    }

    private void updatePoints(UserModel myUser)
    {
        Debug.Log("DiscoveryPanelManager - updatePoints - #Points: " + Player.instance.points + " poiunts gained: " + myUser.points);
        // Update points on server
        string ulrString = "https://arete.pa.itd.cnr.it/arete/rest/private/assessment";
        Debug.Log("DiscoveryPanelManager - updatePoints - ulrString: " + ulrString);
        if ((Player.instance.xAuthToken != null) && (Player.instance.xAuthToken.Length > 0))
        //if (PlayerPrefs.HasKey("xAuthToken"))
        {

            StartCoroutine(updatePointOnServer(ulrString, myUser, Player.instance.xAuthToken));
        }
    }


    IEnumerator updatePointOnServer(string ulrString, UserModel myUser, string xAuthToken)
    {
        Debug.Log("DiscoveryPanelManager - updatePointOnServer ");

        //string dataJsonString = JsonConvert.ToString(myUser);
        var dataJsonString = JsonConvert.SerializeObject(myUser);
        Debug.Log("Position as JSON: " + dataJsonString);

        Debug.Log("SceneManager - SiletLoginPbisAppUser - urlString: " + ulrString);
        var request = new UnityWebRequest(ulrString, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(dataJsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("X-Auth", xAuthToken);
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("DiscoveryPanelManager - updatePointOnServer - Error: " + request.error);
        }
        else
        {
            Debug.Log("DiscoveryPanelManager - updatePointOnServer - complete!");
            Player.instance.points =  myUser.points;
            Player.instance.routineCollection = myUser.routinecollection;
            yield return new WaitForSeconds(2);
            updatePointsonView();

            //anim.Play("ASSESSMENT_OUT");

            anim.Play("ASSESSMENT_Panel_OUT");
            SetInteractionAssessmentPanel(true, 0);
        }
    }

    /*
    private void updatePoints(int points)
    {
        Debug.Log("DiscoveryPanelManager - updatePoints - #Points: " + Player.instance.points + " poiunts gained: " + points);
        // Update points on server
        string ulrString = "https://arete.pa.itd.cnr.it/arete/rest/private/pointsupdate";
        Debug.Log("DiscoveryPanelManager - updatePoints - ulrString: " + ulrString);
        if ((Player.instance.xAuthToken!=null)&& (Player.instance.xAuthToken.Length>0))
        //if (PlayerPrefs.HasKey("xAuthToken"))
        {
            StartCoroutine(updatePointOnServer(ulrString, points, Player.instance.xAuthToken));
        }
    }

    IEnumerator updatePointOnServer(string url, int points, string xAuthToken)
    {
        Debug.Log("DiscoveryPanelManager - updatePointOnServer - points: " + points);
        WWWForm form = new WWWForm();
        form.AddField("points", points.ToString());

        UnityWebRequest www = UnityWebRequest.Post(url, form);
        www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        www.SetRequestHeader("X-Auth", xAuthToken);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("DiscoveryPanelManager - updatePointOnServer - Error: "+ www.error);
        }
        else
        {
            Debug.Log("DiscoveryPanelManager - updatePointOnServer - complete!");
            Player.instance.points = Player.instance.points + points;
            yield return new WaitForSeconds(2);
            updatePointsonView();

            //anim.Play("ASSESSMENT_OUT");
           
            anim.Play("ASSESSMENT_Panel_OUT");
            SetInteractionAssessmentPanel(true,0);
            //_AssessmentPanel.gameObject.SetActive(false);
        }
    }
    */
    private void updatePointsonView()
    {
        Debug.Log("DiscoveryPanelManager - updatePointsonView");
        _points.text = Player.instance.points.ToString();
    }

    // private int CheckAnswer(int index, GameObject answerObj)
    private UserModel CheckAnswer(int index, GameObject answerObj)
    {
        Debug.Log("DiscoveryPanelManager - CheckAnswer ");
        UserModel myUser = new UserModel();
        myUser.username = Player.instance.username;
        myUser.points = Player.instance.points;
        myUser.routinecollection = Player.instance.routineCollection;

        int points = 0;
        if (index > 0)
        {
            if ((activeQuestion != null) && (activeQuestion.answers != null))
            {
                if (activeQuestion.answers.Length >= index)
                {
                    PBISAswer answer = activeQuestion.answers[index - 1];
                    // if ((Player.instance.routineCollection != null) && (Player.instance.routineCollection[activeQuestion.name] != null) && (Player.instance.routineCollection[activeQuestion.name].Length>0))
                    // if ((myUser.routinecollection != null) && (myUser.routinecollection[activeQuestion.name] != null) && (myUser.routinecollection[activeQuestion.name].Length > 0))
                    if ((myUser.routinecollection != null) && (myUser.routinecollection.ContainsKey(activeQuestion.name)) && (myUser.routinecollection[activeQuestion.name] != null) && (myUser.routinecollection[activeQuestion.name].Length > 0))
                        {
                        // if ((!Player.instance.routineCollection[activeQuestion.name].Contains("correct")) && (answer.value.StartsWith("correct")))
                        if ((!myUser.routinecollection[activeQuestion.name].Contains("correct")) && (answer.value.StartsWith("correct")))
                        {
                            // it is NOT the first time - AND previous answers was NOT correct AND the actual answer is CORRECT
                            var pointsParsed = int.TryParse(answer.points, out points);
                            int penality;
                            var penalityParsed = int.TryParse(activeQuestion.penality, out penality);
                            points = points - penality;

                            int pointsGained;
                            string pointsGainedString = Player.instance.routineCollection[activeQuestion.name];
                            var pointsGainedParsed = int.TryParse(pointsGainedString.Substring(0, 1), out pointsGained);
                            pointsGained = pointsGained + points;
                            // Player.instance.routineCollection[activeQuestion.name] = pointsGained.ToString() + "correct";
                            myUser.routinecollection[activeQuestion.name] = pointsGained.ToString() + "correct";

                            myUser.points = myUser.points + points;
                        }
                        else
                        {
                            // it is NOT the first time -  previous answers was NOT correct OR the actual answer not correct
                            Debug.Log("DiscoveryPanelManager - CheckAnswer - visual feedback NO points- Value: " + answer.value);
                        }
                    }
                    else
                    {
                        // FIRST Attempt
                        // if (Player.instance.routineCollection != null)
                        if (myUser.routinecollection == null)
                        {
                            // Player.instance.routineCollection = new Dictionary<string, string>();
                            myUser.routinecollection = new Dictionary<string, string>();
                        }
                        var pointsParsed = int.TryParse(answer.points, out points);
                        // Debug.Log("DiscoveryPanelManager - CheckAnswer - firstAttemp points: " + points);

                        if (answer.value.StartsWith("correct"))
                        {
                            // Player.instance.routineCollection[activeQuestion.name] = points.ToString() + "correct"; // 3CORRECT
                            myUser.routinecollection[activeQuestion.name] = points.ToString() + "correct"; // 3CORRECT
                        }
                        else
                        {
                            // Player.instance.routineCollection[activeQuestion.name] = points.ToString(); // 0 or 1
                            myUser.routinecollection[activeQuestion.name] = points.ToString(); // 0 or 1
                        }
                        myUser.points = myUser.points + points;
                    }
                    SetButtonColor(answer.value, answerObj);
                }
            }
        }
        return myUser;
    }


    public void SetButtonColor(string aswerValue , GameObject answerObj)
    {
        switch (aswerValue)
        {
            case "correct":
                answerObj.transform.gameObject.GetComponent<Image>().color = Color.green;
                break;
            case "partiallyCorrect":
                // answerObj.transform.gameObject.GetComponent<Image>().color = Color.magenta;
                answerObj.transform.gameObject.GetComponent<Image>().color = new Color32(255, 128, 0, 255);
                break;
            case "wrong":
                answerObj.transform.gameObject.GetComponent<Image>().color = Color.red;
                break;
            default:
                answerObj.transform.gameObject.GetComponent<Image>().color = Color.white;
                break;
        }
    }
}


