using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PracticeSelection : MonoBehaviour
{
    private bool sceneOpened;
    private GameObject groupErrorText;
    private int selectedGroup;

    public GameObject panelTitle;
    public GameObject panelInfo;
    public Button infoButton;
    public Button bullyingButton;
    public Button handshakeButton;
    public Button organizeTidyButton;
    public Button organizeUntidyButton;
    public GameObject dropdown;
    internal GameObject messagePanel; // MessagePanel from the 'IntroPBIS' scene

    private void Start()
    {
        Debug.Log("Practice Start");
        
        this.sceneOpened = false;
        this.panelTitle = GameObject.Find("Text_Title");
        this.groupErrorText = GameObject.Find("RoomError");
        this.groupErrorText.SetActive(false);
        this.panelInfo.SetActive(false);
        this.selectedGroup = 0;

        // In case any message from another part of the app is shown, hide it
        this.messagePanel = GameObject.Find("MessagePanel");
        if (this.messagePanel != null)
        {
            this.messagePanel.SetActive(false);
        }
    }

    private bool SetGroup()
    {
        // Validate data
        if (this.selectedGroup == null || this.selectedGroup <= 0)
        {
            Debug.Log("Error -> A group must be selected.");
            this.groupErrorText.SetActive(true);
            return false;
        }
        if (string.IsNullOrEmpty(Player.instance.name))
        {
            Debug.Log("Error -> The player username is null or empty.");
            this.groupErrorText.SetActive(true);
            return false;
        }

        // Set data
        PlayerPrefs.SetString("agentID", Player.instance.name);
        PlayerPrefs.SetString("OrkestraSesion", "Group " + (this.selectedGroup));

        CloseCanvas(); // Hide PBIS app menu (bottom of the screen with 3 buttons)
        PlaceOnPlane p = FindObjectOfType<PlaceOnPlane>();
        p.enabled = false;
        ImageTraking i = FindObjectOfType<ImageTraking>();
        i.enabled = false;

        return true;
    }

    // Method that hides the 3 button menu of the PBIS app
    private void CloseCanvas()
    {
        Canvas uicanvas = GameObject.Find("UI Canvas").GetComponent<Canvas>();
        if (uicanvas != null)
        {
            uicanvas.enabled = false;
        }

        Camera.main.cullingMask = LayerMask.GetMask("Default", "TransparentFX", "Ignore Raycast", "Water", "Rig");
    }

    public void OpenStandUpForOthers()
    {
        if (!this.sceneOpened && this.SetGroup())
        {
            this.sceneOpened = true;
            SceneManager.LoadSceneAsync("SceneBullying", LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync("Practice");
        }
    }

    public void OpenHandshake()
    {
        if (!this.sceneOpened && this.SetGroup())
        {
            this.sceneOpened = true;
            SceneManager.LoadSceneAsync("SceneHandshake", LoadSceneMode.Additive);
            SceneManager.UnloadSceneAsync("Practice");
        }
    }

    public void OpenOrganizeSpace(bool tidy)
    {
        if (!this.sceneOpened && this.SetGroup())
        {
            this.sceneOpened = true;
            if (tidy)
            {
                this.OpenTidyScene();
            }
            else
            {
                this.OpenUntidyScene();
            }
            SceneManager.UnloadSceneAsync("Practice");
        }
    }

    private void OpenTidyScene()
    {
        if (!SceneManager.GetSceneByName("TidyDrawer").isLoaded)
        {
            SceneManager.LoadSceneAsync("TidyDrawer", LoadSceneMode.Additive);
        }
    }

    private void OpenUntidyScene()
    {
        if (!SceneManager.GetSceneByName("UntidyDrawer").isLoaded)
        {
            SceneManager.LoadSceneAsync("UntidyDrawer", LoadSceneMode.Additive);
        }
    }

    private void EnableUI(bool enable)
    {
        this.bullyingButton.gameObject.SetActive(enable);
        this.handshakeButton.gameObject.SetActive(enable);
        this.organizeTidyButton.gameObject.SetActive(enable);
        this.organizeUntidyButton.gameObject.SetActive(enable);
        this.infoButton.gameObject.SetActive(enable);
        this.panelTitle.SetActive(enable);
        this.dropdown.SetActive(enable);
    }

    public void OpenInfoCanvas()
    {
        this.panelInfo.SetActive(true);
        this.infoButton.gameObject.SetActive(false);
        this.EnableUI(false);
    }

    public void CloseInfoCanvas()
    {
        this.panelInfo.SetActive(false);
        this.infoButton.gameObject.SetActive(true);
        this.EnableUI(true);
    }
    public void HandleInputData(int value)
    {
        this.selectedGroup = value;
    }
}
