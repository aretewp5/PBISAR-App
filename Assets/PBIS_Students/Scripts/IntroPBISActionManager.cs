using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPBISActionManager : MonoBehaviour
{
    public static event Action onTokenGained;
    public static event Action onTokenLost;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public static void GainToken()
    {
        onTokenGained?.Invoke();
    }

    public static void LostToken()
    {
        onTokenLost?.Invoke();
    }
}
