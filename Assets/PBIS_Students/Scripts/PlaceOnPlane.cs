using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

//[RequireComponent(typeof(ARRaycastManager))]
public class PlaceOnPlane : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
    private GameObject[] placedPrefab;

    [SerializeField]
    [Tooltip("A reference to the PlacedCementIndicator.")]
    private GameObject placedCementIndicator;

    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject[] PlacedPrefab
    {
        get { return placedPrefab; }
        set { placedPrefab = value; }
    }

    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public GameObject spawnedObject { get; private set; }

    /// <summary>
    /// Invoked whenever an object is placed in on a plane.
    /// </summary>
    public static event Action onPlacedObject;

    ARRaycastManager m_RaycastManager;

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    void Awake()
    {
        Debug.Log("******   ******* Awake PlaceOnPlane");
        m_RaycastManager = GetComponent<ARRaycastManager>();
    }
    int i = 0;

    void Update()
    {
        if ((Input.touchCount > 0)&&(!Player.instance.skipped))
        {
            // Debug.Log("****   **** PlaceOnPlane - Update Touch:"+ Input.touchCount);
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                if (m_RaycastManager.Raycast(touch.position, s_Hits, TrackableType.PlaneWithinPolygon))
                {
                    i++;
                    Pose hitPose = s_Hits[0].pose;


                    if (spawnedObject == null)
                    {
                        Debug.Log("****   **** PlaceOnPlane - Update pawnedObject == null");
                        spawnedObject = Instantiate(placedPrefab[i % placedPrefab.Length], hitPose.position, hitPose.rotation * Quaternion.Euler(0, 180, 0));
                        placedCementIndicator.SetActive(false);

                        // Used in IntroDialogManage
                       // if (onPlacedObject != null)
                       // {
                       //     onPlacedObject();
                       // }
                        onPlacedObject?.Invoke();
                    }
                    /*
                    else
                    {
                        spawnedObject.transform.position = hitPose.position;
                    }

                    if (onPlacedObject != null)
                    {
                        onPlacedObject();
                    }
                    */

                    // spawnedObject = Instantiate(placedPrefab[i % placedPrefab.Length], hitPose.position, hitPose.rotation);

                    
                }
            }
        }else if ((!Player.instance.skipped))
        {
            placedCementIndicator.SetActive(false);
        }
    }
}












/*
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;
using System.Threading.Tasks;

/// <summary>
/// Listens for touch events and performs an AR raycast from the screen touch point.
/// AR raycasts will only hit detected trackables like feature points and planes.
///
/// If a raycast hits a trackable, the <see cref="placedPrefab"/> is instantiated
/// and moved to the hit position.
/// </summary>
[RequireComponent(typeof(ARRaycastManager))]
public class PlaceOnPlane : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
    GameObject m_PlacedPrefab;

    private Animator _anim;
    private Animator myanim;

    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject placedPrefab
    {
        get { return m_PlacedPrefab; }
        set { m_PlacedPrefab = value; }
    }

    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public GameObject spawnedObject { get; private set; }

    void Awake()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
    }

    private async void OnEnable()
    {
        //_anim = GetComponentInChildren<Animator>();
        _anim = m_PlacedPrefab.GetComponentInChildren<Animator>();
        await Task.Delay(100);//let poi be created
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            var mousePosition = Input.mousePosition;
            touchPosition = new Vector2(mousePosition.x, mousePosition.y);
            return true;
        }
#else
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }
#endif

        touchPosition = default;
        return false;
    }

    void Update()
    {
        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;

        if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            var hitPose = s_Hits[0].pose;

            if (spawnedObject == null)
            {
                spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation);
            }
            //else
            //{
            //    spawnedObject.transform.position = hitPose.position;
            //}
        }
    }


    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARRaycastManager m_RaycastManager;
}
*/

