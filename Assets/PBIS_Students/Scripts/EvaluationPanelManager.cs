using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvaluationPanelManager : MonoBehaviour
{
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        anim = uiCanvas.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void Awake()
    {
        
        /*
        foreach (AnimationClip ac in anim.runtimeAnimatorController.animationClips)
        {
            Debug.Log("****   **** EvaluationPanelManager - Animazione nome: " + ac.name);
        }
        */
    }


    public void PositiveButtonSelected()
    {
        //Debug.Log("****   **** EvaluationPanelManager - PositiveButtonSelected");
        IntroPBISActionManager.GainToken();
        anim.Play("EVALUATION_OUT");

    }

    public void NegativeButtonSelected()
    {
        //Debug.Log("****   **** EvaluationPanelManager - NegativeButtonSelected");
        IntroPBISActionManager.LostToken();
        anim.Play("EVALUATION_OUT");

    }



}
