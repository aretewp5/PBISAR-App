namespace Bullying
{
    using UnityEngine;
    using UnityEngine.UI;
    using OrkestraLibImp;

    public class ChooseCharacter : MonoBehaviour, IGameController
    {
        public Canvas canvas;
        public Camera camera;

        public GameObject girlCharacter, boyCharacter;

        public GameObject panelInfo;
        public Button infoButton;
        public GameObject textTitle;

        private bool characterChoose = false;
        private OrkestraImpl orkestra;

        // Start is called before the first frame update
        void Start()
        {
            this.panelInfo.SetActive(false);
            this.canvas.worldCamera = this.camera;
            this.orkestra = FindObjectOfType<OrkestraImpl>();
        }

        void Update()
        {
            Vector3? pos = null;

            // Mouse click
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("click");
                pos = Input.mousePosition;
            }

            // Touch screen
            if (Input.touchCount > 0)
            {
                Debug.Log("touch");
                Touch touch = Input.touches[0];
                pos = touch.position;
            }

            // Detect raycast
            if (pos != null)
            {
                Ray ray = this.camera.ScreenPointToRay((Vector3)pos);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.name == "boy_C")
                    {
                        Debug.Log("hit boy");
                        this.CharacterChosen(0);
                    }
                    else if (hit.collider.gameObject.name == "girl_A")
                    {
                        Debug.Log("hit girl");
                        this.CharacterChosen(1);
                    }
                }
            }
        }

        public void Exit()
        {
            FindObjectOfType<PracticeGamesSceneManager>()?.Exit();
        }

        public void CharacterChosen(int character)
        {
            if (!this.characterChoose)
            {
                this.characterChoose = true;
                Debug.Log("Choose Character");
                PlayerPrefs.SetInt("SelectedCharacter", character);
                FindObjectOfType<PracticeGamesSceneManager>()?.OpenGameScene();
            }
        }

        private void EnableUI(bool enable)
        {
            this.infoButton.enabled = enable;
            this.girlCharacter.SetActive(enable);
            this.boyCharacter.SetActive(enable);
            this.textTitle.SetActive(enable);
        }

        public void OpenInfoCanvas()
        {
            this.panelInfo.SetActive(true);
            this.infoButton.gameObject.SetActive(false);
            this.EnableUI(false);
        }

        public void CloseInfoCanvas()
        {
            this.panelInfo.SetActive(false);
            this.infoButton.gameObject.SetActive(true);
            this.EnableUI(true);
        }
    }
}
