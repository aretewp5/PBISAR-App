using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OrkestraLib.Message;
using OrkestraLibImp;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using static OrkestraLib.Orkestra;
using TMPro;

public class CharacterMovement : MonoBehaviour, IGameController
{
    public GameObject Characters;
    public GameObject[] Bullies;
    public GameObject[] Helpers;
    public GameObject Bullied;

    public ARTrackedImageManager arTrackedImageManager;
    private ARSession arSession;
    public Canvas canvas;
    public Canvas scoreCanvas;
    public Text scoreText;

    private List<Animator> BulliesAnims = new List<Animator>();
    private List<Animator> HelpersAnims = new List<Animator>();
    private Animator BulliedAnim;

    List<SkinnedMeshRenderer> renders = new List<SkinnedMeshRenderer>();

    private Button[] Buttons;
    public Button Button_TryAgain;
    public Canvas resultCanvas;
    public GameObject resultText;

    private OrkestraImpl orkestra;

    private int SelectedCharacter = 0;
    private int OtherCharacter = 1;
    private const int SCORE_EACH_GAME = 1;
    private bool localResponse = false, remoteResponse = false;
    private bool localResponseWin = false, remoteResponseWin = false;
    private Action localResponseAction;
    private Action remoteResponseAction;

    public bool IsGameEnded { get; private set; }

    public Canvas Canvas_DetectMarker;
    public GameObject textWait;
    public GameObject textTitle;

    private int numTries = 1;
    private int maxTries = 4;

    private List<Vector3> originalHelperPosList;
    private List<Quaternion> originalHelperRotList;

    /// <summary>
    /// Store renders component of the gameobjects
    /// Disable them until the image is tracked
    /// </summary>
    private void Awake()
    {
        // Store original position of characters
        this.originalHelperPosList = new List<Vector3>();
        this.originalHelperRotList = new List<Quaternion>();

        renders.AddRange(Bullied.GetComponentsInChildren<SkinnedMeshRenderer>());
        Bullies.ToList().ForEach((e) => { renders.AddRange(e.GetComponentsInChildren<SkinnedMeshRenderer>()); });
        Helpers.ToList().ForEach((e) =>
        {
            renders.AddRange(e.GetComponentsInChildren<SkinnedMeshRenderer>());
            this.originalHelperPosList.Add(e.transform.localPosition);
            this.originalHelperRotList.Add(e.transform.localRotation);
        });

        Debug.Log("this.originalHelperPosList: " + this.originalHelperPosList);

#if !UNITY_EDITOR
        RendererState(false);  // Hide meshes
#endif
        arTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
        ImageTraking it = FindObjectOfType<ImageTraking>();
        if (it) it.enabled = false;
        arSession = FindObjectOfType<ARSession>();
    }

    // Start is called before the first frame update
    void Start()
    {
        // Get orkestra singleton
        this.orkestra = FindObjectOfType<OrkestraImpl>();
        this.orkestra.RestartFunc = Restart;
        this.orkestra.AnimRemoteCharacterFunc = AnimRemoteCharacter;

        // Set chosen characters
        SelectedCharacter = PlayerPrefs.GetInt("SelectedCharacter");
        OtherCharacter = 0;
        if (SelectedCharacter == 0)
        {
            OtherCharacter = 1;
        }

        // Set characters animations
        foreach (GameObject b in Bullies)
        {
            this.BulliesAnims.Add(b.GetComponent<Animator>());
        }

        foreach (GameObject b in Helpers)
        {
            this.HelpersAnims.Add(b.GetComponent<Animator>());
        }

        this.BulliedAnim = this.Bullied.GetComponent<Animator>();
        this.Buttons = GameObject.Find("BullyingButtons").GetComponentsInChildren<Button>();
        this.canvas.enabled = false;

        // Initialize game state variables
        IsGameEnded = false;
        this.localResponse = false;
        this.remoteResponse = false;
        this.localResponseWin = false;
        this.remoteResponseWin = false;

        StartUI();

        // In the editor, start the sequende without waiting for the marker detection
#if UNITY_EDITOR
        StartSequence();
#endif
    }

    void StartUI()
    {
        Debug.Log("---- StartUI ----");
        // Set the UI
        this.Button_TryAgain.gameObject.SetActive(false);
        this.scoreCanvas.enabled = false;
        this.resultCanvas.gameObject.SetActive(false);
        this.textWait.SetActive(false);
    }

    // --------------------- Character Control ---------------------
    void StartSequence()
    {
        Debug.Log("---- StartSequence ----");
        this.Canvas_DetectMarker.enabled = false;
        StartCoroutine(BullyWhisper());

        Bullied.gameObject.transform.LookAt(Helpers[0].transform);

        BulliedAnim.SetBool("Walk", true);

        HelpersAnims[0].SetBool("TalkFront", true);
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject a in Bullies)
        {
            a.transform.LookAt(Bullied.transform);
        }

        if (remoteResponse && localResponse && !IsGameEnded)
        {
            localResponseAction.Invoke();
            remoteResponseAction.Invoke();
            IsGameEnded = true;
            EndGame();
        }
    }

    private void AnimRemoteCharacter(ActMessages response)
    {
        switch (response)
        {
            case ActMessages.LAUGH:
                remoteResponseAction = () =>
                {
                    AnimLaugh(OtherCharacter);
                };
                break;
            case ActMessages.IGNORE:
                remoteResponseAction = () =>
                {
                    AnimIgnore(OtherCharacter);
                };
                break;
            case ActMessages.STOP:
                remoteResponseAction = () =>
                {
                    AnimAskThemToStop(OtherCharacter);
                };
                remoteResponseWin = true;
                break;
        }
        remoteResponse = true;
    }

    private void EndGame()
    {
        Debug.Log("---- EndGame ----");
        // If both answers are correct, the game ends and the points are added
        if (localResponseWin && remoteResponseWin)
        {
            Debug.Log("---- Correct answers in try number " + numTries + ". ----");
            // End Successfully
            StartCoroutine(EndGame("success"));
            // Add points to the player if is the first time it completes the exercise
            if (!PlayerPrefs.HasKey("BullyingPoints"))
            {
                Debug.Log("---- Adding " + (maxTries - numTries) + " points to the player. ----");
                Player.instance.points += maxTries - numTries;
                PlayerPrefs.SetInt("BullyingPoints", maxTries - numTries);
            }
        }
        else // If not, retry a maximum of maxTries times
        {
            Debug.Log("---- Incorrect answers in try number " + numTries + ". ----");
            // Check retry number
            if (numTries < (maxTries - 1))
            {
                numTries++;
                // Retry the exercise
                StartCoroutine(EndGame("retry"));
            }
            else
            {
                // Hide game canvas
                StartCoroutine(EndGame("getHelp"));
                if (!PlayerPrefs.HasKey("BullyingPoints"))
                {
                    PlayerPrefs.SetInt("BullyingPoints", 0);
                }
            }
        }
    }

    IEnumerator EndGame(string endType)
    {
        yield return new WaitForSecondsRealtime(5);
        // Hide game canvas
        canvas.enabled = false;
        // Show result canvas and set the corresponding text
        if (endType == "success")
        {
            resultText.GetComponent<TMP_Text>().text = "Congratulations! You answered correctly! \n\n Now you can go back by clicking the back button.";
            resultCanvas.gameObject.SetActive(true);
        }
        else if (endType == "retry")
        {
            resultText.GetComponent<TMP_Text>().text = "Sorry, the answers were not correct but you can try again!";
            resultCanvas.gameObject.SetActive(true);
            ShowTryAgain();
        }
        else if (endType == "getHelp")
        {
            resultText.GetComponent<TMP_Text>().text = "Sorry, the answers were not correct, but you can ask your peers or teachers for help and try again! \n\n Now you can go back by clicking the back button.";
            resultCanvas.gameObject.SetActive(true);
        }
    }

    private void ShowTryAgain()
    {
        Button_TryAgain.gameObject.SetActive(true);
    }

    public void TryAgain()
    {
        this.orkestra.Dispatch(Channel.Application, new TryAgainMessage(this.orkestra.agentId, true));
        Restart();
    }

    private void ResetHelperPositions()
    {
        int i = 0;
        Helpers.ToList().ForEach((e) =>
        {
            e.transform.localPosition = this.originalHelperPosList[i];
            Quaternion currentRotation = e.transform.localRotation;
            e.transform.localRotation = Quaternion.Slerp(currentRotation, this.originalHelperRotList[i], Time.deltaTime);
            i++;
        });
    }

    private void Restart()
    {
        Debug.Log("---- Restart ----");
        ResetHelperPositions();
        // Reset game state variables
        IsGameEnded = false;
        this.localResponse = false;
        this.remoteResponse = false;
        this.localResponseWin = false;
        this.remoteResponseWin = false;
        this.canvas.enabled = true;
        StartUI();
        this.textTitle.SetActive(true);
        ShowButtons();
        StartSequence();
    }

    public void Exit()
    {
        FindObjectOfType<PracticeGamesSceneManager>()?.Exit();
    }

    IEnumerator BullyWhisper()
    {
        BulliesAnims[1].SetTrigger("Whisper");

        yield return new WaitForSecondsRealtime(3.3f);

        BulliesAnims[0].SetBool("FrontStand", true);

        BulliesAnims[1].SetBool("FrontStand", true);

        BulliesAnims[2].SetBool("FrontStand", true);

        BulliedAnim.SetBool("Walk", false);

        BulliedAnim.SetBool("HeadBowed", true);

        yield return new WaitForSecondsRealtime(1);

        HelpersAnims[0].SetBool("TalkFront", false);

        foreach (GameObject a in Helpers)
        {
            a.transform.LookAt(Bullied.transform);
        }

        this.canvas.enabled = true;
    }

    public void HelperLaugh()
    {
        localResponseAction = () =>
        {
            AnimLaugh(SelectedCharacter);
        };
        this.orkestra.Dispatch(Channel.Application, new ActMessage(this.orkestra.agentId, ActMessages.LAUGH));
        localResponse = true;
        HideButtons();
        this.textWait.SetActive(true);
        this.textTitle.SetActive(false);
    }

    private void AnimLaugh(int character)
    {
        this.textWait.SetActive(false);
        HelpersAnims[character].transform.LookAt(Bullied.transform);
        HelpersAnims[character].SetBool("FrontStand", true);
    }

    public void HelperIgnore()
    {
        localResponseAction = () =>
        {
            AnimIgnore(SelectedCharacter);
        };
        this.orkestra.Dispatch(Channel.Application, new ActMessage(this.orkestra.agentId, ActMessages.IGNORE));
        localResponse = true;
        HideButtons();
        this.textWait.SetActive(true);
        this.textTitle.SetActive(false);
    }

    private void AnimIgnore(int character)
    {
        this.textWait.SetActive(false);
        HelpersAnims[character].transform.Rotate(0, 180, 0);
        HelpersAnims[character].SetBool("Walk", true);
    }

    public void HelperAskThemToStop()
    {
        localResponseAction = () =>
        {
            AnimAskThemToStop(SelectedCharacter);
        };
        this.orkestra.Dispatch(Channel.Application, new ActMessage(this.orkestra.agentId, ActMessages.STOP));
        localResponse = true;
        localResponseWin = true;
        HideButtons();
        this.textWait.SetActive(true);
        this.textTitle.SetActive(false);
    }

    private void AnimAskThemToStop(int character)
    {
        this.textWait.SetActive(false);
        HelpersAnims[character].transform.LookAt(Bullies[0].transform);
        HelpersAnims[character].SetBool("Walk", true);
        StartCoroutine(GoToBullies(character));
    }

    IEnumerator GoToBullies(int character)
    {
        yield return new WaitForSecondsRealtime(1.5f);
        HelpersAnims[character].SetBool("Walk", false);
        HelpersAnims[character].transform.LookAt(Bullies[1].transform);
        HelpersAnims[character].SetBool("No", true);
    }

    private void HideButtons()
    {
        foreach (Button b in Buttons)
        {
            b.gameObject.SetActive(false);
        }
    }

    private void ShowButtons()
    {
        foreach (Button b in Buttons)
        {
            b.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Subscribe to the Ar tracked image change method
    /// </summary>
    private void OnEnable()
    {
        if (arTrackedImageManager)
        {
            arTrackedImageManager.trackedImagesChanged += ImageChanged;
        }
    }

    /// <summary>
    /// Unsubscribe to the tracked image change method and disconnect from orkestra
    /// </summary>
    private void OnDisable()
    {
        if (arTrackedImageManager)
        {
            arTrackedImageManager.trackedImagesChanged -= ImageChanged;
        }
    }

    /// <summary>
    /// When the tracked Image is detected/updated the update image method is called
    /// The game object active value is set to false when the tracked image isnt being the detected by the device
    /// </summary>
    /// <param name="eventArgs">Events of the AR Tracked image manager</param>
    void ImageChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (ARTrackedImage trackedImage in eventArgs.added)
        {
            UpdateImage(trackedImage);
            RendererState(true);
            StartSequence();
        }
        foreach (ARTrackedImage trackedImage in eventArgs.updated)
        {
            UpdateImage(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {
            RendererState(false);
        }
    }

    /// <summary>
    /// Update the rotation of the game object if the marker changes it's rotation.
    /// </summary>
    /// <param name="trackedImage">marker tracked by the AR App.</param>
    private void UpdateImage(ARTrackedImage trackedImage)
    {
        Characters.transform.SetPositionAndRotation(trackedImage.transform.position, trackedImage.transform.rotation);
    }

    /// <summary>
    /// Enable or disable renders components from the characters.
    /// </summary>
    /// <param name="active"></param>
    void RendererState(bool active)
    {
        foreach (SkinnedMeshRenderer a in renders)
        {
            a.enabled = active;
        }
    }
}

public enum ActMessages
{
    IGNORE, LAUGH, STOP, HUG
}
