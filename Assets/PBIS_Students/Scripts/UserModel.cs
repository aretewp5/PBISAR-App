using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UserModel
{
    public string username;
    public string email;
    public int id;
    public string nome;
    public string cognome;
    public string section;
    public string password;
    public string organizationName;
    public string token;
    public string role;
    public int points;

    public Dictionary<string, string> routinecollection;

    public string moodleiduser;
}
