using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

[RequireComponent(typeof(MainApp))]
public class ARTapToPlace : MonoBehaviour
{
    private Vector3 touchedPosition;
    public GameObject objectToPlace;
    public GameObject whereToPlace;
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
    //public AssetReference assetReference;

    ARRaycastManager m_arRayCastManager;
    ARPlaneManager m_planeManager;
    private MainApp m_MainApp;
    private GameObject spawnedObject;

    void Awake()
    {
        //Addressables.DownloadDependenciesAsync(this.objectToPlace);

        m_arRayCastManager = FindObjectOfType<ARRaycastManager>();
        m_planeManager = FindObjectOfType<ARPlaneManager>();

        PlaceOnPlane p = FindObjectOfType<PlaceOnPlane>();
        p.enabled = false;

        //FindObjectOfType<PlacementIndicator>().enabled = false;

        //m_planeManager.planePrefab = ARplane;

        m_MainApp = GetComponent<MainApp>();
        Debug.Log("AR TAP AWAKE");
        m_planeManager.enabled = true;
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }
        touchPosition = default;
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        // Spawns the drawer when launching the app from the Unity editor
#if UNITY_EDITOR
        if (spawnedObject == null)
        {
            spawnedObject = Instantiate(objectToPlace, new Vector3(0.08f, 0, 0), Quaternion.identity);
            spawnedObject.transform.SetParent(whereToPlace.transform, true);
        }
#endif
        //m_arRayCastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, TrackableType.Planes);

        if (spawnedObject != null)
        {
            foreach (var plane in m_planeManager.trackables)
            {
                plane.gameObject.SetActive(false);
            }
        }

        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;

        if (m_arRayCastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
        {
            Pose hitPose = hits[0].pose;

            if (spawnedObject == null)
            {
                touchedPosition = hitPose.position;
                //touchedPosition += new Vector3(0.08f, 0, 0);
                spawnedObject = Instantiate(objectToPlace, touchedPosition, Quaternion.identity);
                spawnedObject.transform.SetParent(whereToPlace.transform, true);

                m_MainApp.LocalUserReady();
            }
        }
    }
}
