using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Detect collisions on the drawer
/// </summary>
public class DetectDrawerCollision : MonoBehaviour
{
    internal bool isSomethingColliding;
    private int numObjectsColliding;
    internal Rigidbody lastObjectOut;
    private const int totalObjects = 4; // All except the math book

    private string[] NamesOfObjectsThatShouldBeInside = { "Biology_Book", "Ruler",
        "History_Book", "Pencil" };

    private List<string> objectsInsideList = new List<string>();
    internal bool allObjectsInsideList { get; private set; }
    public bool isNextTurn { get; internal set; }

    public bool tidy;

    private SpawnableManager m_SpanwableManager;
    private MainApp m_MainApp;

    void Start()
    {
        Debug.Log("---- DetectDrawerCollision Start ----");
        numObjectsColliding = 5;
        isSomethingColliding = true;
        objectsInsideList.AddRange(MainApp.BOOKS);

        m_SpanwableManager = GameObject.Find("Manager").GetComponent<SpawnableManager>();
        m_MainApp = GameObject.Find("Manager").GetComponent<MainApp>();
    }

    // Entering the drawer collision area
    void OnTriggerEnter(Collider collision)
    {
        Debug.Log(collision.gameObject.name + " entered " + gameObject.name + " collider");

        if (CheckValidCollision(collision) && numObjectsColliding < 5 && !objectsInsideList.Contains(collision.gameObject.name))
        {
            objectsInsideList.Add(collision.gameObject.name);
            numObjectsColliding++;
            isSomethingColliding = true;
            Debug.Log("Enter Colliding " + numObjectsColliding);
        }
        // Check if the turn has to change (it depends on the exercise)
        if (m_SpanwableManager.IsMyTurn)
        {
            if (!tidy && m_MainApp.FinalExercise)
            {
                isNextTurn = true;
            }
            if (tidy || (!tidy && !m_MainApp.FinalExercise))
            {
                isNextTurn = false;
            }
        }
    }

    // Method to check only for objects of the exercise (not the drawer itself, nor the plane or the trigger area)
    private bool CheckValidCollision(Collider collision)
    {
        return !(collision.gameObject.name.Equals("Plane") || collision.gameObject.name.Equals("Drawer") || collision.gameObject.name.Equals("DrawerColliderIn"));
    }

    // Leaving the drawer collision area
    void OnTriggerExit(Collider collision)
    {
        if (CheckValidCollision(collision))
        {
            objectsInsideList.Remove(collision.gameObject.name);
            numObjectsColliding--;

            Debug.Log(collision.gameObject.name + " exits the drawer, " + numObjectsColliding + " objects left.");

            if (tidy)
            {
                // In the tidy exercise, the objective is to get the math book out
                if (collision.gameObject.name == "Math_Book")
                {
                    numObjectsColliding = 0;
                }

                // Get the turn state to display it on screen
                if (m_SpanwableManager.IsMyTurn)
                {
                    isNextTurn = true;
                }
            }
            else
            {
                // In the untidy's first exercise, the objective is to get the math book out
                if (!m_MainApp.FinalExercise)
                {
                    if(collision.gameObject.name == "Math_Book")
                    {
                        numObjectsColliding = 0;
                    }
                }
                // Get the turn state to display it on screen
                if (m_SpanwableManager.IsMyTurn)
                {
                    if (!m_MainApp.FinalExercise)
                    {
                        isNextTurn = true;
                    }

                    if (m_MainApp.FinalExercise)
                    {
                        isNextTurn = false;
                    }
                }
            }

            if (numObjectsColliding == 0)
            {
                lastObjectOut = collision.gameObject.GetComponent<Rigidbody>();
                Debug.Log("All out!");
            }
        }
    }

    private void Update()
    {
        // If the last objet is using gravity, it means it has been released
        if (lastObjectOut != null)
        {
            if (lastObjectOut.useGravity)
            {
                isSomethingColliding = false;
                lastObjectOut = null;
            }
        }
        else
        {
            allObjectsInsideList = false;
            if (numObjectsColliding == totalObjects)
            {
                if (AreallObjectsInsideList())
                {
                    Debug.Log("All objects are inside ");
                    allObjectsInsideList = true;
                }
            }
        }
    }

    /// <summary>
    /// Detect if all objects are inside the drawer
    /// </summary>
    /// <returns></returns>
    private bool AreallObjectsInsideList()
    {
        foreach (string name in NamesOfObjectsThatShouldBeInside)
        {
            if (!objectsInsideList.Contains(name))
            {
                return false;
            }
        }
        return true;
    }
}
