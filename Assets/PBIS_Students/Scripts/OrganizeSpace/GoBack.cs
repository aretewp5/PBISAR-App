using OrkestraLibImp;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace OrganizeSpace
{
    public class GoBack : MonoBehaviour
    {
        public void GoBackFromTidyScene()
        {
            SceneManager.UnloadSceneAsync("TidyDrawer");
            SceneManager.LoadSceneAsync("Practice", LoadSceneMode.Additive);
            ResetParameters();
        }

        public void GoBackFromUnTidyScene()
        {
            SceneManager.UnloadSceneAsync("UntidyDrawer");
            SceneManager.LoadSceneAsync("Practice", LoadSceneMode.Additive);
            ResetParameters();
        }

        void ResetParameters()
        {
            Camera.main.cullingMask = LayerMask.GetMask("Default", "TransparentFX", "UI", "Ignore Raycast", "Water", "Rig");
            FindObjectOfType<ImageTraking>().enabled = true;
            Canvas uicanvas = null;

            if (GameObject.Find("UI Canvas") != null)
            {
                uicanvas = GameObject.Find("UI Canvas").GetComponent<Canvas>();
                if (uicanvas != null)
                    uicanvas.enabled = true;
            }

            if (FindObjectOfType<DetectDrawerCollision>())
                Destroy(FindObjectOfType<DetectDrawerCollision>().gameObject.transform.parent.transform.parent.gameObject);

            Destroy(GameObject.FindObjectOfType<OrkestraImpl>().gameObject);
        }
    }
}
