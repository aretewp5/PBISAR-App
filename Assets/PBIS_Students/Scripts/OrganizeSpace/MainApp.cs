using OrkestraLib.Message;
using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using OrganizeSpace;
using static OrkestraLib.Orkestra;
using OrkestraLibImp;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(SpawnableManager))]
public class MainApp : MonoBehaviour, IGameController
{
    public static string[] BOOKS = { "Biology_Book", "Ruler", "History_Book", "Pencil", "Math_Book" };

    private SpawnableManager m_SpawnableManager;
    internal bool isLocalUserReady = false;
    internal bool isRemoteUserReady = false;

    private const float MessageDispatchRate = 0.01f;
    private int untidyAnswer;
    private int itemsForceAnswer;

    private Canvas FirstExerciseCanvas;
    private Canvas FirstQuestionCanvas;
    private Canvas Question2Canvas;
    private Canvas FinalExerciseCanvas;
    private Canvas EndCanvas;
    public string SceneName;

    private bool isPlaying;
    private DetectDrawerCollision drawerCollisionDetecter;
    internal TMP_Text yourTurn_Text;
    internal TMP_Text peerTurn_Text;
    private bool isCanvasQuestionDisplayed;
    private bool LocalQuestionsAnswered = false;
    internal bool FinalExercise;
    internal bool RemoteQuestionsAnswered = false;

    internal List<Image> Buttons_ItemsForce = new List<Image>();
    private int tidyAnswer;
    private OrkestraImpl orkestra;
    private GameObject Text_ConnectedUsers;
    private GameObject Text_Room;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("MAIN APP START");
        orkestra = FindObjectOfType<OrkestraImpl>();
        orkestra.ApplicationEvents += AppEventSubscriber;
        this.Text_ConnectedUsers = GameObject.Find("Text_ConnectedUsers");
        this.Text_Room = GameObject.Find("Text_Room");

        // Save spawnable manager component to access 3d objects and game properties
        m_SpawnableManager = GetComponent<SpawnableManager>();

        // Get Text to enable/disable it depending on your turn
        yourTurn_Text = GameObject.Find("yourTurn_Text").GetComponent<TMP_Text>();
        yourTurn_Text.enabled = false;

        peerTurn_Text = GameObject.Find("peerTurn_Text").GetComponent<TMP_Text>();
        peerTurn_Text.enabled = false;

        // Save the canvas so we can enable/disable them
        // Disable all canvas at the beginning until the drawer is placed 
        var firstExercise = GameObject.Find("FirstExerciseCanvas");
        if (firstExercise != null)
        {
            FirstExerciseCanvas = firstExercise.GetComponent<Canvas>();
            FirstExerciseCanvas.enabled = false;
        }

        var firstQuestion = GameObject.Find("FirstQuestionCanvas");
        if (firstQuestion != null)
        {
            FirstQuestionCanvas = firstQuestion.GetComponent<Canvas>();
            FirstQuestionCanvas.enabled = false;
        }

        var secondQuestion = GameObject.Find("Question2Canvas");
        if (secondQuestion != null)
        {
            Question2Canvas = secondQuestion.GetComponent<Canvas>();
            Question2Canvas.enabled = false;
        }

        var finalExerciseObj = GameObject.Find("FinalExerciseCanvas");
        if (finalExerciseObj)
        {
            FinalExerciseCanvas = finalExerciseObj.GetComponent<Canvas>();
            FinalExerciseCanvas.enabled = false;
        }

        var endCanvasObj = GameObject.Find("EndCanvas");
        if (endCanvasObj)
        {
            EndCanvas = endCanvasObj.GetComponent<Canvas>();
            EndCanvas.enabled = false;
        }
        FinalExercise = false;
    }

   private void LateUpdate()
   {
        // If drawer is ar placed
        if (drawerCollisionDetecter != null)
        {
            // If nothing is touching the drawer collider and the first question canvas was not displayed yet
            // Deactivate the controls and enable the first question canvas
            // The first exercise finished and the users have to answer the question
            if (!drawerCollisionDetecter.isSomethingColliding && !isCanvasQuestionDisplayed)
            {
                // When the questions starts you cant move any object
                DeactivateControls();

                FirstExerciseCanvas.enabled = false;
                FirstQuestionCanvas.enabled = true;
                isCanvasQuestionDisplayed = true;
            }
            // The final exercise finished, the game ends
            // All objects except the math book are inside
            else if (FinalExercise && drawerCollisionDetecter.allObjectsInsideList)
            {
                DeactivateControls();
                FinalExerciseCanvas.enabled = false;
                TMP_Text updateText = EndCanvas.gameObject.GetComponentInChildren<TMP_Text>();
                if (updateText != null)
                {
                    updateText.text = "Good job! \n\n Now you can go back by clicking the back button.";
                }
                EndCanvas.enabled = true;
            }
        }
   }

    // Reset the variables so we reuse them for the next exercise
    private void DeactivateControls()
    {
        isRemoteUserReady = false;
        isLocalUserReady = false;
        SetMyTurn(false);
    }

    /**
     *  When the local user is ready send the info to other users and if both users are ready enable exercise canvas.
     *  The first user to be ready starts with the first turn
     */
    internal void LocalUserReady()
    {
        Debug.Log("LocalUserReady");
        drawerCollisionDetecter = GameObject.Find("DrawerColliderIn").GetComponent<DetectDrawerCollision>();

        // Deactivate the placement indicator if it still exists
        PlacementIndicator pIndcator = FindObjectOfType<PlacementIndicator>();
        if (pIndcator)
        {
            GameObject placement = pIndcator.gameObject;
            if (placement)
            {
                placement.SetActive(false);
            }
        }

        isLocalUserReady = true;
        SendReadyToOtherUsers();

        if (!isRemoteUserReady)
        {
            Debug.Log("My turn");
            SetMyTurn(true);
        }
        else
        {
            // Everything is ready
            EnableCanvas();
        }
    }

    // When the remote user is ready we wait until the local user is ready
    internal void RemoteUserReady()
    {
        isRemoteUserReady = true;
        if (!isLocalUserReady)
        {
            Debug.Log("Not my turn!");
            SetMyTurn(false);
        }
        else
        {
            // Everything is ready
            EnableCanvas();
        }
    }

    // Enable the first or last canvas depending on the game stage
    void EnableCanvas()
    {
        if (!FinalExercise)
        {
            EnableFirstExerciseCanvas();
        }
        else
        {
            EnableFinalExerciseCanvas();
        }
    }

    // If both users are ready the exercise can start
    void EnableFirstExerciseCanvas()
    {
        if (isLocalUserReady && isRemoteUserReady)
        {
            Debug.Log("First exercise canvas enabled");
            FirstExerciseCanvas.enabled = true;
            TMP_Text[] textes = FirstExerciseCanvas.GetComponentsInChildren<TMP_Text>();
            foreach (TMP_Text t in textes)
            {
                if (t.gameObject.name.Equals("yourTurn_Text"))
                {
                    yourTurn_Text = t;
                }
                else if (t.gameObject.name.Equals("peerTurn_Text"))
                {
                    peerTurn_Text = t;
                }
            }
            yourTurn_Text.enabled = false;
            peerTurn_Text.enabled = false;
        }
    }

    // Disable previous canvas and starts the final exercise
    void EnableFinalExerciseCanvas()
    {
        if (isLocalUserReady && isRemoteUserReady)
        {
            Debug.Log("Final exercise canvas enabled");
            TMP_Text[] textes = FinalExerciseCanvas.GetComponentsInChildren<TMP_Text>();
            foreach (TMP_Text t in textes)
            {
                if (t.gameObject.name.Equals("yourTurn_Text"))
                {
                    yourTurn_Text = t;
                }
                else if (t.gameObject.name.Equals("peerTurn_Text"))
                {
                    peerTurn_Text = t;
                }
            }

            yourTurn_Text.enabled = false;
            peerTurn_Text.enabled = false;

            Question2Canvas.enabled = false;
            FinalExerciseCanvas.enabled = true;
        }
    }

    internal void SetMyTurn(bool v)
    {
        m_SpawnableManager.IsMyTurn = v;
    }

    internal void MoveObjects(ObjCoords oC)
    {
        orkestra.Events.Add(() =>
        {
            GameObject ObjectToMove = GameObject.Find(oC.name);
            Rigidbody rb = ObjectToMove.GetComponent<Rigidbody>();
            // Disable gravity so we can move the object without falling
            rb.useGravity = false;
            // Change object position
            oC.Update(ObjectToMove.transform);
        });
    }

    internal void StopObjects(ObjCoords oC)
    {
        orkestra.Events.Add(() =>
        {
            GameObject ObjectToMove = GameObject.Find(oC.name);
            if (ObjectToMove == null)
            {
                Debug.LogError("Object not found");
            }
            else
            {
                Rigidbody rb = ObjectToMove.GetComponent<Rigidbody>();
                rb.useGravity = true;
            }
        });
    }

    public void Answer_TheDrawerWasUntidy(int answer)
    {
        untidyAnswer = answer;
        // Get the reference for the next question text
        TMP_Text question2Text = null;
        TMP_Text[] textes = Question2Canvas.gameObject.GetComponentsInChildren<TMP_Text>();
        foreach (TMP_Text t in textes)
        {
            if (t.gameObject.name.Equals("Text_Question2"))
            {
                question2Text = t;
            }
        }
        if (untidyAnswer == 0 || tidyAnswer == 2)
        {
            if (question2Text != null)
            {
                // Update next question with correct text
                question2Text.text = "Correct! Now answer the next question: \n\n" + question2Text.text;
            }
            // Add corresponding points to the player
            if (!PlayerPrefs.HasKey("UntidyPoints"))
            {
                Debug.Log("---- Adding 3 points to the player. ----");
                Player.instance.points += 3;
                PlayerPrefs.SetInt("UntidyPoints", 3);
            }
        }
        else
        {
            if (question2Text != null)
            {
                // Update next question with correct text
                question2Text.text = "Sorry not correct! Now try the next question: \n\n" + question2Text.text;
            }
            // Add corresponding points to the player
            if (!PlayerPrefs.HasKey("UntidyPoints"))
            {
                Debug.Log("---- Adding 0 points to the player. ----");
                PlayerPrefs.SetInt("UntidyPoints", 0);
            }
        }
        FirstQuestionCanvas.enabled = false;
        Question2Canvas.enabled = true;
        SaveButtonsOfItemsForceToTakeOut();
    }

    private void SaveButtonsOfItemsForceToTakeOut()
    {
        // Save the buttons to change the color
        Buttons_ItemsForce.Add(GameObject.Find("Button_Noneofthem").GetComponent<Image>());
        Buttons_ItemsForce.Add(GameObject.Find("Button_Pencil").GetComponent<Image>());
        Buttons_ItemsForce.Add(GameObject.Find("Button_Biology").GetComponent<Image>());
        Buttons_ItemsForce.Add(GameObject.Find("Button_HistoryBook").GetComponent<Image>());
        Buttons_ItemsForce.Add(GameObject.Find("Button_MathsBook").GetComponent<Image>());
    }

    public void Answer_TheDrawerWastidy(int answer)
    {
        tidyAnswer = answer;
        FirstQuestionCanvas.enabled = false;
        TMP_Text updateText;
        if (tidyAnswer == 3)
        {
            updateText = EndCanvas.gameObject.GetComponentInChildren<TMP_Text>();
            if (updateText != null)
            {
                updateText.text = "The answer was correct. Good job! \n\n Now you can go back by clicking the back button.";
            }

            // Add points to the player if is the first time it completes the exercise
            if (!PlayerPrefs.HasKey("Tidy"))
            {
                Debug.Log("---- Adding 3 points to the player. ----");
                Player.instance.points += 3;
                PlayerPrefs.SetInt("Tidy", 3);
            }
        }
        else
        {
            updateText = EndCanvas.gameObject.GetComponentInChildren<TMP_Text>();
            if (updateText != null)
            {
                updateText.text = "The answer was not correct. Nice try! \n\n Now you can go back by clicking the back button.";
            }

            // Add points to the player if is the first time it completes the exercise
            if (!PlayerPrefs.HasKey("Tidy"))
            {
                Debug.Log("---- Adding 0 points to the player. ----");
                PlayerPrefs.SetInt("Tidy", 0);
            }
        }
        EndCanvas.enabled = true;
    }

    // Change colors of options of items force to take out
    public void Answer_ItemsForceToTakeOut(int answer)
    {
        itemsForceAnswer = answer;

        if (answer == (int)ItemsForceToTakeOut.NONEOFTHEM)
        {
            for (int i = 0; i < Buttons_ItemsForce.Count; i++)
            {
                if (i != answer)
                {
                    Buttons_ItemsForce[i].color = Color.white;
                }
            }
        }
        else
        {
            Buttons_ItemsForce[(int)ItemsForceToTakeOut.NONEOFTHEM].color = Color.white;
        }

        Color color = Buttons_ItemsForce[answer].color;

        if (color == Color.green)
        {
            color = Color.white;
        }
        else
        {
            color = Color.green;
        }

        Buttons_ItemsForce[answer].color = color;
    }

    // Finish the questions and start the last exercise
    public void GoToPutElementsAgainInDrawer()
    {
        // Update UI
        foreach (Button b in Question2Canvas.GetComponentsInChildren<Button>())
        {
            b.interactable = false;
        }
        Question2Canvas.GetComponentInChildren<TMP_Text>().text = "Wait...";

        // Update control variables
        LocalQuestionsAnswered = true;
        FinalExercise = true;

        // Send Orkestra event
        SendAllAnswersFinished();

        // Continue
        LocalUserReady();
    }

    /// <summary>
    /// Dispatchs camera messages at constant rate (fps): <code>60 / <see cref="MessageDispatchRate"/></code>
    /// </summary>
    internal IEnumerator UpdateRemoteObject()
    {
        while (m_SpawnableManager.selectedObject != null)
        {
            Debug.Log("Sending: " + m_SpawnableManager.selectedObject.name);

            SendObjectCoords(m_SpawnableManager.selectedObject);

            yield return new WaitForSeconds(MessageDispatchRate);
        }
        StopObjectCoords(m_SpawnableManager.lastSharedObject);

        if (drawerCollisionDetecter.isNextTurn)
        {
            NextTurn();
        }
    }

    public void NextTurn()
    {
        Debug.Log("Next Turn");

        drawerCollisionDetecter.isNextTurn = false;
        yourTurn_Text.enabled = false;
        peerTurn_Text.enabled = true;
        m_SpawnableManager.IsMyTurn = false;
        SendOtherUserTurn();
    }

    internal void SendReadyToOtherUsers()
    {
        Debug.Log("Sending Orkestra event SendReadyToOtherUsers");
        ReadyToStart rTS = new ReadyToStart(orkestra.agentId, true);
        orkestra.Dispatch(Channel.Application, rTS);
    }

    internal void SendOtherUserTurn()
    {
        Debug.Log("Sending Orkestra event SendOtherUserTurn");
        YourTurn yourTurn = new YourTurn(orkestra.agentId, true);
        orkestra.Dispatch(Channel.Application, yourTurn);
    }

    internal void SendObjectCoords(GameObject objectToShare)
    {
        orkestra.Dispatch(Channel.Application, new ObjCoords(orkestra.agentId, objectToShare, true));
    }

    internal void StopObjectCoords(GameObject objectToShare)
    {
        orkestra.Dispatch(Channel.Application, new ObjCoords(orkestra.agentId, objectToShare, false));
    }

    internal void SendAllAnswersFinished()
    {
        Debug.Log("Sending Orkestra event SendAllAnswersFinished");
        orkestra.Dispatch(Channel.Application, new AnswersFinished(orkestra.agentId, true));
    }

    /// <summary>
    /// Subscriber to the orkestra application channel
    /// </summary>
    /// <param name="sender">Sender of the message</param>
    /// <param name="evt">Event receive</param>
    void AppEventSubscriber(object sender, ApplicationEvent evt)
    {
        // Check the type of the message received
        if (evt.IsEvent(typeof(ReadyToStart)))
        {
            ReadyToStart readyToStart = new ReadyToStart(evt.value);

            if (!readyToStart.sender.Equals(orkestra.agentId))
            {
                RemoteUserReady();
            }
        }
        else if (evt.IsEvent(typeof(YourTurn)))
        {
            YourTurn yT = new YourTurn(evt.value);

            if (!yT.sender.Equals(orkestra.agentId))
            {
                orkestra.Events.Add(() =>
                {
                    SetMyTurn(yT.value);
                });
            }
        }
        else if (evt.IsEvent(typeof(ObjCoords)))
        {
            ObjCoords oC = new ObjCoords(evt.value);

            if (!oC.sender.Equals(orkestra.agentId))
            {
                if (oC.sharing)
                {
                    MoveObjects(oC);
                }
                else
                {
                    StopObjects(oC);
                }
            }
        }
        else if (evt.IsEvent(typeof(AnswersFinished)))
        {
            AnswersFinished aF = new AnswersFinished(evt.value);

            if (!aF.sender.Equals(orkestra.agentId))
            {
                RemoteQuestionsAnswered = true;
            }
        }
    }

    /**
     * Starts coroutine to go back to Practice scene.
     */
    public void Exit()
    {
        Debug.Log("MainApp");
        StartCoroutine(PerformExit());
    }

    /**
     * Removes all game scenes
     */
    IEnumerator PerformExit()
    {
        SceneManager.LoadSceneAsync("Practice", LoadSceneMode.Additive);
        DestroyImmediate(orkestra.gameObject);

        // Unstacking process
        for (int i = SceneManager.sceneCount - 1; i >= 0; i--)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.name.Equals("TidyDrawer") || scene.name.Equals("UntidyDrawer"))
            {
                if (scene.isLoaded)
                {
                    AsyncOperation asyncLoad1 = SceneManager.UnloadSceneAsync(scene.name);
                    while (!asyncLoad1.isDone)
                    {
                        yield return null;
                    }
                }
            }
        }

        ResetParameters();
    }

    private void ResetParameters()
    {
        Camera.main.cullingMask = LayerMask.GetMask("Default", "TransparentFX", "UI", "Ignore Raycast", "Water", "Rig");
        FindObjectOfType<ImageTraking>().enabled = true;
        if (GameObject.Find("UI Canvas") != null)
        {
            Canvas uicanvas = GameObject.Find("UI Canvas").GetComponent<Canvas>();
            if (uicanvas != null)
            {
                uicanvas.enabled = true;
            }
        }

        PlaceOnPlane p = FindObjectOfType<PlaceOnPlane>();
        p.enabled = true;
    }
}

//public enum AnswersTidyDrawer
//{
//    MATERIAL_I_DIDNT_NEED = 0, SPENT_LITTLE_TIME = 1, SPENT_MORE_TIME = 2, I_TOOK_OUT_ONLY_WHAT_I_NEEDED = 3
//}

public enum ItemsForceToTakeOut
{
    NONEOFTHEM = 0, PENCIL = 1, MATHS_NOTEBOOK = 2, HISTORY_BOOK = 3, MATHS_BOOK = 4
}
