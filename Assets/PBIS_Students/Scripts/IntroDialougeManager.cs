using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;
//using System.Text.RegularExpressions;
namespace MirageXR
{
    public class IntroDialougeManager : MonoBehaviour
    {
        public static IntroDialougeManager Instance { get; private set; }

        private const char Separator = '=';
        private readonly Dictionary<string, string> _dialogs = new Dictionary<string, string>();
        private readonly Dictionary<string, string> _aswers = new Dictionary<string, string>();
        private int MaxAnswerNumer = 4;
        private int alienDialogIndex;
        private TextMeshPro alienDialougeText;

        [SerializeField] private float typingSpeed = 0.05f;

        [Header("Answers Pannel")]
        //[SerializeField] private GameObject answersConsole;
        [SerializeField] private GameObject messagePanel;
        [SerializeField] private GameObject AnswersPanel;
        [SerializeField] private GameObject answer1;
        [SerializeField] private GameObject answer2;
        [SerializeField] private GameObject answer3;
        [SerializeField] private GameObject answer4;

        //[SerializeField] private GameObject skipButton;
        [SerializeField] private Transform skipButton;

        [SerializeField] private GameObject LoginPanel;


        /*
            [Header("Dialouge Sentences")]
            [TextArea]
            [SerializeField] private string[] alienDialougeSentences;
        */
        private GameObject alienSpawned;
        private GameObject alienBubble;

        private Animator anim;


        // Start is called before the first frame update
        void Awake()
        {
            Debug.Log("IntroDialougeManager - Awake");
            if (Instance != null)
            {
                Debug.LogError($"{nameof(Instance.GetType)} must only be a single copy!");
                return;
            }

            Instance = this;

            // Show the skipButton since the sencond time use.
            //Debug.Log("IntroDialougeManager - Awake - firstUse: "+ Player.instance.firstUse);
            if (PlayerPrefs.HasKey("appUsage"))
            {
                int appUsage = PlayerPrefs.GetInt("appUsage");
                Debug.Log("IntroDialougeManager - Awake - appUsage: " + appUsage);
                if (appUsage > 0)
                {
                    Debug.Log("IntroDialougeManager - Awake - show skipButton: ");
                    //skipButton.SetActive(true);
                    skipButton.localScale = new Vector3(1, 1, 0);
                }
                else
                {
                    Debug.Log("IntroDialougeManager - Awake - hide skipButton: ");
                    //skipButton.SetActive(false);
                    skipButton.localScale = new Vector3(0, 0, 0);
                    PlayerPrefs.SetInt("appUsage", 0);
                    PlayerPrefs.Save();
                }
                //PlayerPrefs.SetInt("appUsage", appUsage + 1);
            }
            else
            {
                Debug.Log("IntroDialougeManager - Awake - No appUsage - hide skipButton: ");
                PlayerPrefs.SetInt("appUsage", 0);
                PlayerPrefs.Save();
                //skipButton.SetActive(false);
                skipButton.localScale = new Vector3(0, 0, 0);
            }
           
        
            





            //Debug.Log("****   **** Awake IntroDialougeManager");
            GameObject uiCanvas = GameObject.Find("UI Canvas");
            anim = uiCanvas.GetComponent<Animator>();
            /*
            foreach (AnimationClip ac in anim.runtimeAnimatorController.animationClips)
            {
                Debug.Log("IntroDialougeManager - Awake - Animazione nome: " + ac.name);
            }
            */
            Button buttonAnswer = answer1.GetComponent<Button>();
            buttonAnswer.onClick.AddListener(buttonAnswer1Selected);

            buttonAnswer = answer2.GetComponent<Button>();
            buttonAnswer.onClick.AddListener(buttonAnswer2Selected);

            buttonAnswer = answer3.GetComponent<Button>();
            buttonAnswer.onClick.AddListener(buttonAnswer3Selected);

            buttonAnswer = answer4.GetComponent<Button>();
            buttonAnswer.onClick.AddListener(buttonAnswer4Selected);


#if (UNITY_ANDROID && !UNITY_EDITOR) || (UNITY_IOS)
            Debug.Log("****   **** UNITY_ANDROID || UNITY_IOS");
            messagePanel.SetActive(true);
            messagePanelManager messageManager = (messagePanelManager)messagePanel.GetComponent(typeof(messagePanelManager));
            if (messageManager)
            {
                messageManager.SetMoveDeviceMessage();
            }
            LoadDialogText();
#else

            Debug.Log("IntroDialougeManager - Awake - Editor mode");
            showLoginPanel();
#endif

        }

        
        private void OnDestroy()
        {
            Instance = null;
        }
        



        // public Animation mAnim; string[] anims; 
        // Use this for initialization void Start () { anims = new string[mAnim.GetClipCount ()]; int index = 0; foreach (AnimationState mState in mAnim) { anims [index] = mState.clip.name; index++; } }


        void Start()
        {
            // Debug.Log("****   **** START IntroDialougeManager");
            /*
            Button b = gameObject.GetComponent<Button>();
            b.onClick.AddListener(delegate() { StartGame("Level1"); });
            */

        }

        void OnEnable()
        {
            //Debug.Log("****   **** OnEnable IntroDialougeManager");
            PlaceOnPlane.onPlacedObject += AlienPlaced;
        }


        void OnDisable()
        {
            //Debug.Log("****   **** OnDisable IntroDialougeManager");
            PlaceOnPlane.onPlacedObject -= AlienPlaced;
        }


        // Update is called once per frame
        void Update()
        {

        }


        void AlienPlaced()
        {
            Debug.Log("IntroDialougeManager - AlienPlaced - Spawned");
            alienSpawned = GameObject.Find("AlienBubbled(Clone)");
            //alienSpawned = GameObject.Find("AlienIntro(Clone)");
            if (alienSpawned)
            {
                // Debug.Log("****   **** Alieno Spawned OK");
                //alienSpawned.SetActive(false);
                //Transform canvasTransform = alienSpawned.FindDeepChild("BubbleCanvas");
                //Transform canvasTransform = alienSpawned.transform.FindDeepChild("BubbleCanvas");
                //canvasTransform.GetComponent<TextMeshPro>().text = "Hello ARPRO1";;

                /*
                GameObject uiCanvas = GameObject.Find("UI Canvas");
                Transform messagePanelTansform = uiCanvas.transform.FindDeepChild("MessagePanel");
                if (messagePanelTansform)
                {
                    // Debug.Log("****   **** TextBubble trovata");
                    messagePanelTansform.gameObject.SetActive(false);
                }
                */
                messagePanel.SetActive(false);

                GameObject arSessionOrigin = GameObject.Find("AR Session Origin");
                if (arSessionOrigin)
                {
                    //Debug.Log("****   **** IntroDialougeManager- AlienPlaced AR Session Origin TROVATO");
                    ARPlaneManager arPlaneManager = arSessionOrigin.GetComponent<ARPlaneManager>();
                    if (arPlaneManager)
                    {
                        //Debug.Log("****   **** IntroDialougeManager- AlienPlaced ARPlaneManager TROVATO");
                        foreach (var plane in arPlaneManager.trackables)
                        {
                            plane.gameObject.SetActive(false);
                        }
                        arPlaneManager.enabled = false;
                    }
                }



                Transform textBubbleTansform = alienSpawned.transform.FindDeepChild("TextBubble");
                if (textBubbleTansform)
                {
                    //Debug.Log("****   **** canvasTransform");
                    textBubbleTansform.gameObject.SetActive(false);
                }

                // Questo è ok
                // alienSpawned.GetComponentInChildren<TextMeshPro>().SetText("Hello ARPRO 34");

                alienDialougeText = alienSpawned.GetComponentInChildren<TextMeshPro>();

                /*
                Debug.Log("****   **** Alieno Spawned Pippo");
                TextBubble textBubble = alienSpawned.GetComponent<TextBubble>();
                textBubble.Setup("pippo");
                Debug.Log("****   **** Alieno Spawned Pippo done");
                */
                Intro();
            }
            else
            {
                //Debug.Log("****   **** Alieno Spawned ERROR");
            }
        }

        private void Intro()
        {
            //Debug.Log("****   **** StartDialog");
            alienDialogIndex = 1;
            StartCoroutine(ShowDialog());
        }

        /// <summary>
        /// Manage dialog text that are coded in a text file like: 
        /// dialog1=Hello, earthlings!
        /// dialog2=I'm ARPRO!\\n I'm from ARGON.
        /// </summary>
        IEnumerator ShowDialog()
        {
            AnswersPanel.SetActive(false);

            //Debug.Log("****   **** Intro");

            //yield return new WaitForSeconds(1.0f);
            Transform textBubbleTansform = alienSpawned.transform.FindDeepChild("TextBubble");
            if (textBubbleTansform)
            {
                //Debug.Log("****   **** TextBubble trovata");
                textBubbleTansform.gameObject.SetActive(true);
                alienDialougeText = alienSpawned.GetComponentInChildren<TextMeshPro>();
            }
            else
            {

                //Debug.Log("****   **** TextBubble NON trovata");
            }

            alienDialougeText.text = string.Empty;

            //Debug.Log("****   **** Dialog: "+"dialog"+alienDialogIndex);
            //Debug.Log("****   **** Dialog: "+alienDialogIndex+" text: "+ _dialogs["dialog"+alienDialogIndex] );
            foreach (char letter in _dialogs["dialog" + alienDialogIndex].ToCharArray())
            {
                alienDialougeText.text += letter;
                //Debug.Log("****   **** Intro letter:" + letter);
                yield return new WaitForSeconds(typingSpeed);
            }
            yield return new WaitForSeconds(0.5f);
            if (alienDialogIndex < _dialogs.Count)
            {
                Debug.Log("IntroDialougeManager - ShowDialog - ShowAnswer alienDialogIndex: " + alienDialogIndex);
                ShowAnswer();
            }
            else
            {
                Debug.Log("IntroDialougeManager - ShowDialog - showLoginPanel alienDialogIndex: " + alienDialogIndex);
                showLoginPanel();
            }

        }


        /// <summary>
        /// Manage dialog answers that are coded in a text file like: 
        /// answer1-1=Hello ARPRO!
        /// answer1-2=Welcome to\\n Earth! >
        /// </summary>
        private void ShowAnswer()
        {
            //Debug.Log("****   **** ShowAnswers");
            if (!Player.instance.skipped)
            {
                AnswersPanel.SetActive(true);
                Transform buttonTansform;
                TextMeshProUGUI buttontext;
                string answer, labelAnswer, buttonName;
                for (int i = 1; i <= MaxAnswerNumer; i++)
                {
                    buttonName = "btnAnswer" + i;
                    buttonTansform = AnswersPanel.transform.FindDeepChild(buttonName);
                    labelAnswer = "answer" + alienDialogIndex + "-" + i;
                    //Debug.Log("****   **** Punto 1");
                    //answer = _dialogs[labelAnswer];
                    //if ((_dialogs.ContainsKey(labelAnswer)) && (_dialogs[labelAnswer]!=null))
                    if ((_aswers.ContainsKey(labelAnswer)) && (_aswers[labelAnswer] != null))
                    {
                        //Debug.Log("****   **** Punto 2");
                        //answer = _dialogs[labelAnswer];
                        answer = _aswers[labelAnswer];
                        buttonTansform.gameObject.SetActive(true);
                        buttontext = buttonTansform.gameObject.GetComponentInChildren<TextMeshProUGUI>();
                        //Debug.Log("****   **** Answer " + labelAnswer + ": " + answer);
                        buttontext.text = answer;
                    }
                    else
                    {
                        //Debug.Log("****   **** Answer " + labelAnswer + " NULLA ");
                        buttonTansform.gameObject.SetActive(false);
                        break;
                    }
                }
            }
            //Debug.Log("****   **** button done");
        }


        /// <summary>
        ///    -- verificare se va eliminato
        /// </summary>
        /// <returns></returns>
        IEnumerator ShowLogin()
        {

            Debug.Log("****   ****  IntroDialougeManager - ShowLogin !!!!! ");
            AnswersPanel.SetActive(false);

            

            yield return new WaitForSeconds(1.0f);
            Transform textBubbleTansform = alienSpawned.transform.FindDeepChild("TextBubble");
            if (textBubbleTansform)
            {
                // Debug.Log("****   **** TextBubble trovata");
                textBubbleTansform.gameObject.SetActive(false);
            }
            else
            {

                //Debug.Log("****   **** TextBubble NON trovata");
            }
            yield return new WaitForSeconds(1.0f);
            ShowAnswer();
        }


        private void LoadDialogText()
        {
            //Debug.Log("****   **** Load dialogs ...");

            var file = Resources.Load<TextAsset>("Text/introDialogs");
            if (file == null)
            {
                // 
            }

            foreach (var line in file.text.Split('\n'))
            {
                var prop = line.Split(Separator);
                // Debug.Log("Numero parametri per Linea: " + prop.Length);
                // To accep also comment lines
                if (prop.Length == 2)
                {
                    string key = prop[0];
                    if (key.StartsWith("dialog"))
                    {
                        //Debug.Log("Dialog: " + key);
                        _dialogs[prop[0]] = prop[1];
                    }
                    else
                    {
                        //Debug.Log("Answer: " + key);
                        _aswers[prop[0]] = prop[1];
                    }
                }
                else
                {
                    //Debug.Log("****   **** NOT key=Value - Probably iti is a comment");
                }


            }
        }


        public void skipButtonSelected()
        {
            Debug.Log("IntroDialougeManager - skipButtonSelected");
            AnswersPanel.SetActive(false);
            Player.instance.skipped = true;
            Debug.Log("IntroDialougeManager - skipButtonSelected AnswersPanel hided");
            showLoginPanel();

        }

        public void buttonAnswer1Selected()
        {
            //Debug.Log("****   **** buttonAnswer1Selected");
            AnswersPanel.SetActive(false);
            showLoginPanel();
            //buttonAnswerSelected(1);
        }

        public void buttonAnswer2Selected()
        {
            //Debug.Log("****   **** buttonAnswer2Selected");
            buttonAnswerSelected(2);
        }

        public void buttonAnswer3Selected()
        {
            //Debug.Log("****   **** buttonAnswer3Selected");
            //Animator anim = LoginPanel.transform.parent.GetComponent<Animator>();
            //anim.Play("LOGIN_IN");
            buttonAnswerSelected(3);
        }

        public void buttonAnswer4Selected()
        {
            //Debug.Log("****   **** buttonAnswer4Selected");
            buttonAnswerSelected(4);
        }

        public void buttonAnswerSelected(int num)
        {
            Debug.Log("****   **** IntroDialougeManager - button Selected");
            AnswersPanel.SetActive(false);
            alienDialogIndex++;
            StartCoroutine(ShowDialog());
            switch (num)
            {
                case 1:
                    Debug.Log("****   **** IntroDialougeManager - button 1 selected");
                    break;
                case 2:
                    Debug.Log("****   **** IntroDialougeManager - button 2 selected");
                    break;
                case 3:
                    Debug.Log("****   **** IntroDialougeManager - button 3 selected");
                    break;
                case 4:
                    Debug.Log("****   **** IntroDialougeManager - button 4 selected");
                    break;
            }
        }



        public void showLoginPanel()
        {
            Debug.Log("IntroDialougeManager - showLoginPanel");

            //skipButton.SetActive(false);
            skipButton.localScale = new Vector3(0, 0, 0);
            Debug.Log("IntroDialougeManager - showLoginPanel - Hided Skip by scaling");

            //#if UNITY_ANDROID || UNITY_IOS
#if (UNITY_ANDROID && !UNITY_EDITOR) || (UNITY_IOS)
        Debug.Log("IntroDialougeManager - showLoginPanel - textBubbleTansform...");
        if (alienSpawned!=null){
            Transform textBubbleTansform = alienSpawned.transform.FindDeepChild("TextBubble");
            Debug.Log("IntroDialougeManager - showLoginPanel - Hide textBubbleTansform...");
            if (textBubbleTansform!=null){
                Debug.Log("IntroDialougeManager - showLoginPanel - Hide textBubbleTansform...1");
                textBubbleTansform.gameObject.SetActive(false);
                Debug.Log("IntroDialougeManager - showLoginPanel - Hide textBubbleTansform...2");
            }
        }
        Debug.Log("IntroDialougeManager - showLoginPanel - Hide textBubbleTansform ... done!");
#else
            // Editor Mode
            /*
            GameObject uiCanvas = GameObject.Find("UI Canvas");
            if (uiCanvas)
            {
                Transform messagePanelTansform = uiCanvas.transform.FindDeepChild("MessagePanel");
                if (messagePanelTansform)
                {
                    // Debug.Log("****   **** TextBubble trovata");
                    messagePanelTansform.gameObject.SetActive(false);
                    GameObject messagePanel = messagePanelTansform.gameObject;
                    messagePanelManager messageManager = (messagePanelManager)messagePanel.GetComponent(typeof(messagePanelManager));
                    if (messageManager)
                    {
                        messageManager.ClearMessage();
                    }
                    else
                    {
                        Debug.Log("IntroDialougeManager - showLoginPanel -   NO MESSAGEMANAGER");
                    }
                }
            }
            */

            /*
            Debug.Log("IntroDialougeManager - showLoginPanel - Hide messagePanel ...");
            messagePanel.SetActive(false);
            Debug.Log("IntroDialougeManager - showLoginPanel - Hide messagePanel ... done!");
            messagePanelManager messageManager = (messagePanelManager)messagePanel.GetComponent(typeof(messagePanelManager));
            Debug.Log("IntroDialougeManager - showLoginPanel -  messageManager");
            {
                Debug.Log("IntroDialougeManager - showLoginPanel -  messageManager clear ...");
                messageManager.ClearMessage();
                Debug.Log("IntroDialougeManager - showLoginPanel -  messageManager clear ... done!");
            }
            */
#endif
            // Hide message plane 
            Debug.Log("IntroDialougeManager - showLoginPanel - Hide messagePanel ...");
            if (messagePanel != null)
            {
                messagePanel.SetActive(false);
                
                messagePanelManager messageManager = (messagePanelManager)messagePanel.GetComponent(typeof(messagePanelManager));
                if (messageManager!=null){
                    Debug.Log("IntroDialougeManager - showLoginPanel -  messageManager clear ...");
                    messageManager.ClearMessage();
                }
            }
            Debug.Log("IntroDialougeManager - showLoginPanel - Hide messagePanel ... done!");

            // Hide any plan detector
            GameObject arSessionOrigin = GameObject.Find("AR Session Origin");
            if (arSessionOrigin)
            {
                ARPlaneManager arPlaneManager = arSessionOrigin.GetComponent<ARPlaneManager>();
                if (arPlaneManager)
                {
                    foreach (var plane in arPlaneManager.trackables)
                    {
                        plane.gameObject.SetActive(false);
                    }
                    arPlaneManager.enabled = false;
                }
            }

            anim.Play("LOGIN_IN");
        }




        public async void PostLoginSucceed()
        //public void PostLoginSucceed()
        {
            Debug.Log("****   **** IntroDialougeManager - Start Alien dialogs after a LoginSucceed");
            StartCoroutine(ShowRoutinePanel());
            //Add RoutineSelectionScene
            SceneManager.LoadSceneAsync("RoutineSelection", LoadSceneMode.Additive);
        }


        IEnumerator ShowRoutinePanel()
        {
            Debug.Log("****   **** IntroDialougeManager - ShowRoutinePanel");
            AnswersPanel.SetActive(false);
   
            /*
            yield return new WaitForSeconds(1.0f);
            Transform textBubbleTansform = alienSpawned.transform.FindDeepChild("TextBubble");
            if (textBubbleTansform)
            {
                // Debug.Log("****   **** TextBubble trovata");
                textBubbleTansform.gameObject.SetActive(false);
            }
            else
            {

                //Debug.Log("****   **** TextBubble NON trovata");
            }*/
            yield return new WaitForSeconds(1.0f);
            anim.Play("ROUTINE_IN");
        }

    }
}
