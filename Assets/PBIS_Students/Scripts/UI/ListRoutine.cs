using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using TMPro;
using UnityEditor;
using UnityEngine;
using MirageXR;
using UnityEngine.Networking;
using Newtonsoft.Json.Linq;


public class ListRoutine : BaseView
{

    public static RoutineListView Instance { get; private set; }

    //[SerializeField] private Button _btnLogin;
    //[SerializeField] private Button _btnSettings;
    //[SerializeField] private Button _btnHelp;
    //[SerializeField] private Button _btnAddActivity;
    //[SerializeField] private TMP_InputField _inputFieldSearch;
    [SerializeField] private Transform _listTransform;
    [SerializeField] private RoutineLIstItem _listListItemPrefab;
    //[SerializeField] private LoginView _loginViewPrefab;
    //[SerializeField] private SettingsView _settingsViewPrefab;
    //public Button BtnAddActivity => _btnAddActivity;


    private List<SessionContainer> _content;
    private readonly List<RoutineLIstItem> _items = new List<RoutineLIstItem>();
    private bool _interactable = true;


    public bool interactable
    {
        get
        {
            return _interactable;
        }
        set
        {
            _interactable = value;
            //_btnAddActivity.interactable = value;
            //_items.ForEach(t => t.interactable = value);
        }
    }

    public override async void Initialization(BaseView parentView)
    {
        Debug.Log("ListRoutine - Initialization");
        base.Initialization(parentView);
        //_btnLogin.onClick.AddListener(OnLoginClick);
        //_btnSettings.onClick.AddListener(OnSettingsClick);
        //_btnHelp.onClick.AddListener(OnHelpClick);
        //_btnAddActivity.onClick.AddListener(OnAddActivityClick);
        //_inputFieldSearch.onValueChanged.AddListener(OnInputFieldSearchChanged);
        /*if (!DBManager.LoggedIn && DBManager.rememberUser)
        {
            await AutoLogin();
        }*/
        UpdateListView();
    }

    private async Task AutoLogin()
    {
        Debug.Log("ListRoutine - AutoLogin");
        if (!LocalFiles.TryToGetUsernameAndPassword(out var username, out var password)) return;

        LoadView.Instance.Show();
        await RootObject.Instance.moodleManager.Login(username, password);
        LoadView.Instance.Hide();
    }


    private static async Task<List<SessionContainer>> GetContent()
    {
        Debug.Log("ListRoutine - GetContent");
        var dictionary = new Dictionary<string, SessionContainer>();
        
        var localList = await LocalFiles.GetDownloadedActivities();
        /*localList.ForEach(t =>
        {
            if (dictionary.ContainsKey(t.id))
            {
                dictionary[t.id].Activity = t;
            }
            else
            {
                dictionary.Add(t.id, new SessionContainer { Activity = t });
            }
        });
        */
        var remoteList = await RootObject.Instance.moodleManager.GetArlemList();
        remoteList?.ForEach(t =>
        {
            if (dictionary.ContainsKey(t.sessionid))
            {
                dictionary[t.sessionid].Session = t;
            }
            else
            {
                dictionary.Add(t.sessionid, new SessionContainer { Session = t });
            }
        });

        return dictionary.Values.ToList();


    }



    public void UpdateListView_NEW()
    {
        Debug.Log("ListRoutine - UpdateListView StartCoroutine");
        StartCoroutine(GetDataCNR("https://arete.ucd.ie//mod/arete/classes/webservice/getdata_from_db.php"));
    }





    public async void UpdateListView()
    {
        Debug.Log("ListRoutine - UpdateListView");
        _content = await GetContent();
#if UNITY_EDITOR
        if (!EditorApplication.isPlaying) return;
#endif
        _items.ForEach(item => Destroy(item.gameObject));
        _items.Clear();
        if (_content.Count > 0)
        {
            Debug.Log("ListRoutine - UpdateListView #routine:"+ _content.Count);
        }
        else
        {
            Debug.Log("ListRoutine - UpdateListView NO routine");
        }
        _content.ForEach(content =>
        {
            var item = Instantiate(_listListItemPrefab, _listTransform);
            item.Initialization(content);
            _items.Add(item);
        });
    }

    private void OnSettingsClick()
    {
        //            PopupsViewer.Instance.Show(_settingsViewPrefab);

    }

    private void OnLoginClick()
    {
        //PopupsViewer.Instance.Show(_loginViewPrefab);
    }

    private void OnHelpClick()
    {
        /*if (!TutorialManager.Instance.IsTutorialRunning)
        {
            TutorialDialog tDialog = RootView.Instance.TutorialDialog;
            tDialog.Toggle();
        }
        else
        {
            TutorialManager.Instance.CloseTutorial();
        }*/

    }

    private async void OnAddActivityClick()
    {
        Debug.Log("ListRoutine - OnAddActivityClick");
        LoadView.Instance.Show();
        interactable = false;
        await RootObject.Instance.editorSceneService.LoadEditorAsync();
        RootObject.Instance.activityManager.CreateNewActivity();
        interactable = true;
        LoadView.Instance.Hide();
        EventManager.NotifyOnNewActivityCreationButtonPressed();
    }

    private void OnInputFieldSearchChanged(string text)
    {
        foreach (var item in _items)
        {
            var enable = string.IsNullOrEmpty(text) || item.activityName.ToLower().Contains(text.ToLower());
            item.gameObject.SetActive(enable);
        }
    }





    IEnumerator GetDataCNR(string uri)
    {
        Debug.Log("ListRoutine - GetDataCNR");

        WWWForm form = new WWWForm();
        form.AddField("request", "arlemlist");
        Debug.Log("ListRoutine - GetDataCNR - Form ");

        UnityWebRequest www = UnityWebRequest.Post(uri, form);

        Debug.Log("ListRoutine - GetDataCNR - Post ");
        yield return www.SendWebRequest();
        Debug.Log("ListRoutine - GetDataCNR - Send ");


        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("ListRoutine - GetCustomDataCNR - ERROR: " + www.error);
        }
        else
        {
            Debug.Log("ListRoutine - GetDataCNR - Post complete response code: "+ www.responseCode);

            string responseText = www.downloadHandler.text;
            Debug.Log("ListRoutine - GetDataCNR - Post complete response code: " + responseText);

            
            Debug.Log("ListRoutine - UpdateListView");
            //_content = await GetContent();
            _content = GetArlemContent(responseText);

            _items.ForEach(item => Destroy(item.gameObject));
            _items.Clear();
            if (_content.Count > 0)
            {
                Debug.Log("ListRoutine - UpdateListView #routine:" + _content.Count);
            }
            else
            {
                Debug.Log("ListRoutine - UpdateListView NO routine");
            }
            _content.ForEach(content =>
            {
                var item = Instantiate(_listListItemPrefab, _listTransform);
                item.Initialization(content);
                _items.Add(item);
            });
            
        }
    }



    //private Task<List<SessionContainer>> GetArlemContent(string jsonArlem)
    private List<SessionContainer> GetArlemContent(string jsonArlem)
    {
        Debug.Log("ListRoutine - GetArlemContent");
        var dictionary = new Dictionary<string, SessionContainer>();
        //var remoteList = await RootObject.Instance.moodleManager.GetArlemList();
        var remoteList = ParseArlemListJson(jsonArlem);
        remoteList?.ForEach(t =>
        {
            if (dictionary.ContainsKey(t.sessionid))
            {
                dictionary[t.sessionid].Session = t;
            }
            else
            {
                dictionary.Add(t.sessionid, new SessionContainer { Session = t });
            }
        });

        return dictionary.Values.ToList();
    }




    private List<Session> ParseArlemListJson(string json)
    {
        Debug.Log("MoodleManager - ParseArlemListJson");
        const string emptyJson = "[]";

        try
        {
            var arlemList = new List<Session>();

            if (json == emptyJson)
            {
                Debug.Log("Probably there is no public activity on the server.");
                return arlemList;
            }

            var parsed = JObject.Parse(json);
            foreach (var pair in parsed)
            {

                if (pair.Value != null)
                {
                    var arlem = pair.Value.ToObject<Session>();

                    // CNR
                    Debug.Log("MoodleManager - ParseArlemListJson #arlemList: " + arlemList.Count);

                    Debug.Log("MoodleManager - ParseArlemListJson Activity : " + arlem.userid + " - " + arlem.author);
                    string myTeacher = PlayerPrefs.GetString("myTeacher");
                    if ((arlem.userid != null) && (arlem.userid.Length > 0) && (myTeacher != null) && (myTeacher.Length > 0) && (arlem.userid == myTeacher))
                    {
                        Debug.Log("MoodleManager - ParseArlemListJson Activity FITERED By Author for : " + arlem.userid + " - " + arlem.author);
                        //take name of the activity
                        if (arlem.activity_json != null)
                        {
                            JObject activity_jsonObject = JObject.Parse(arlem.activity_json);
                            var arlemActivity = activity_jsonObject.ToObject<ARLEMActivity>();
                            if ((arlemActivity.name != null) && (arlemActivity.name.Length > 0))
                            {
                                string lessonSelected = PlayerPrefs.GetString("lessonSelected");
                                Debug.Log("MoodleManager - ParseArlemListJson lessonSelected : " + lessonSelected);
                                Debug.Log("MoodleManager - ParseArlemListJson arlemActivity : " + arlemActivity.name);
                                if ((lessonSelected != null) && (lessonSelected.Length > 0) && (arlemActivity.name.StartsWith(lessonSelected)))
                                {
                                    Debug.Log("MoodleManager - ParseArlemListJson fitered " + arlem.author + ": " + arlemActivity.name);
                                    arlem.name = arlemActivity.name;
                                    arlemList.Add(arlem);
                                }
                            }
                            else
                            {
                                Debug.Log("MoodleManager - ParseArlemListJson arlemActivity null ");
                            }
                        }

                    }
                }
            }

            return arlemList;
        }
        catch (Exception e)
        {
            Debug.LogError($"ParseArlemListJson error\nmessage: {e}");
            return null;
        }
    }

    //take name of the activity
    public class ARLEMActivity
    {
        public string id;
        public string name;
    }
    



}
