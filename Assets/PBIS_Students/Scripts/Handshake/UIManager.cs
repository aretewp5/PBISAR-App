using OrkestraLibImp;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class UIManager : MonoBehaviour, IGameController
{
    public Button Hello { get; private set; }
    public Button HelloPunch { get; private set; }
    public Button HighFive { get; private set; }
    public Button Reset { get; private set; }
    public TMP_Text Label { get; private set; }
    public Button Back { get; private set; }

    List<Button> buttons = new List<Button>();

    public GameObject trackedImagePrefab;
    public ARTrackedImageManager arTrackedImageManager;
    SkinnedMeshRenderer[] renders;

    public CharacterControl cc;
    public Canvas Canvas_DetectMarker;
    public Canvas canvas;

    private bool uiStarted = false;

    public GameObject textWait;
    public GameObject textTitle;

    /// <summary>
    /// Store renders component of the gameobjects
    /// Disable them until the image is tracked
    /// </summary>
    private void Awake()
    {
        renders = trackedImagePrefab.GetComponentsInChildren<SkinnedMeshRenderer>();
#if !UNITY_EDITOR
        RendererState(false); // Hide meshes
#endif
        arTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
    }

    // Start is called before the first frame update
    /// <summary>
    /// Store the ui elements in objects, give them an event listener
    /// </summary>
    void Start()
    {
        this.textWait.SetActive(false);
        Hello = FindButton("Button_Hello");
        Hello.onClick.AddListener(() =>
        {
            cc.SendHandshake(HandShakesTypes.SALUTO);
            this.textWait.SetActive(cc.answerReceive == null);
            this.textTitle.SetActive(false);
            HideButtons();
        });

        HelloPunch = FindButton("Button_FistBump");
        HelloPunch.onClick.AddListener(() =>
        {
            cc.SendHandshake(HandShakesTypes.PUGNETTO);
            this.textWait.SetActive(cc.answerReceive == null);
            this.textTitle.SetActive(false);
            HideButtons();
        });

        HighFive = FindButton("Button_HighFive");
        HighFive.onClick.AddListener(() =>
        {
            cc.SendHandshake(HandShakesTypes.HIGHFIVE);
            this.textWait.SetActive(cc.answerReceive == null);
            this.textTitle.SetActive(false);
            HideButtons();
        });

        Reset = FindButton("Button_TryAgain");
        Reset.onClick.AddListener(() =>
        {
            EnableButtons();
            cc.ResetSession();
            StartUI();
            cc.SendReset();
        });

        Label = GameObject.Find("Text_Question").GetComponent<TMP_Text>();
        AddButtons(Hello, HelloPunch, HighFive);
#if UNITY_EDITOR
        StartUI();
#endif
        canvas.enabled = false;
    }

    public void Exit()
    {
        FindObjectOfType<PracticeGamesSceneManager>()?.Exit();
    }

    private Button FindButton(string buttonName)
    {
        return GameObject.Find(buttonName).GetComponent<Button>();
    }

    /// <summary>
    /// Start state of the UI Text 
    /// </summary>
    public void StartUI()
    {
        Debug.Log("----- StartUI -----");
        EnableButtons();
        Canvas_DetectMarker.enabled = false;
        Reset.gameObject.SetActive(false);
        if (cc != null)
        {
            if (cc.selectedCharacter.Equals(CharacterTypes.Boy))
            {
                Label.text = TextAnswers.RESPONDTEACHER;
            }
            else if (cc.selectedCharacter.Equals(CharacterTypes.Woman))
            {
                Label.text = TextAnswers.RESPONDSTUDENT;
            }
        }
        canvas.enabled = true;
    }

    public void showQuestion()
    {
        this.textWait.SetActive(false);
        this.textTitle.SetActive(true);
    }

    public void hideWait()
    {
        this.textWait.SetActive(false);
    }

    public void ShowTryAgain()
    {
        Reset.gameObject.SetActive(true);
    }

    public void HideButtons()
    {
        foreach (Button b in this.buttons)
        {
            b.gameObject.SetActive(false);
        }
    }

    public void EnableButtons()
    {
        this.buttons.ForEach((button) => {
            button.gameObject.SetActive(true);
            button.interactable = true;
            ColorButton(button, Color.white);
        });
    }

    private void ColorButton(Button b, Color color)
    {
        b.GetComponent<Image>().color = color;
    }

    /// <summary>
    /// Store all the buttons in a list
    /// </summary>
    /// <param name="buttons"></param>
    void AddButtons(params Button[] buttons)
    {
        this.buttons.AddRange(buttons);
    }

    /**
     * --------------------- AR IMAGE TRACKER ---------------------
    */
    /// <summary>
    /// Subscribe to the Ar tracked image change method
    /// </summary>
    private void OnEnable()
    {
        if (arTrackedImageManager)
        {
            arTrackedImageManager.trackedImagesChanged += ImageChanged;
        }
    }

    /// <summary>
    /// Unsubscribe to the tracked image change method and disconnect from orkestra
    /// </summary>
    private void OnDisable()
    {
        if (arTrackedImageManager)
        {
            arTrackedImageManager.trackedImagesChanged -= ImageChanged;
        }
    }

    /// <summary>
    /// When the tracked Image is detected/updated the update image method is called
    /// The game object active value is set to false when the tracked image isnt being the detected by the device
    /// </summary>
    /// <param name="eventArgs">Events of the AR Tracked image manager</param>
    void ImageChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (ARTrackedImage trackedImage in eventArgs.added)
        {
            //Debug.Log("----- ImageChanged ADDED -----");
            UpdateImage(trackedImage);
            RendererState(true);
            StartUI();
        }
        foreach (ARTrackedImage trackedImage in eventArgs.updated)
        {
            //Debug.Log("----- ImageChanged updated -----");
            if (!uiStarted)
            {
                RendererState(true);
                StartUI();
                this.uiStarted = true;
            }
            UpdateImage(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {
            //Debug.Log("----- ImageChanged removed -----");
            RendererState(false);
        }
    }

    /// <summary>
    /// Update the rotation of the game object if the marker changes it's rotation
    /// </summary>
    /// <param name="trackedImage">marker tracked by the AR App</param>
    private void UpdateImage(ARTrackedImage trackedImage)
    {
        trackedImagePrefab.transform.SetPositionAndRotation(trackedImage.transform.position, trackedImage.transform.rotation);
    }

    /// <summary>
    /// Enable or disable renders components from the characters
    /// </summary>
    /// <param name="active"></param>
    void RendererState(bool active)
    {
        foreach (SkinnedMeshRenderer a in renders)
        {
            a.enabled = active;
        }
    }
}

public static class TextAnswers
{
    public const string RESPONDTEACHER = "How do you properly greet the Teacher?";
    public const string RESPONDSTUDENT = "How do you properly greet the Student?";
    public const string BOTHANSWERSCORRECT = "Both Answers were correct! Congratulations! \n\n Now you can go back by clicking the back button.";
    public const string STUDENTANSWERCORRECT = "Only the student's answer was correct...";
    public const string TEACHERANSWERCORRECT = "Only the teacher's answer was correct...";
    public const string BOTHANSWERSINCORRECT = "Both answers were incorrect :'( ... ";
    public const string EMPTY = "";
}
