using OrkestraLibImp;
using OrkestraLib;
using OrkestraLib.Exceptions;
using OrkestraLib.Message;
using OrkestraLib.Plugins;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static OrkestraLib.Orkestra;

public class CharacterControl : MonoBehaviour
{
    public string selectedCharacter;
    public string remoteSelectedCharacter;

    private GameObject character;
    private GameObject remoteCharacter;

    private Animator animator;
    private Animator remoteAnimator;

    public Canvas connectingCanvas;

    internal string answerReceive;
    internal string answerEmitted;

    public UIManager uim;

    private Queue<Action> animEvents = new Queue<Action>();

    private bool isAnim = false;
    private bool allAnswered = false;
    private OrkestraImpl orkestra;

    private int numTries = 1;
    private int maxTries = 4;

    /// <summary>
    /// Store gameObjects and animators component from the characters
    /// Init Orkestra connection
    /// </summary>
    void Start()
    {
        orkestra = FindObjectOfType<OrkestraImpl>();
        uim.cc = this;
        selectedCharacter = PlayerPrefs.GetString("Character");

        if (selectedCharacter.Equals(CharacterTypes.Boy))
        {
            remoteSelectedCharacter = CharacterTypes.Woman;
        }
        else
        {
            remoteSelectedCharacter = CharacterTypes.Boy;
        }

        remoteCharacter = GameObject.Find(remoteSelectedCharacter + "_Game");
        remoteAnimator = remoteCharacter.GetComponent<Animator>();

        character = GameObject.Find(selectedCharacter + "_Game");
        animator = character.GetComponent<Animator>();

        // Subscribe to Application Events so we receive the events from the Application Channel
        orkestra.ApplicationEvents += AppEventSubscriber;
    }

    public void Exit()
    {
        FindObjectOfType<PracticeGamesSceneManager>()?.Exit();
    }

    /// <summary>
    /// Reset the answers
    /// </summary>
    internal void ResetSession()
    {
        this.answerEmitted = null;
        this.answerReceive = null;
        this.allAnswered = false;
    }

    /// <summary>
    /// Send the reset message through Orkestra
    /// </summary>
    internal void SendReset()
    {
        orkestra.Dispatch(Channel.Application, new TryAgainMessage(orkestra.agentId, true));
    }

    internal void SendHandshake(string handShake)
    {
        answerEmitted = handShake;
        orkestra.Dispatch(Channel.Application, new HandShakeMessage(orkestra.agentId, handShake));
    }

    private void Update()
    {
        if (answerEmitted != null && answerReceive != null && !allAnswered)
        {
            allAnswered = true;
            orkestra.Events.Add(() =>
            {
                Animate(answerReceive, true);
                Animate(answerEmitted, false);
                uim.Label.text = TextAnswers.EMPTY;
            });

            animEvents.Enqueue(() =>
            {
                GameEnds();
            });
        }

        bool localAnim = animator.GetCurrentAnimatorStateInfo(0).IsName(answerEmitted);
        bool remoteAnim = remoteAnimator.GetCurrentAnimatorStateInfo(0).IsName(answerReceive);
        bool anims = localAnim || remoteAnim;

        if (anims)
        {
            isAnim = true;
        }
        else if (isAnim)
        {
            isAnim = false;
            if (animEvents.Count > 0)
            {
                animEvents.Dequeue().Invoke();
                Debug.Log("Invoke");
            }
        }
    }

    /// <summary>
    /// Trigger the character's animation. Also establish when the game should restart
    /// </summary>
    /// <param name="HandShake">Type of Hand shake</param>
    /// <param name="remote">if its true, its receive from the other devices. 
    /// If its false the animation is trigger by the local player </param>
    public void Animate(string HandShake, bool remote)
    {
        if (remote)
        {
            remoteAnimator.SetTrigger(HandShake);
        }
        else
        {
            animator.SetTrigger(HandShake);
        }
    }

    /// <summary>
    /// Show the Result
    /// Show the try again button
    /// </summary>
    private void GameEnds()
    {
        uim.HideButtons();
        uim.showQuestion();
        if (answerReceive != null && answerEmitted != null)
        {
            if (answerReceive.Equals(HandShakesTypes.SALUTO) && answerEmitted.Equals(HandShakesTypes.SALUTO))
            {
                Debug.Log("---- Correct answers in try number " + numTries + ". ----");
                uim.Label.text = TextAnswers.BOTHANSWERSCORRECT;
                // Add points to the player if is the first time it completes the exercise
                if (!PlayerPrefs.HasKey("HandshakePoints"))
                {
                    Debug.Log("---- Adding " + (maxTries - numTries) + " points to the player. ----");
                    Player.instance.points += maxTries - numTries;
                    PlayerPrefs.SetInt("HandshakePoints", maxTries - numTries);
                }
                return;
            }
            else if (answerReceive.Equals(HandShakesTypes.SALUTO))
            {
                if (selectedCharacter.Equals(CharacterTypes.Woman))
                {
                    uim.Label.text = TextAnswers.STUDENTANSWERCORRECT;
                }
                else
                {
                    uim.Label.text = TextAnswers.TEACHERANSWERCORRECT;
                }
            }
            else if (answerEmitted.Equals(HandShakesTypes.SALUTO))
            {
                if (selectedCharacter.Equals(CharacterTypes.Woman))
                {
                    uim.Label.text = TextAnswers.TEACHERANSWERCORRECT;
                }
                else
                {
                    uim.Label.text = TextAnswers.STUDENTANSWERCORRECT;
                }
            }
            else
            {
                uim.Label.text = TextAnswers.BOTHANSWERSINCORRECT;
            }
            Debug.Log("---- Incorrect answers in try number " + numTries + ". ----");
            if (numTries < (maxTries - 1))
            {
                numTries++;
                uim.ShowTryAgain();
            }
            else
            {
                uim.Label.text = "Sorry, the answers were not correct, but you can ask your peers or teachers for help and try again! \n\n Now you can go back by clicking the back button.";
                if (!PlayerPrefs.HasKey("HandshakePoints"))
                {
                    PlayerPrefs.SetInt("HandshakePoints", 0);
                }
            }
        }
    }

    /// <summary>
    /// Subscriber to the orkestra application channel
    /// </summary>
    /// <param name="sender">Sender of the message</param>
    /// <param name="evt">Event receive</param>
    void AppEventSubscriber(object sender, ApplicationEvent evt)
    {
        // Check the type of the message receive
        if (evt.IsEvent(typeof(HandShakeMessage)))
        {
            HandShakeMessage handShakeMessage = new HandShakeMessage(evt.value);
            if (!handShakeMessage.sender.Equals(orkestra.agentId))
            {
                Debug.Log("Answer received: " + handShakeMessage.handShake);
                answerReceive = handShakeMessage.handShake;
                uim.hideWait();
            }
        }
        else if (evt.IsEvent(typeof(TryAgainMessage)))
        {
            TryAgainMessage tryAgainMessage = new TryAgainMessage(evt.value);
            if (!tryAgainMessage.sender.Equals(orkestra.agentId))
            {
                if (tryAgainMessage.retry)
                {
                    orkestra.Events.Add(() =>
                    {
                        ResetSession();
                        uim.StartUI();
                    });
                }
            }
        }
    }
}

public static class CharacterTypes
{
    public const string Boy = "boy_B";
    public const string Woman = "woman_C";
}

public static class HandShakesTypes
{
    public const string HIGHFIVE = "HighFive";
    public const string PUGNETTO = "Pugnetto";
    public const string SALUTO = "Saluto";
    public const string STRETTAMANO = "StrettaMano";
}
