namespace Handshake
{
    using UnityEngine;
    using UnityEngine.UI;
    using OrkestraLibImp;

    public class ChooseCharacter : MonoBehaviour, IGameController
    {
        public Canvas canvas;
        public Camera camera;

        public GameObject teacherCharacter, studentCharacter;

        public GameObject panelInfo;
        public Button infoButton;
        public GameObject textTitle;

        private bool characterChoose = false;
        private OrkestraImpl orkestra;

        // Start is called before the first frame update
        void Start()
        {
            this.panelInfo.SetActive(false);
            this.canvas.worldCamera = camera;
            this.orkestra = FindObjectOfType<OrkestraImpl>();
        }

        public void Exit()
        {
            FindObjectOfType<PracticeGamesSceneManager>()?.Exit();
        }

        public void CharacterChosen(int character)
        {
            if (!characterChoose)
            {
                characterChoose = true;
                Debug.Log("Choose Character");
                if (character == 0)
                    PlayerPrefs.SetString("Character", CharacterTypes.Boy);
                else
                    PlayerPrefs.SetString("Character", CharacterTypes.Woman);

                FindObjectOfType<PracticeGamesSceneManager>()?.OpenGameScene();
            }
        }

        private void EnableUI(bool enable)
        {
            this.infoButton.enabled = enable;
            this.teacherCharacter.SetActive(enable);
            this.studentCharacter.SetActive(enable);
            this.textTitle.SetActive(enable);
        }

        public void OpenInfoCanvas()
        {
            panelInfo.SetActive(true);
            infoButton.gameObject.SetActive(false);
            EnableUI(false);
        }

        public void CloseInfoCanvas()
        {
            panelInfo.SetActive(false);
            infoButton.gameObject.SetActive(true);
            EnableUI(true);
        }

        // Update is called once per frame
        void Update()
        {
            Vector3? pos = null;

            // Mouse click
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("touch");
                pos = Input.mousePosition;
            }

            // Touch screen
            if (Input.touchCount > 0)
            {
                Debug.Log("touch");

                Touch touch = Input.touches[0];
                pos = touch.position;
            }

            // Detect raycast
            if (pos != null)
            {
                Ray ray = camera.ScreenPointToRay((Vector3)pos);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.name == "boy_B")
                    {
                        Debug.Log("hit student");
                        CharacterChosen(0);
                    }
                    else if (hit.collider.gameObject.name == "woman_C")
                    {
                        Debug.Log("hit teacher");
                        CharacterChosen(1);
                    }
                }
            }
        }
    }
}
