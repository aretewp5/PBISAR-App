using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using UnityEngine.UI;

using System.Text.RegularExpressions;


using System.Threading.Tasks;
using System.Linq;
using UnityEditor;
using Newtonsoft.Json;
using UnityEngine.Networking;
using System.Text;
using UnityEngine.SceneManagement;

public class NickNameManager : MonoBehaviour
{
    [SerializeField] private TMP_InputField _inputFieldUserName;
    //[SerializeField] private TMP_InputField _inputFieldPassword;

    [SerializeField] private TextMeshProUGUI _statusMessage;

    
    public static NickNameManager instance;
    public string nameUser;
    private Animator anim;

    void Awake()
    {
        instance = this;
        _statusMessage.text ="";
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        anim = uiCanvas.GetComponent<Animator>();
        foreach(AnimationClip ac in anim.runtimeAnimatorController.animationClips){
             Debug.Log("****   **** Animazione nome: "+ ac.name);
        }
    }

    void Start()
    {
         _inputFieldUserName.onEndEdit.AddListener(UserNameUpdated);
        //_inputFieldPassword.onEndEdit.AddListener(PasswordUpdated);
        
    }

    void Update()
    {
        
    }


   private void OnEnable()
    {
        //ResetValues();
    }



    public void startButtonSelectedOLD(){
        // anim.Play("LOGIN_ERROR");
        //Login(_inputFieldUserName.text, _inputFieldPassword.text);
        
        Login(_inputFieldUserName.text, "");
    }

     public void cancelButtonSelected(){
        anim.Play("LOGIN_OUT");
    }


    public void startButtonSelected() {

        string organization = Player.instance.organization;
        if ((organization != null) && (organization.Length > 0))
        {
            // StudenName.text = organization + "-" + myDialog.name;

            string studentUsername = organization + "-" + _inputFieldUserName.text;
            //Debug.Log("StudentHomeManager - DialogOkButtonSelected -  studentUsername: " + studentUsername);

            int index = _inputFieldUserName.text.IndexOf('-');
            //Debug.Log("StudentHomeManager - DialogOkButtonSelected -  index: " + index);
            if (index <= 0)
            {
                string message = FindObjectOfType<LangResolver>().GetLocalizedText("SUDENTCODE_UNFORMATTED");
                if ((message != null) && (message.Length > 0))
                    SetWarning(message);
                else
                    SetWarning("Student conde not valid!");
            }
            else
            {
                string studentClassName = _inputFieldUserName.text.Substring(0, index);
                //Debug.Log("StudentHomeManager - DialogOkButtonSelected -  studentClassName: " + studentClassName);

                string studentName = _inputFieldUserName.text.Substring(index + 1);
                //Debug.Log("StudentHomeManager - DialogOkButtonSelected -  studentName: " + studentName);

                string studentPassword = GeneratePassword(organization, studentClassName, studentName);
                Debug.Log("StudentHomeManager - DialogOkButtonSelected -  studentPassword: " + studentPassword);

                StartCoroutine(StudentLogin(studentUsername, studentPassword));
            }


        }
        else
        {
            //ERROR no data session found
            //TODO Manage this case
        }

    }




    public void SetWarning(string warningText)
    {
        Debug.Log("NickNameManager - SetWarning - warningText: "+ warningText);
        _statusMessage.text = warningText;
        _statusMessage.gameObject.SetActive(true);
    }


    private string GeneratePassword(string organization, string className, string name)
    {
        string password = organization + (organization + name).Length + className + name.ToUpper() + (className + name).Length;
        return password;
    }



    IEnumerator StudentLogin(string username, string password)
    {
        // DialogManager dialogManager = DialogPanel.GetComponent<DialogManager>();
        string ulrString = "https://arete.pa.itd.cnr.it/arete/rest/public/login";
        string logindataJsonString = "{ \"username\":\"" + username + "\" , \"password\": \"" + password + "\" }";
        Debug.Log("SceneManager - SiletLoginPbisAppUser - urlString: " + ulrString);
        var request = new UnityWebRequest(ulrString, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(logindataJsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();

        if (request.error != null)
        {
            if ((request.responseCode >= 400) && (request.responseCode < 500))
            {
                if ((request.responseCode == 401) || (request.responseCode == 403))
                {
                    Debug.Log("SceneManager - SiletLoginPbisAppUser - Errore! ");
                    string message = FindObjectOfType<LangResolver>().GetLocalizedText("SUDENTCODE_UNAUTHORIZED");
                    if ((message != null) && (message.Length > 0))
                        SetWarning(message);
                    else
                        SetWarning("You must enter a valid Student code!");
                }
                else
                {
                    string message = FindObjectOfType<LangResolver>().GetLocalizedText("RESOURCE_FORBIDDEN");
                    if ((message != null) && (message.Length > 0))
                        SetWarning(message);
                    else
                        SetWarning("Resouce Forbidden.");
                }
            }
            else
            {
                string message = FindObjectOfType<LangResolver>().GetLocalizedText("INTERNAL_SERVER_ERROR");
                if ((message != null) && (message.Length > 0))
                    SetWarning(message);
                else
                    SetWarning("Server error. Please, try again.");
            }
        }
        else
        {
            Debug.Log("SceneManager - SiletLoginPbisAppUser - OK");
            string xAuthToken = request.GetResponseHeader("x-auth");
            Debug.Log("SceneManager - SiletLoginPbisAppUser - xAuthToken: " + xAuthToken);
            UserModel user = JsonConvert.DeserializeObject<UserModel>(request.downloadHandler.text);
            Player.instance.xAuthToken = xAuthToken;
            Player.instance.username = username;
            Player.instance.name = user.nome;
            Player.instance.section = user.section;
            Player.instance.points = user.points;
            /*
            string studentInfo = "<b>Nome:</b> " + user.nome;
            studentInfo += "<br><b>Section:</b> " + user.section;
            studentInfo += "<br><b>Points:</b> " + user.points;
            StudentInfo.text = studentInfo;
            
            DialogPanel.transform.gameObject.SetActive(false);
            NewClassPanel.transform.gameObject.SetActive(false);
            */


            LearningLockerSetting.instance.SendStatement("started", "arExperience");
            anim.Play("LOGIN_OUT");
            // SceneManager.LoadSceneAsync("StudentHomeScene", LoadSceneMode.Additive);


            //Find AlienBubbled and set active false
            GameObject alienSpawned = GameObject.Find("AlienBubbled(Clone)");
            if (alienSpawned)
                alienSpawned.SetActive(false);


            //   - Show Section and Token Panel
            GameObject uiCanvas = GameObject.Find("UI Canvas");
            Transform sectionPanelTansform = uiCanvas.transform.FindDeepChild("SectionPanel");
            if (sectionPanelTansform)
            {
                // Debug.Log("****   **** TextBubble trovata");
                sectionPanelTansform.gameObject.SetActive(true);
            }
            Transform discoveryPanelTansform = uiCanvas.transform.FindDeepChild("DiscoveryPanel");
            if (discoveryPanelTansform)
            {
                // Debug.Log("****   **** TextBubble trovata");
                discoveryPanelTansform.gameObject.SetActive(true);
            }



        }

    }


    private async void Login(string username, string password)
    {
        Debug.Log("****   **** NickNameManager - Login button pressed");
        /*
        var (result, response) = await Network.LoginRequestAsync(username, password, DBManager.domain);
        if (result && response.StartsWith("succeed"))
        {
            OnLoginSucceed(username, password, response.Split(',')[1]);
            Debug.Log("Moodle userId: "+await MoodleManager.Instance.GetUserId());

            // **************   *****************
            // Da implementare per prendere il nome dell'utente collegato
            // Debug.Log("Moodle Name: "+await MoodleManager.Instance.GetUserFullName());
            
            //Toast.Instance.Show("Login succeeded");
            _statusMessage.text ="";
            
        }
        else
        {
            anim.Play("LOGIN_ERROR");
            _statusMessage.text ="Check your nickname/password";
            // _inputFieldUserName.SetInvalid();
            // _inputFieldPassword.SetInvalid();

            
        }
        */
        OnLoginSucceed(username, password);
        //Send statement to CNR LL 
        nameUser = _inputFieldUserName.text;
        Debug.Log("Nome inserito " + nameUser);
        LearningLockerSetting.instance.SendStatement("started", "arExperience");

    }

    private void OnLoginSucceed(string username, string password)
    {
        Debug.Log("OOOOK");
        Debug.Log($"{username} logged in successfully.");

        anim.Play("LOGIN_OUT");

        
        GameObject alienSpawned = GameObject.Find("AlienBubbled(Clone)");
        if (alienSpawned)
            alienSpawned.SetActive(false);


        //   - Show Section and Token Panel
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        Transform sectionPanelTansform = uiCanvas.transform.FindDeepChild("SectionPanel");
        if (sectionPanelTansform)
        {
            // Debug.Log("****   **** TextBubble trovata");
            sectionPanelTansform.gameObject.SetActive(true);
        }
        Transform discoveryPanelTansform = uiCanvas.transform.FindDeepChild("DiscoveryPanel");
        if (discoveryPanelTansform)
        {
            // Debug.Log("****   **** TextBubble trovata");
            discoveryPanelTansform.gameObject.SetActive(true);
        }

    }




    public void UserNameUpdated(string text) 
    {
        Debug.Log("UserName Output string "  + text);
        _statusMessage.text ="";
    }

    public void PasswordUpdated(string text) 
    {
        Debug.Log("Password Output string "  + text);
        _statusMessage.text ="";
    }



       
    private static bool IsValidUsername(string urlString)
    {
        const string regexExpression = "^(\\w{2,}?\\S+)$";
        var regex = new Regex(regexExpression);
        return regex.IsMatch(urlString);
        
    }

    private static bool IsValidPassword(string urlString)
    {
        const string regexExpression = "^(\\w{3,}?\\S+)$";
        var regex = new Regex(regexExpression);
        return regex.IsMatch(urlString);
        
    }


}
