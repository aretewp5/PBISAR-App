using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
  
public class RotateWheel : MonoBehaviour
{
    [SerializeField, Range(0, 3)] private float rotationRate = 0.4f;
    Color greenArete = new Color(0.749f, 0.953f, 0.161f);
    Color lightBlue = new Color(0.165f,0.357f,0.776f);
    GameObject indicator;

    
    private void Awake()
    {
        indicator = GameObject.Find("Indicator");
        indicator.GetComponent<Image>().color = lightBlue;
    }

    void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            Debug.Log("Touching at: " + touch.position);

            if (touch.phase == TouchPhase.Began)
            {
                Debug.Log("Touch phase began at: " + touch.position);
                indicator.GetComponent<Image>().color = lightBlue;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                Debug.Log("Touch phase Moved");
                
                transform.Rotate(0, 0, touch.deltaPosition.x * rotationRate, Space.Self);
                //transform.Rotate(0, 0, touch.deltaPosition.x * rotationRate, Space.World);
                indicator.GetComponent<Image>().color = lightBlue;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                Debug.Log("Touch phase Ended");
                indicator.GetComponent<Image>().color = greenArete;

            }
        }
    }
    
   
}