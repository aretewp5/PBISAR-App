using OrkestraLibImp;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PracticeGamesSceneManager : MonoBehaviour, IGameController
{
    [SerializeField]
    public string SceneParent = "Practice";

    [SerializeField]
    public string SceneManagerName = "";

    [SerializeField]
    public string SceneChooseCharacter = "ChooseCharacter";

    [SerializeField]
    public string SceneGamePlay = "GameScene";

    void Start()
    {
        StartCoroutine(PerformOpenSceneChooseCharacter());
    }

    /**
     * Starts coroutine to go back to Practice scene.
     */
    public void Exit()
    {
        Debug.Log("PracticeGamesManager");
        StartCoroutine(PerformExit());
    }

    public bool IsSceneActive(string name)
    {
        if (SceneManager.sceneCount <= 1)
        {
            return false;
        }

        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.name.Equals(name))
            {
                return scene.isLoaded;
            }
        }

        return false;
    }

    public void RestartGame()
    {
        StartCoroutine(PerformRestartGameScene());
    }

    public void OpenGameScene()
    {
        PlayerPrefs.SetInt("TimesTried", 0);
        PlayerPrefs.SetInt("Score", 0);
        if (IsSceneActive(SceneChooseCharacter))
        {
            SceneManager.UnloadSceneAsync(SceneChooseCharacter);
        }

        SceneManager.LoadSceneAsync(SceneGamePlay, LoadSceneMode.Additive);
    }

    IEnumerator PerformRestartGameScene()
    {
        AsyncOperation asyncLoad = SceneManager.UnloadSceneAsync(SceneGamePlay);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        SceneManager.LoadSceneAsync(SceneGamePlay, LoadSceneMode.Additive);
    }

    IEnumerator PerformOpenSceneChooseCharacter()
    {
        if (IsSceneActive(SceneManagerName))
        {
            // Ensure scene manager is not destroyed
            AsyncOperation asyncLoad = SceneManager.UnloadSceneAsync(SceneManagerName);
            while (!asyncLoad.isDone)
            {
                yield return null;
            }
        }

        SceneManager.LoadSceneAsync(SceneChooseCharacter, LoadSceneMode.Additive);
    }

    /**
     * Removes all game scenes
     */
    IEnumerator PerformExit()
    {
        // Unstacking process
        for (int i = SceneManager.sceneCount - 1; i >= 0; i--)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.name.Equals(SceneChooseCharacter) || scene.name.Equals(SceneGamePlay))
            {
                if (scene.isLoaded)
                {
                    AsyncOperation asyncLoad1 = SceneManager.UnloadSceneAsync(scene.name);
                    while (!asyncLoad1.isDone)
                    {
                        yield return null;
                    }
                }
            }
        }

        Destroy(FindObjectOfType<OrkestraImpl>().gameObject);
        ResetParameters();
        SceneManager.LoadSceneAsync(SceneParent, LoadSceneMode.Additive);
    }

    private void ResetParameters()
    {
        Camera.main.cullingMask = LayerMask.GetMask("Default", "TransparentFX", "UI", "Ignore Raycast", "Water", "Rig");
        FindObjectOfType<ImageTraking>().enabled = true;
        Canvas uicanvas = null;
        if (GameObject.Find("UI Canvas") != null)
        {
            uicanvas = GameObject.Find("UI Canvas").GetComponent<Canvas>();
            if (uicanvas != null)
            {
                uicanvas.enabled = true;
            }
        }

        PlaceOnPlane p = FindObjectOfType<PlaceOnPlane>();
        p.enabled = true;
    }
}
