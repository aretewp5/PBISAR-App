using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text;


using Newtonsoft.Json; //libreria JSON .NET For Unity, non necesario installare perch? gi? contenuta nel pacchetto TinCan-xAPI (External/TinCan-xAPI)
/*
// ************* Inizio xAPI  
using System;
using TinCan;
using TinCan.LRSResponses;
// ************** Fine xAPI  
*/


public class SetupManager : MonoBehaviour
{

    public GameObject username;
    public GameObject password;
    public Text infoMessage;
    public Text versionLabel;
    private string userString;
    private string passString;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("LoginScene - Start - Application Version : " + Application.version);
        //Debug.Log("LoginScene - Start - Application buildGUID : " + Application.buildGUID);
        versionLabel.text = FindObjectOfType<LangResolver>().GetLocalizedText("VERSION") + ": " + Application.version;
        // PlayerPrefs.DeleteKey("xAuthToken");
        // PlayerPrefs.DeleteKey("UserRole");
    }
    private void Awake()
    {
        Debug.Log("LoginScene - Awake ....");
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log("SetupManager - Error. Check Internet connection!");
            infoMessage.text = FindObjectOfType<LangResolver>().GetLocalizedText("NO_INTERN_CONNECTION");
        }
        else
        {
            Debug.Log("SetupManager - You are connected to the Internet !");
            infoMessage.text = "";
        }
    }

    // Update is called once per frame
    void Update()
    {
        userString = username.GetComponent<InputField>().text;
        passString = password.GetComponent<InputField>().text;
    }

    public void SetupButtonSelected()
    {

        if (userString == "" && passString == "")
        {
            infoMessage.text = FindObjectOfType<LangResolver>().GetLocalizedText("USERNAME_PASSWORD_ERROR");

            // TODO Only for Debug - remove for the release
            username.GetComponent<InputField>().text = "PBISUSER-CNR";
            password.GetComponent<InputField>().text = "PippoBello69!";
            userString = "PBISUSER-CNR";
            passString = "PippoBello69!";
        }

        //Debug.Log("SetupManager - signInSelected username: " + userString + "  password: " + passString);
        string ulrString = "https://arete.pa.itd.cnr.it/arete/rest/public/login";
        //Debug.Log("SetupManager - ulrString: " + ulrString);
        string logindataJsonString = "{ \"username\":\"" + userString + "\" , \"password\": \"" + passString + "\" }";
        //Debug.Log("LoginScene - logindataJsonString: " + logindataJsonString);
        StartCoroutine(setupAction(ulrString, logindataJsonString));
    }



    // Esempio di POST
    //public IEnumerator doLogin(string url, string logindataJsonString)
    IEnumerator setupAction(string url, string logindataJsonString)
    {
        Debug.Log("SetupManager - loginAction: " + url);
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(logindataJsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();

        if (request.error != null)
        {
            Debug.Log("SetupManager - Error: " + request.error + "  Status Code: " + request.responseCode);
            if ((request.responseCode >= 400) && (request.responseCode < 500))
            {
                if ((request.responseCode == 401) || (request.responseCode == 403))
                {
                    Debug.Log("SetupManager - Errore! ");
                    //Debug.Log("LoginScene - Errore: " + FindObjectOfType<LangResolver>().GetLocalizedText("USER_UNAUTHORIZED"));
                    infoMessage.text = FindObjectOfType<LangResolver>().GetLocalizedText("USER_UNAUTHORIZED");
                    //Debug.Log("LoginScene - Errore: " + FindObjectOfType<LangResolver>().GetLocalizedText("USER_UNAUTHORIZED"));
                }
                else
                {
                    infoMessage.text = FindObjectOfType<LangResolver>().GetLocalizedText("RESOURCE_FORBIDDEN");
                }
            }
            else
            {//if (request.responseCode >= 500))
                infoMessage.text = FindObjectOfType<LangResolver>().GetLocalizedText("INTERNAL_SERVER_ERROR");
            }
        }
        else
        {

            Debug.Log("SetupManager -  OK");
            string xAuthToken = request.GetResponseHeader("x-auth");
            //Debug.Log("SetupManager -  xAuthToken: "+ xAuthToken);
            UserModel user = JsonConvert.DeserializeObject<UserModel>(request.downloadHandler.text);

            PlayerPrefs.SetString("pbisAppUserName", user.username);
            PlayerPrefs.SetString("pbisAppUserPassword", passString);
            PlayerPrefs.Save();

            Player.Reset();
            Player.SetupData();

            Player.instance.pbisAppUserXAuthToken = xAuthToken;
            Player.instance.organization = user.organizationName;

            //Player.instance.firstUse = true;

            SceneManager.LoadScene("IntroPBIS");
            // SceneManager.LoadScene("IntroPBIS",LoadSceneMode.Additive);
            // SceneManager.UnloadSceneAsync("SetupScene");
            
        }

    }

}
