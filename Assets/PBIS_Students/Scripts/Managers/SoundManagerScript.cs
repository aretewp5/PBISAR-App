using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip beepSound;
    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        beepSound = Resources.Load<AudioClip>("beep");
        audioSrc = GetComponent<AudioSource>();
    }

    public static void playSound()
    {
        audioSrc.PlayOneShot(beepSound);
    }
}
