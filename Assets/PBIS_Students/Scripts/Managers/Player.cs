using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player instance;

    private void Awake()
    {
        instance = this;
    }

    //General
    public string username;
    public string name;
    public string role;
    public string xAuthToken;
    public string section;
    public string organization;
    public int points;
    public string myTeacher;
    public string lessonSelected;

    public string pbisAppUserPassword;
    public string pbisAppUserName;
    public string pbisAppUserXAuthToken;

    public string AppModality;

    //public bool firstUse;
    public bool skipped=false;

    public Dictionary<string, string> routineCollection;
    public Dictionary<string, string> pbisLesson;

    public static void Reset()
    {
        Player.instance.username = "";
        Player.instance.name = "";
        Player.instance.role = "";
        Player.instance.xAuthToken = "";
        Player.instance.section = "";
        Player.instance.organization = "";
        Player.instance.points = 0;

        Player.instance.pbisAppUserPassword = "";
        Player.instance.pbisAppUserName = "";
        Player.instance.pbisAppUserXAuthToken = "";

        Player.instance.AppModality = "";

        Player.instance.routineCollection = new Dictionary<string, string>();
        /*
        Player.instance.pbisLesson = new Dictionary<string, string>();
        Player.instance.pbisLesson.Add("Greet Others", "1-");
        Player.instance.pbisLesson.Add("Walk with a goal", "2-");
        Player.instance.pbisLesson.Add("I Keep my hands and feet to myself", "3-");
        Player.instance.pbisLesson.Add("Keep your working space organised", "4-");
        Player.instance.pbisLesson.Add("I store my belongings", "5-");
        Player.instance.pbisLesson.Add("Work independently at your desk", "6-");
        Player.instance.pbisLesson.Add("Stand up for others", "7-");
        Player.instance.pbisLesson.Add("I help others with questions", "8-");
        Player.instance.pbisLesson.Add("I let others be in peace", "9-");
        */
        Player.instance.myTeacher = "";
        Player.instance.lessonSelected = "";
        //Player.instance.firstUse = true;
        Player.instance.skipped = false;
    }


    public static void SetupData()
    {
        Player.instance.pbisLesson = new Dictionary<string, string>();
        Player.instance.pbisLesson.Add("Greet Others", "1-");
        Player.instance.pbisLesson.Add("Walk with a goal", "2-");
        Player.instance.pbisLesson.Add("I Keep my hands and feet to myself", "3-");
        Player.instance.pbisLesson.Add("Keep your working space organised", "4-");
        Player.instance.pbisLesson.Add("I store my belongings", "5-");
        Player.instance.pbisLesson.Add("Work independently at your desk", "6-");
        Player.instance.pbisLesson.Add("Stand up for others", "7-");
        Player.instance.pbisLesson.Add("I help others with questions", "8-");
        Player.instance.pbisLesson.Add("I let others be in peace", "9-");
    }
}
