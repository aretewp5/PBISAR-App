using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using UnityEngine.UI;
using MirageXR;
using Network = MirageXR.Network;
using System.Text.RegularExpressions;


using System.Threading.Tasks;
using System.Linq;
using UnityEditor;




public class DummyScript : MonoBehaviour
{
    [SerializeField] private TMP_InputField _inputFieldUserName;
    //[SerializeField] private TMP_InputField _inputFieldPassword;

    [SerializeField] private TextMeshProUGUI _statusMessage;


    private Animator anim;

    void Awake()
    {
        _statusMessage.text = "";
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        anim = uiCanvas.GetComponent<Animator>();
        foreach (AnimationClip ac in anim.runtimeAnimatorController.animationClips)
        {
            Debug.Log("****   **** Animazione nome: " + ac.name);
        }
    }

    void Start()
    {
        _inputFieldUserName.onEndEdit.AddListener(UserNameUpdated);
        //_inputFieldPassword.onEndEdit.AddListener(PasswordUpdated);
    }

    void Update()
    {

    }


    private void OnEnable()
    {
        //ResetValues();
    }






    public void startButtonSelected()
    {
        // anim.Play("LOGIN_ERROR");
        //Login(_inputFieldUserName.text, _inputFieldPassword.text);
        Login(_inputFieldUserName.text, "");


    }

    public void cancelButtonSelected()
    {
        anim.Play("LOGIN_OUT");
    }



    private async void Login(string username, string password)
    {
        //ModalWindowView.Instance.ShowWaiter();
        var (result, response) = await Network.LoginRequestAsync(username, password, DBManager.domain);

        //ModalWindowView.Instance.HideWaiter();
        if (result && response.StartsWith("succeed"))
        {
            OnLoginSucceed(username, password, response.Split(',')[1]);
            Debug.Log("Moodle userId: " + await RootObject.Instance.moodleManager.GetUserId());

            // **************   *****************
            // Da implementare per prendere il nome dell'utente collegato
            // Debug.Log("Moodle Name: "+await MoodleManager.Instance.GetUserFullName());

            //Toast.Instance.Show("Login succeeded");
            _statusMessage.text = "";

        }
        else
        {
            anim.Play("LOGIN_ERROR");
            _statusMessage.text = "Check your nickname/password";
            // _inputFieldUserName.SetInvalid();
            // _inputFieldPassword.SetInvalid();

            //Toast.Instance.Show("Check your login/password");
        }
    }

    private void OnLoginSucceed(string username, string password, string token)
    {
        Debug.Log("OOOOK");
        DBManager.token = token;
        DBManager.username = username;
        Debug.Log($"{username} logged in successfully.");
        anim.Play("LOGIN_OUT");
        
        
        GameObject alienSpawned = GameObject.Find("AlienBubbled(Clone)");
        if (alienSpawned)
            alienSpawned.SetActive(false);


        RoutineListView.Instance.UpdateListView();

        IntroDialougeManager.Instance.PostLoginSucceed();

        
        //ActivityListView.Instance.UpdateListView();
        /*
        if (_toggleRemember.isOn)
        {
            LocalFiles.SaveUsernameAndPassword(username, password);
        }
        else
        {
            LocalFiles.RemoveUsernameAndPassword();
        }*/

        
        //ModalWindowView.Instance.HideLogin();
    }




    public void UserNameUpdated(string text)
    {
        Debug.Log("UserName Output string " + text);
        _statusMessage.text = "";
    }

    public void PasswordUpdated(string text)
    {
        Debug.Log("Password Output string " + text);
        _statusMessage.text = "";
    }




    private static bool IsValidUsername(string urlString)
    {
        const string regexExpression = "^(\\w{2,}?\\S+)$";
        var regex = new Regex(regexExpression);
        return regex.IsMatch(urlString);

    }

    private static bool IsValidPassword(string urlString)
    {
        const string regexExpression = "^(\\w{3,}?\\S+)$";
        var regex = new Regex(regexExpression);
        return regex.IsMatch(urlString);

    }


}
