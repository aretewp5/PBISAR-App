using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using UnityEngine.UI;
using MirageXR;
using Network = MirageXR.Network;
using System.Text.RegularExpressions;


using System.Threading.Tasks;
using System.Linq;
using UnityEditor;
using Newtonsoft.Json;
using UnityEngine.Networking;
using System.Text;

public class LoginManager : MonoBehaviour
{
    [SerializeField] private TMP_InputField _inputFieldUserName;
    [SerializeField] private TextMeshProUGUI _statusMessage;


    private Animator anim;

    void Awake()
    {
        _statusMessage.text = "";
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        anim = uiCanvas.GetComponent<Animator>();
        foreach (AnimationClip ac in anim.runtimeAnimatorController.animationClips)
        {
            Debug.Log("LoginManager - Animation: " + ac.name);
        }
    }

    void Start()
    {
        _inputFieldUserName.onEndEdit.AddListener(UserNameUpdated);
    }



    public void startButtonSelected()
    {

        string organization = Player.instance.organization;
        if ((organization != null) && (organization.Length > 0))
        {
            // StudenName.text = organization + "-" + myDialog.name;

            string studentUsername = organization + "-" + _inputFieldUserName.text;
            //Debug.Log("StudentHomeManager - DialogOkButtonSelected -  studentUsername: " + studentUsername);

            int index = _inputFieldUserName.text.IndexOf('-');
            //Debug.Log("StudentHomeManager - DialogOkButtonSelected -  index: " + index);
            if (index <= 0)
            {
                string message = FindObjectOfType<LangResolver>().GetLocalizedText("SUDENTCODE_UNFORMATTED");
                if ((message != null) && (message.Length > 0))
                    SetWarning(message);
                else
                    SetWarning("Student conde not valid!");
            }
            else
            {
                string studentClassName = _inputFieldUserName.text.Substring(0, index);
                //Debug.Log("StudentHomeManager - DialogOkButtonSelected -  studentClassName: " + studentClassName);

                string studentName = _inputFieldUserName.text.Substring(index + 1);
                //Debug.Log("StudentHomeManager - DialogOkButtonSelected -  studentName: " + studentName);

                string studentPassword = GeneratePassword(organization, studentClassName, studentName);
                Debug.Log("StudentHomeManager - DialogOkButtonSelected -  studentPassword: " + studentPassword);

                StartCoroutine(StudentLogin(studentUsername, studentPassword));
            }


        }
        else
        {
            //ERROR no data session found
            //TODO Manage this case
            string message = FindObjectOfType<LangResolver>().GetLocalizedText("SESSION_ERROR_RESTART");
            if ((message != null) && (message.Length > 0))
                SetWarning(message);
            else
                SetWarning("An unexpected error has occurred, restart the app");
        }
    }




    public void SetWarning(string warningText)
    {
        Debug.Log("LoginManager - SetWarning - warningText: " + warningText);
        _statusMessage.text = warningText;
        _statusMessage.gameObject.SetActive(true);
    }


    private string GeneratePassword(string organization, string className, string name)
    {
        string password = organization + (organization + name).Length + className + name.ToUpper() + (className + name).Length;
        return password;
    }



    IEnumerator StudentLogin(string username, string password)
    {
        // DialogManager dialogManager = DialogPanel.GetComponent<DialogManager>();
        string ulrString = "https://arete.pa.itd.cnr.it/arete/rest/public/login";
        string logindataJsonString = "{ \"username\":\"" + username + "\" , \"password\": \"" + password + "\" }";
        Debug.Log("SceneManager - SiletLoginPbisAppUser - urlString: " + ulrString);
        var request = new UnityWebRequest(ulrString, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(logindataJsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();

        if (request.error != null)
        {
            if ((request.responseCode >= 400) && (request.responseCode < 500))
            {
                if ((request.responseCode == 401) || (request.responseCode == 403))
                {
                    Debug.Log("LoginManager - StudentLogin - Errore! ");
                    string message = FindObjectOfType<LangResolver>().GetLocalizedText("SUDENTCODE_UNAUTHORIZED");
                    if ((message != null) && (message.Length > 0))
                        SetWarning(message);
                    else
                        SetWarning("You must enter a valid Student code!");
                }
                else
                {
                    string message = FindObjectOfType<LangResolver>().GetLocalizedText("RESOURCE_FORBIDDEN");
                    if ((message != null) && (message.Length > 0))
                        SetWarning(message);
                    else
                        SetWarning("Resouce Forbidden.");
                }
            }
            else
            {
                string message = FindObjectOfType<LangResolver>().GetLocalizedText("INTERNAL_SERVER_ERROR");
                if ((message != null) && (message.Length > 0))
                    SetWarning(message);
                else
                    SetWarning("Server error. Please, try again.");
            }
        }
        else
        {
            Debug.Log("LoginManager - StudentLogin - OK");
            string xAuthToken = request.GetResponseHeader("x-auth");
            Debug.Log("LoginManager - StudentLogin - xAuthToken: " + xAuthToken);
            UserModel user = JsonConvert.DeserializeObject<UserModel>(request.downloadHandler.text);

            Player.SetupData();

            Player.instance.xAuthToken = xAuthToken;
            Player.instance.username = username;
            Player.instance.name = user.nome;
            Player.instance.section = user.section;
            Player.instance.points = user.points;

            Player.instance.routineCollection = user.routinecollection;
            
            //Player.instance.firstUse = false;
            if (PlayerPrefs.HasKey("appUsage")) {
                int appUsage = PlayerPrefs.GetInt("appUsage");

                if (appUsage > 0)
                {
                    PlayerPrefs.SetInt("appUsage", appUsage + 1);
                }
                else
                {
                    PlayerPrefs.SetInt("appUsage", 1);
                }
            }
            else
            {
                PlayerPrefs.SetInt("appUsage", 1);
            }
            
            PlayerPrefs.Save();

            Debug.Log("LoginManager - StudentLogin - appUsage: " + PlayerPrefs.GetInt("appUsage"));
            // Debug
            /*
            if ((Player.instance.routineCollection != null) && (Player.instance.routineCollection.Count > 0))
            {
                foreach (var key in Player.instance.routineCollection.Keys)
                {
                    Debug.Log("LoginManager - StudentLogin - routine: " + key + " = "+ Player.instance.routineCollection[key]);
                }
            }
            */

            StartCoroutine(LoadTeacherList(Player.instance.organization, Player.instance.section));

            // OLD
            /*
            LearningLockerSetting.instance.SendStatement("started", "arExperience");
            anim.Play("LOGIN_OUT");
            // SceneManager.LoadSceneAsync("StudentHomeScene", LoadSceneMode.Additive);

            GameObject alienSpawned = GameObject.Find("AlienBubbled(Clone)");
            if (alienSpawned)
                alienSpawned.SetActive(false);

            //   - Show Section and Token Panel
            GameObject uiCanvas = GameObject.Find("UI Canvas");
            Transform sectionPanelTansform = uiCanvas.transform.FindDeepChild("SectionPanel");
            if (sectionPanelTansform)
            {
                // Debug.Log("****   **** TextBubble trovata");
                sectionPanelTansform.gameObject.SetActive(true);
            }
            Transform discoveryPanelTansform = uiCanvas.transform.FindDeepChild("DiscoveryPanel");
            if (discoveryPanelTansform)
            {
                // Debug.Log("****   **** TextBubble trovata");
                discoveryPanelTansform.gameObject.SetActive(true);
            }
            */
        }
    }






    IEnumerator LoadTeacherList(string organization, string myClass)
    {
        //string ulrString = "https://arete.pa.itd.cnr.it/arete/rest/private/studentList?organizationName=CNR&section=4E";
        string ulrString = "https://arete.pa.itd.cnr.it/arete/rest/private/teacherList?organizationName=" + organization + "&section=" + myClass;
        Debug.Log("LoginManager - LoadTeacherList: " + ulrString);
        using (UnityWebRequest webRequest = UnityWebRequest.Get(ulrString))
        {
            // Request and wait for the desired page.
            webRequest.SetRequestHeader("X-Auth", Player.instance.xAuthToken);
            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError("LoginManager - LoadTeacherList - Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError("LoginManager - LoadTeacherList - HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log("LoginManager - LoadTeacherList - OK");
                    //Debug.Log("LoginManager - LoadTeacherList - Received: " + webRequest.downloadHandler.text);
                    string jsonString = "{\"Items\":" + webRequest.downloadHandler.text + "}";
                    //Debug.Log("LoginManager - LoadTeacherList - : jsonString " + jsonString);
                    UserModel[] studentArray = JsonHelper.FromJson<UserModel>(jsonString);
                    ITEMS items = JsonConvert.DeserializeObject<ITEMS>(jsonString);
                    List<UserModel> teacherList = items.items;
                    if ((teacherList != null) && (teacherList.Count > 0))
                    {
                        foreach (UserModel teacher in teacherList)
                        {
                            Debug.Log("LoginManager - LoadTeacherList - Teacher: " + teacher.username);
                        }
                        if ((teacherList[0] != null) && (teacherList[0].username.Length > 0)) {

                            
                            Player.instance.myTeacher = teacherList[0].moodleiduser;
                            PlayerPrefs.SetString("myTeacher", teacherList[0].moodleiduser);

                            // RESET the lesson selectetd in the preferences
                            PlayerPrefs.SetString("lessonSelected", "");
            

                            PlayerPrefs.Save();
                            Debug.Log("LoginManager - LoadTeacherList - MyTeacher:" + Player.instance.myTeacher);




                            // All right go ahead
                            LearningLockerSetting.instance.SendStatement("started", "arExperience");
                            anim.Play("LOGIN_OUT");
                            // SceneManager.LoadSceneAsync("StudentHomeScene", LoadSceneMode.Additive);

                            GameObject alienSpawned = GameObject.Find("AlienBubbled(Clone)");
                            if (alienSpawned)
                                alienSpawned.SetActive(false);

                            //   - Show Section and Token Panel
                            GameObject uiCanvas = GameObject.Find("UI Canvas");
                            Transform sectionPanelTansform = uiCanvas.transform.FindDeepChild("SectionPanel");
                            if (sectionPanelTansform)
                            {
                                // Debug.Log("****   **** TextBubble trovata");
                                sectionPanelTansform.gameObject.SetActive(true);
                            }
                            Transform discoveryPanelTansform = uiCanvas.transform.FindDeepChild("DiscoveryPanel");
                            if (discoveryPanelTansform)
                            {
                                // Debug.Log("****   **** TextBubble trovata");
                                discoveryPanelTansform.gameObject.SetActive(true);
                            }

                        }
                        else
                        {
                            Debug.Log("LoginManager - LoadTeacherList - Errore sul Teacher! ");
                            string message = FindObjectOfType<LangResolver>().GetLocalizedText("NOTEACHER_ASSIGNED");
                            if ((message != null) && (message.Length > 0))
                                SetWarning(message);
                            else
                                SetWarning("No teacher assigned");
                        }

                    }
                    else
                    {
                        Debug.Log("LoginManager - LoadTeacherList - Errore NESSUN Teacher! ");
                        string message = FindObjectOfType<LangResolver>().GetLocalizedText("NOTEACHER_ASSIGNED");
                        if ((message != null) && (message.Length > 0))
                            SetWarning(message);
                        else
                            SetWarning("No teacher assigned");
                    }
                    break;
            }
        }
    }








    public void cancelButtonSelected()
    {
        string message = FindObjectOfType<LangResolver>().GetLocalizedText("SUDENTCODE_NULL");
        if ((message != null) && (message.Length > 0))
            SetWarning(message);
        else
            SetWarning("You must enter a valid Student code!");
        //anim.Play("LOGIN_OUT");
    }




    public void UserNameUpdated(string text)
    {
        Debug.Log("LoginManager - UserName Output string " + text);
        _statusMessage.text = "";
    }


    /*
    private static bool IsValidUsername(string urlString)
    {
        const string regexExpression = "^(\\w{2,}?\\S+)$";
        var regex = new Regex(regexExpression);
        return regex.IsMatch(urlString);

    }

    private static bool IsValidPassword(string urlString)
    {
        const string regexExpression = "^(\\w{3,}?\\S+)$";
        var regex = new Regex(regexExpression);
        return regex.IsMatch(urlString);

    }
    */

}


[System.Serializable]
public class ITEMS
{
    public List<UserModel> items;
}