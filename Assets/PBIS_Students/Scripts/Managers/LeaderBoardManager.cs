using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;

public class LeaderBoardManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _nameFirst;
    [SerializeField] private TextMeshProUGUI _pointFirst;
    [SerializeField] private TextMeshProUGUI _nameSecond;
    [SerializeField] private TextMeshProUGUI _pointSecond;
    [SerializeField] private TextMeshProUGUI _nameThird;
    [SerializeField] private TextMeshProUGUI _pointThird;

    [SerializeField] private TextMeshProUGUI _studentMessage;

    //[SerializeField] private Transform _LeaderBoardBackgroudPanel;

    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        GameObject uiCanvas = GameObject.Find("UI Canvas");
        anim = uiCanvas.GetComponent<Animator>();

        _nameFirst.text = "Dog";
        _pointFirst.text = "50";
        _nameSecond.text = "Cat";
        _pointSecond.text = "30";
        _nameThird.text = "Fox";
        _pointThird.text = "10";
        _studentMessage.text = "You have earned " + Player.instance.points.ToString() + " points";
    }


    void Awake()
    {
        _studentMessage.text = "You have earned " + Player.instance.points.ToString() + " points";
    }


    // Update is called once per frame
    void Update()
    {
        _studentMessage.text = "You have earned " + Player.instance.points.ToString() + " points";
    }

    public void setPoints()
    {
        _studentMessage.text = "You have earned " + Player.instance.points.ToString() + " points";
    }


    public void CloseButtonSelected()
    {
        Debug.Log("LeaderBoardManager - OkButtonSelected "); 
        //_LeaderBoardBackgroudPanel.gameObject.SetActive(false);
        anim.Play("LEADERBOARDNEW_OUT");
    }



    public void UpdatePodium()
    {
        Debug.Log("LeaderBoardManager - UpdatePodium... ");
        if ((Player.instance.section != null) && (Player.instance.section.Length > 0))
        {
            Debug.Log("ClassManager - LoadClasses - LoadClassStudents section: " + Player.instance.section);
            // Load studnts of the class
            StartCoroutine(LoadLeaderBoard(Player.instance.organization, Player.instance.section));
        }
        else
        {
            // Create a section/class
            Debug.Log("ClassManager - LoadClasses - NOT LoadClassStudents");
        }

    }


    IEnumerator LoadLeaderBoard(string organization, string myClass)
    {
        string ulrString = "https://arete.pa.itd.cnr.it/arete/rest/private/leaderboard?organizationName=" + organization + "&section=" + myClass;
        Debug.Log("LeaderBoardManager - LoadLeaderBoard: " + ulrString);
        using (UnityWebRequest webRequest = UnityWebRequest.Get(ulrString))
        {
            // Request and wait for the desired page.
            webRequest.SetRequestHeader("X-Auth", Player.instance.xAuthToken);
            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError("Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError("HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log("LeaderBoardManager - LoadLeaderBoard - Received: " + webRequest.downloadHandler.text);
                    string jsonString = "{\"Items\":" + webRequest.downloadHandler.text + "}";
                    Debug.Log("LeaderBoardManager - LoadLeaderBoard - : jsonString " + jsonString);
                    UserModel[] studentArray = JsonHelper.FromJson<UserModel>(jsonString);
                    if ((studentArray != null) && (studentArray.Length > 0))
                    {
                        for (int i = 0; i < studentArray.Length; i++)
                        {
                            Debug.Log("LeaderBoardManager - LoadLeaderBoard:" + studentArray[i].username);
                        }
                        SetPodium(studentArray, myClass);

                    }
                    break;
            }
        }
    }

    private void SetPodium(UserModel[] studentArray, string className)
    {
        if ((studentArray != null) && (studentArray.Length >= 3))
        {
            _nameFirst.text = studentArray[0].nome;
            _pointFirst.text = studentArray[0].points.ToString();
            _nameSecond.text = studentArray[1].nome;
            _pointSecond.text = studentArray[1].points.ToString();
            _nameThird.text = studentArray[2].nome;
            _pointThird.text = studentArray[2].points.ToString();
            
        }
        else if ((studentArray != null) && (studentArray.Length ==2))
        {
            _nameFirst.text = studentArray[0].nome;
            _pointFirst.text = studentArray[0].points.ToString();
            _nameSecond.text = studentArray[1].nome;
            _pointSecond.text = studentArray[1].points.ToString();
            _nameThird.text = "";
            _pointThird.text = "";
        }
        else if ((studentArray != null) && (studentArray.Length == 1))
        {
            _nameFirst.text = studentArray[0].nome;
            _pointFirst.text = studentArray[0].points.ToString();
            _nameSecond.text = "";
            _pointSecond.text = "";
            _nameThird.text = "";
            _pointThird.text = "";
        }
        _studentMessage.text = Player.instance.name + ", you have earned " + Player.instance.points.ToString() + " points";
    }
}

