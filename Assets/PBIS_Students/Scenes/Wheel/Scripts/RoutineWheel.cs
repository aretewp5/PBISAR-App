using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Collections;
using TMPro;



public class RoutineWheel : MonoBehaviour
{
    ArrayList myAL = new ArrayList();
    [Header("References :")]
    [SerializeField] private GameObject linePrefab;
    [SerializeField] private Transform linesParent;

    [Space]
    [SerializeField] private Transform PickerWheelTransform;
    [SerializeField] private Transform wheelCircle;
    [SerializeField] private GameObject wheelPiecePrefab;
    [SerializeField] private Transform wheelPiecesParent;


    [Space]
    [Header("Picker wheel settings :")]
   
    [SerializeField] [Range(.2f, 2f)] private float wheelSize = 1f;

    [Space]
    [Header("Picker wheel pieces :")]
    public PieceWheel[] wheelPieces;

    // Events
    private UnityAction onSpinStartEvent;
    private UnityAction<PieceWheel> onSpinEndEvent;


    private bool _isSpinning = false;

    public bool IsSpinning { get { return _isSpinning; } }


    private Vector2 pieceMinSize = new Vector2(81f, 146f);
    private Vector2 pieceMaxSize = new Vector2(144f, 213f);
    private int piecesMin = 2;
    private int piecesMax = 12;

    private float pieceAngle;
    private float halfPieceAngle;
    private float halfPieceAngleWithPaddings;


    private double accumulatedWeight;
    private System.Random rand = new System.Random();

    private List<int> nonZeroChancesIndices = new List<int>();

    private void Start()
    {

        if ((Player.instance.pbisLesson != null) && (Player.instance.pbisLesson.Count > 0))
        {
            foreach (string key in Player.instance.pbisLesson.Keys)
            {
                // myAL.Add(Player.instance.pbisLesson[key]);
                myAL.Add(key);

            }
           
        }
        
        /*
        //To be replaced with names taken from moodle
        myAL.Add("Greet Others");
        myAL.Add("Keep your hands/feet to yourself");
        myAL.Add("Walk with a goal");
        myAL.Add("Keep your working space organised");
        myAL.Add("Store your belongings");
        //myAL.Add("Work independently at your desk");
        //myAL.Add("Stand up for others");
        //myAL.Add("Help others with questions");
        myAL.Add("Let others be");
        */



        pieceAngle = 360 / wheelPieces.Length;
        halfPieceAngle = pieceAngle / 2f;
        halfPieceAngleWithPaddings = halfPieceAngle - (halfPieceAngle / 4f);

        Generate();

        CalculateWeightsAndIndices();
        if (nonZeroChancesIndices.Count == 0)
            Debug.LogError("You can't set all pieces chance to zero");



    }

   

    private void Generate()
    {
        wheelPiecePrefab = InstantiatePiece();

        RectTransform rt = wheelPiecePrefab.transform.GetChild(0).GetComponent<RectTransform>();
        float pieceWidth = Mathf.Lerp(pieceMinSize.x, pieceMaxSize.x, 1f - Mathf.InverseLerp(piecesMin, piecesMax, wheelPieces.Length));
        float pieceHeight = Mathf.Lerp(pieceMinSize.y, pieceMaxSize.y, 1f - Mathf.InverseLerp(piecesMin, piecesMax, wheelPieces.Length));
        rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, pieceWidth);
        rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, pieceHeight);

        for (int i = 0; i < wheelPieces.Length; i++)
            DrawPiece(i);

        Destroy(wheelPiecePrefab);
    }

    private void DrawPiece(int index)
    {
        PieceWheel piece = wheelPieces[index];
        Transform pieceTrns = InstantiatePiece().transform.GetChild(0);

        
        pieceTrns.GetChild(1).GetComponentInChildren<TMP_Text>().text = myAL[index].ToString();
            

        //Line
        Transform lineTrns = Instantiate(linePrefab, linesParent.position, Quaternion.identity, linesParent).transform;
        lineTrns.RotateAround(wheelPiecesParent.position, Vector3.back, (pieceAngle * index) + halfPieceAngle);

        pieceTrns.RotateAround(wheelPiecesParent.position, Vector3.back, pieceAngle * index);
    }

    private GameObject InstantiatePiece()
    {
        return Instantiate(wheelPiecePrefab, wheelPiecesParent.position, Quaternion.identity, wheelPiecesParent);
    }

 


      

    public void OnSpinStart(UnityAction action)
    {
        onSpinStartEvent = action;
    }

    public void OnSpinEnd(UnityAction<PieceWheel> action)
    {
        onSpinEndEvent = action;
    }


    private int GetRandomPieceIndex()
    {
        double r = rand.NextDouble() * accumulatedWeight;

        for (int i = 0; i < wheelPieces.Length; i++)
            if (wheelPieces[i]._weight >= r)
                return i;

        return 0;
    }

    private void CalculateWeightsAndIndices()
    {
        for (int i = 0; i < wheelPieces.Length; i++)
        {
            PieceWheel piece = wheelPieces[i];

            //add weights:
            accumulatedWeight += piece.Chance;
            piece._weight = accumulatedWeight;

            //add index :
            piece.Index = i;

            //save non zero chance indices:
            if (piece.Chance > 0)
                nonZeroChancesIndices.Add(i);
        }
    }




    private void OnValidate()
    {
        if (PickerWheelTransform != null)
            PickerWheelTransform.localScale = new Vector3(wheelSize, wheelSize, 1f);

        if (wheelPieces.Length > piecesMax || wheelPieces.Length < piecesMin)
            Debug.LogError("[ PickerWheelwheel ]  pieces length must be between " + piecesMin + " and " + piecesMax);
    }


       

}
