using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Clickwheel : MonoBehaviour
{
    public bool debug = true;

    public TMP_Text _nameRoutine;

    private Animator anim;

    public void ClickWheelPiece()
    {
        if (debug) Debug.Log("Clickwheel - ClickWheelPiece");
        Debug.Log("You clicked on "+_nameRoutine.text);

        GameObject routineSelectedPanel = GameObject.Find("PanelInhibitor");
        anim = routineSelectedPanel.GetComponent<Animator>();

        if (routineSelectedPanel){
            routineSelectedPanel.GetComponentInChildren<TMP_Text>().text = "You have selected behavioral routine\n"+_nameRoutine.text + "\nDo you want to continue?";

            // Set the lesson selectetd in the preferences
            PlayerPrefs.SetString("lessonSelected", Player.instance.pbisLesson[_nameRoutine.text]);
            PlayerPrefs.Save();
            

            anim.Play("Panel_In");

        }
        GameObject wheel = GameObject.Find("Spinning Circle");
        wheel.GetComponent<RotateWheel>().enabled = false;
            
    }

    public void ClickIndicator()
    {

        GameObject routineSelectedPanel = GameObject.Find("RoutineSelectedPanel");
        anim = routineSelectedPanel.GetComponent<Animator>();

        if (routineSelectedPanel)
        {
            anim.Play("Panel_In");

        }
    }

    
}
