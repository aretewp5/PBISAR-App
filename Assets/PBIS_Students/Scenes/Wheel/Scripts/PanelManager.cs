using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PanelManager : MonoBehaviour
{
    public bool debug = true;

    private Animator anim;

    [SerializeField] private GameObject teachBubble;
    
    


    // Start is called before the first frame update
    public void OnClickYes(){
        if (debug) Debug.Log("PanelManager - OnClickYes");
        GameObject routineSelectedPanel = GameObject.Find("PanelInhibitor");
        anim = routineSelectedPanel.GetComponent<Animator>();

        if (routineSelectedPanel){
            
            anim.Play("Panel_Out");
            SceneManager.LoadSceneAsync("RoutineSelection", LoadSceneMode.Additive);
            //SceneManager.LoadSceneAsync("TeachWithBubble", LoadSceneMode.Additive);
            
            //SceneManager.LoadScene("RoutineSelection", LoadSceneMode.Additive);
            //SceneManager.LoadScene("TeachWithBubble", LoadSceneMode.Additive);

            //SceneManager.UnloadSceneAsync("WheelRoutine");

        }
        
        GameObject wheelRoutine = GameObject.Find("Wheel");
        if (wheelRoutine)
        {
            Debug.Log("PanelManager - OnClickYes - wheelRoutine not unloaded");
            wheelRoutine.SetActive(false);
        }

        teachBubble.SetActive(true);




    }

     public void OnClickNo(){
        if (debug) Debug.Log("PanelManager - OnClickNo");
        // RESET the lesson selectetd in the preferences
        PlayerPrefs.SetString("lessonSelected", "");
        PlayerPrefs.Save();

        GameObject routineSelectedPanel = GameObject.Find("PanelInhibitor");
        anim = routineSelectedPanel.GetComponent<Animator>();

        if (routineSelectedPanel){
            
            anim.Play("Panel_Out");

        }


        GameObject wheel = GameObject.Find("Spinning Circle");
        wheel.GetComponent<RotateWheel>().enabled = true;

    }
}
